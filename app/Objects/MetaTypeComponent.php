<?php
/**
 * Created by PhpStorm.
 * User: jfoy
 * Date: 2/9/19
 * Time: 10:10 AM
 */

namespace App\Objects;

use App\Objects\Models\MetaTypes;
use App\Objects\Models\ResumeLineup;
use App\Objects\Resume\Lineup\Attributes\Attributes;

class MetaTypeComponent extends \App\Objects\Abstracted\AbstractComponent {
	/** @var ResumeLineup */
	protected $database_model;

	protected $resume_attributes;

	function __construct( $pk = null ) {
		parent::__construct( 'MetaTypes', $pk );
	}

	/**
	 * @return string
	 */
	function get_type(): string {
		return $this->database_model->getAttribute( 'type' );
	}

	/**
	 * @return bool
	 */
	public function save(): bool {
		return parent::default_save();
	}

	/**
	 * @return array
	 */
	public function output(): array {
		return [
			'id'             => $this->database_model->getAttribute( 'id' ),
			'type'           => $this->database_model->getAttribute( 'type' ),
			'object_manager' => $this->database_model->getAttribute( 'object_manager' ),
		];
	}
}
