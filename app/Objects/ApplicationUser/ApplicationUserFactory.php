<?php
/**
 * Created by PhpStorm.
 * User: jfoy
 * Date: 2/11/19
 * Time: 8:16 PM
 */

namespace App\Objects\ApplicationUser;

use App\Objects\Models\Application;
use Illuminate\Database\Eloquent\Collection;
use App\Objects\Application\ApplicationComponent;
use App\Objects\ApplicationUser\ApplicationUserComponent;

class ApplicationUserFactory
{

    /**
     * @param int|null $int
     *
     * @return ApplicationUserComponent
     */

    public static function get($int = null): ApplicationUserComponent
    {
        return new ApplicationUserComponent($int);
    }

    protected $models = [];

    public function __construct(Collection $collection = null)
    {
        if (!$collection) {
            return false;
        }
        $this->models = $this->generate_models($collection);
    }

    /**
     * @param $collection
     *
     * @return array
     */
    protected function generate_models(Collection $collection): array
    {
        $container = [];
        foreach ($collection as $key) {
            $component = new ApplicationUserComponent($key);
            if (!$component->isEmpty()) {
                $container[] = $component;
            }
        }

        return $container;
    }

    /**
     * @return array
     */
    public function get_applications(): array
    {
        return $this->models;
    }
}
