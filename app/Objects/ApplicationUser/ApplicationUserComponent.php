<?php

namespace App\Objects\ApplicationUser;

use App\Objects\Abstracted\AbstractComponent;

class ApplicationUserComponent extends AbstractComponent
{
   /** @var ApplicationUser */
   protected $database_model;

   /**
    * @param int|null $pk
    */

   public function __construct($pk = null)
   {
       parent::__construct('ApplicationUser', $pk);
   }

   /**
    * @return bool
    */
   public function save(): bool
   {
       return parent::default_save();
   }

   /**
    * @return array
    */
   public function output(): array
   {
       return parent::default_output();
   }

   public function get_user()
   {
       return $this->database_model->get_user();
   }

   public function populate($values)
   {
       $this->database_model->role = $values['role'];
       $this->database_model->first_name = $values['first_name'];
       $this->database_model->last_name = $values['last_name'];
       $this->database_model->email = $values['email'];
       $this->database_model->application_id = $values['application_id'];
       if ($this->isEmpty()) {
           $this->database_model->created_at = new \DateTime('now', new \DateTimeZone('utc'));
       }
       $this->database_model->updated_at = (new \DateTime('now', new \DateTimeZone('UTC')));


       return $this;
   }
}
