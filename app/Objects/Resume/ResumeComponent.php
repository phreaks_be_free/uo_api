<?php

namespace App\Objects\Resume;

use App\Objects\Models\Resume;
use Illuminate\Support\Facades\DB;
use App\Objects\Models\ResumeProfile;
use App\Objects\Models\ResumeProject;
use App\Objects\Abstracted\AbstractComponent;
use App\Objects\Application\ApplicationComponent;
use App\Objects\Resume\Profile\ResumeProfileComponent;
use App\Objects\Resume\Project\ResumeProjectComponent;

class ResumeComponent extends AbstractComponent
{
	/** @var Resume */
	protected $database_model;

	protected $profile;

	protected $projects = [];

	/**
	 * @param int|null $pk
	 */

	public function __construct($pk = null)
	{
		parent::__construct('Resume', $pk);

		/** @var ResumeProfile */
		$this->profile = new ResumeProfileComponent(
			ResumeProfile::Where('resume_id', $this->get_id())->take(1)->get()->first()
		);
		$projects_array = (ResumeProject::Where('resume_id', $this->get_id())->get())->all();
		foreach ($projects_array as $key) {
			$this->projects[] = new ResumeProjectComponent($key);
		}
	}

	/**
	 * @return bool
	 */
	public function save(): bool
	{
		return parent::default_save();
	}

	/**
	 * @return array
	 */
	public function output(): array
	{
		return array_merge([
			'profile'  => $this->profile->output(),
			'projects' => array_map(function ($project) {
				return $project->output();
			}, $this->projects)
		], parent::default_output());
	}

	/**
	 * @param ApplicationComponent $application_component
	 * @param string $name
	 * @param boolean|null $default
	 *
	 * @return $this
	 * @throws \Exception
	 */
	public function populate(ApplicationComponent $application_component, string $name, $default = null)
	{
		/**
		 * TODO: Update all other resumes to default 0 if default is 1
		 * 								Done ;) 
		 */

		if (!empty($default)) {
			DB::table($this->database_model->getTable())->update(['default' => 0]);
		}

		$this->database_model->name           = $name;
		$this->database_model->application_id = $application_component->get_id();
		if ($this->isEmpty()) {
			$this->database_model->created_at = new \DateTime('now', new \DateTimeZone('UTC'));
		}
		$this->database_model->updated_at = new \DateTime('now', new \DateTimeZone('UTC'));
		$this->database_model->default    = empty($default) ? 0 : 1;

		return $this;
	}


	/**
	 * @return ResumeProfileComponent|false
	 */
	function get_profile()
	{
		return $this->profile;
	}

	/**
	 * @return ResumeProjectComponent[]|false
	 */
	function get_projects()
	{
		return $this->projects;
	}
}
