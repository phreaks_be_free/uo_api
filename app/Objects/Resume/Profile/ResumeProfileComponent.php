<?php

namespace App\Objects\Resume\Profile;


use App\Objects\Models\Resume;
use App\Objects\Models\ResumeProfile;
use App\Objects\Models\ResumeProfileEducation;
use App\Objects\Models\ResumeProfileEmployment;
use App\Objects\Models\ResumeProfileReference;
use App\Objects\Models\ResumeProfileSkills;

use App\Objects\Abstracted\AbstractComponent;
use Illuminate\Database\Eloquent\Collection;


class ResumeProfileComponent extends AbstractComponent
{
	/** @var ResumeProfile */
	protected $database_model;

	protected $education = [];
	protected $employment = [];
	protected $references = [];
	protected $skills = [];

	/**
	 * @param int|null $pk
	 */

	public function __construct($pk = null)
	{
		parent::__construct('ResumeProfile', $pk);

		/** @var Collection education */
		$this->education  = (ResumeProfileEducation::where('resume_profile_id', $this->get_id())->get())->toArray();
		$this->employment = (ResumeProfileEmployment::where('resume_profile_id', $this->get_id())->get())->toArray();
		$this->references = (ResumeProfileReference::where('resume_profile_id', $this->get_id())->get())->toArray();
		$this->skills     = (ResumeProfileSkills::Where('resume_profile_id', $this->get_id())->get())->all();
	}

	/**
	 * @return bool
	 */
	public function save(): bool
	{
		return parent::default_save();
	}

	/**
	 * @return array
	 */
	public function output(): array
	{
		$skills = [];
		foreach ($this->skills as $profile_skill) {
			$profile_skill_array = [];
			$profile_skill_array['id'] = $profile_skill->getAttribute('id');
			$profile_skill_array['resume_skill'] = ($profile_skill->get_skill())->toArray();
			// skill progress
			$profile_skill_array['progress'] = $profile_skill->progress;


			$skills[] = $profile_skill_array;
		}
		return array_merge([
			'education'  => $this->education,
			'employment' => $this->employment,
			'references' => $this->references,
			'skills'     => $skills
		], parent::default_output());
	}


	public function populate(Resume $resume, string $full_name, string $email, string $title, $phone, string $objective, string $display_picture_url, string $biography, string $slider_text)
	{
		$this->database_model->resume_id = $resume->getAttribute('id');
		$this->database_model->full_name = $full_name;
		$this->database_model->title     = $title;
		$this->database_model->email     = $email;
		$this->database_model->phone     = $phone;
		$this->database_model->objective     = $objective;
		$this->database_model->display_picture_url = $display_picture_url;
		$this->database_model->biography = $biography;
		$this->database_model->slider_text = $slider_text;


		return $this;
	}

	public function get_resume_id() {
		return $this->get_model()->getAttribute('resume_id');
	}
}
