<?php

/**
 * Created by PhpStorm.
 * User: jfoy
 * Date: 2/9/19
 * Time: 10:10 AM
 */

namespace App\Objects\Resume;

use Illuminate\Support\Facades\DB;
use App\Objects\Models\ResumeSkill;
use App\Objects\Models\ResumeProfile;
use App\Objects\Abstracted\AbstractFactory;
use App\Objects\Resume\Models\ApplicationResume;
use App\Objects\Application\ApplicationComponent;
use App\Objects\Models\Resume;
use App\Objects\Models\ResumeProfileEducation;
use App\Objects\Models\ResumeProfileEmployment;
use App\Objects\Models\ResumeProfileReference;
use App\Objects\Models\ResumeProfileSkills;
use App\Objects\Models\ResumeProject;
use App\Objects\Models\ResumeProjectDisplay;
use App\Objects\Models\ResumeProjectSkills;
use App\Objects\Resume\Profile\ResumeProfileComponent;
use App\Objects\Resume\Project\ResumeProjectComponent;

class ResumeFactory extends AbstractFactory
{
	/** @var ApplicationResume */
	protected $resume_by_application;

	protected $components = [];

	function __construct()
	{
		parent::__construct('App\Objects\Resume\ResumeComponent');
	}


	/**
	 * @return Resumes[]
	 */
	public function get_resumes_with_no_profile(): array
	{
		$resumes = $this->get_components();
		$container = [];
		if (!empty($resumes)) {
			foreach ($resumes as $key) {
				if ($key->get_profile()->isEmpty()) {
					$container[] = $key;
				}
			}
		}
		return $container;
	}


	/**
	 * @param ApplicationComponent $application_component
	 *
	 * @return ResumeComponent
	 */
	public static function by_application(ApplicationComponent $application_component)
	{
		$resume_row = DB::table('resume')
			->join('applications', 'applications.id', '=', 'resume.application_id')
			->select('resume.*')
			->where('applications.id', $application_component->get_model()->getAttribute('id'))
			->where('resume.default', 1)
			->get();

		if (!method_exists($resume_row, 'first')) {
			return new ResumeComponent();
		}

		$resume = new ResumeComponent($resume_row->first());

		return $resume;
	}

	/**
	 * get resume by resume_profile
	 *
	 * @param ResumeProfileComponent $profile_component
	 * @return ResumeComponent
	 */
	public static function by_resume_profile(ResumeProfileComponent $profile_component)
	{
		$resume = new ResumeComponent($profile_component->get_model()->get_resume());
		return $resume;
	}



	/** Resume Skill */

	/**
	 * @param int $id
	 * @return ResumeSkill|null
	 */
	public static function get_skill(int $id = null)
	{
		if ($id) {
			$model = ResumeSkill::find($id);
		} else {
			$model = new ResumeSkill();
		}
		return $model;
	}

	/**
	 * @return ResumeSkills[]
	 */
	public static function get_skills(): array
	{
		return ResumeSkill::all()->toArray();
	}

	/**
	 * @param string $name
	 * @param string $tag
	 * @return ResumeSkill
	 */
	public static function populate_skill(ResumeSkill $model, string $name, string $tag): ResumeSkill
	{
		$model->name = $name;
		$model->tag = $tag;
		return $model;
	}



	/** Resume Profile */

	/**
	 * @param any $pk
	 * @return ResumeProfileComponent
	 */
	public static function get_profile($pk = null)
	{
		return new ResumeProfileComponent($pk);
	}

	/**
	 * @return ResumeProfileComponents[]
	 */
	public static function get_profiles(): array
	{
		$container = [];
		$profiles = ResumeProfile::all();
		foreach ($profiles as $key) {
			$profile = new ResumeProfileComponent($key);
			if (!$profile->isEmpty()) {
				$container[] = $profile;
			}
		}
		return $container;
	}


	/** Profile Skill */

	/**
	 * @param any $id
	 * @return ResumeProfileSkills|void
	 */
	public static function get_profile_skill(int $id = null)
	{
		if ($id) {
			$model = ResumeProfileSkills::find($id);
		} else {

			$model = new ResumeProfileSkills();
		}
		return $model;
	}


	/**
	 * @param ResumeProfileSkills $model
	 * @param ResumeProfile $resumeProfile
	 * @param ResumeSkill $resumeSkill
	 * @param int $progress
	 * @return ResumeProfileSkills
	 */
	public static function populate_profile_skill(ResumeProfileSkills $model, ResumeProfile $resumeProfile, ResumeSkill $resumeSkill, int $progress): ResumeProfileSkills
	{
		$model->resume_profile_id = $resumeProfile->getAttribute('id');
		$model->resume_skill_id = $resumeSkill->getAttribute('id');
		$model->progress = $progress;
		return $model;
	}



	/** Profile Education */


	/**
	 * @param any $id
	 * @return ResumeProfileEducation|null
	 */
	public static function get_profile_education(int $id = null)
	{
		if ($id) {
			$model = ResumeProfileEducation::find($id);
		} else {
			$model = ResumeProfileEducation();
		}
		return $model;
	}

	/**
	 * @param ResumeProfile $profile
	 * @param string $institute
	 * @param string $description
	 * @param date $started_at
	 * @param date $ended_at
	 * @param any $current
	 * @return ResumeProfileEducation
	 */
	public static function populate_profile_education(ResumeProfileEducation $model, ResumeProfile $profile, string $institute, string $description, string $started_at, string $ended_at, $current): ResumeProfileEducation
	{
		if (!empty($current)) {
			DB::table($model->getTable())->update(['current' => 0]);
		}
		$model->resume_profile_id = $profile->getAttribute('id');
		$model->institute = $institute;
		$model->description = $description;
		$model->started_at = $started_at;
		$model->ended_at = $ended_at;
		$model->current = empty($current) ? 0 : 1;
		return $model;
	}



	/** Profile Employment */

	/**
	 * @param int|null $id
	 * @return ResumeProfileEmployment
	 */
	public static function get_profile_employment(int $id = null)
	{
		if ($id) {
			$model = ResumeProfileEmployment::find($id);
		} else {
			$model = new ResumeProfileEmployment();
		}
		return $model;
	}

	/**
	 * @param ResumeProfileEmployment $model
	 * @param ResumeProfile $profile
	 * @param string $company_name
	 * @param string $position_description
	 * @param string $started_at
	 * @param string $ended_at
	 * @param any $current
	 * @return ResumeProfileEmployment
	 */
	public static function populate_profile_employment(ResumeProfileEmployment $model, ResumeProfile $profile, string $company_name, string $position_description, string $started_at, string $ended_at, $current): ResumeProfileEmployment
	{
		if (!empty($current)) {
			DB::table($model->getTable())->update(['current' => 0]);
		}
		$model->resume_profile_id = $profile->getAttribute('id');
		$model->company_name = $company_name;
		$model->position_description = $position_description;
		$model->started_at = $started_at;
		$model->ended_at = $ended_at;
		$model->current = empty($current) ? 0 : 1;
		return $model;
	}



	/** Profile Reference */

	/**
	 * @param int|null $id
	 * @return ResumeProfileReference
	 */
	public static function get_profile_reference(int $id = null)
	{
		if ($id) {
			$model = ResumeProfileReference::find($id);
		} else {
			$model = new ResumeProfileReference();
		}

		return $model;
	}

	/**
	 * @param ResumeProfileReference $model
	 * @param ResumeProfile $profile
	 * @param string $name
	 * @param string $email
	 * @param string $phone
	 * @param string $relationship
	 * @return ResumeProfileReference
	 */
	public static function populate_profile_reference(ResumeProfileReference $model, ResumeProfile $profile, string $name, string $email, string $phone, string $relationship): ResumeProfileReference
	{
		$model->resume_profile_id = $profile->getAttribute('id');
		$model->name = $name;
		$model->email = $email;
		$model->phone = $phone;
		$model->relationship = $relationship;
		return $model;
	}



	/** Resume Project */

	/**
	 * @param any $pk
	 * @return ResumeProjectComponent
	 */
	public static function get_project($pk = null)
	{
		$component = new ResumeProjectComponent($pk);
		return $component;
	}

	/**
	 * @return ResumeProjectComponents[]
	 */
	public static function get_projects(): array
	{
		$container = [];
		$projects = ResumeProject::all();
		foreach ($projects as $key) {
			$project = new ResumeProjectComponent($key);
			if (!$project->isEmpty()) {
				$container[] = $project;
			}
		}
		return $container;
	}


	/** Project Display */

/**
	 * @param int $id
	 * @return ResumeProjectDisplay|null
	 */
	public static function get_project_display(int $id = null)
	{
		if ($id) {
			$model = ResumeProjectDisplay::find($id);
		} else {
			$model = new ResumeProjectDisplay();
		}
		return $model;
	}

	/**
	 * @return ResumeProjectDisplay[]
	 */
	public static function get_project_displays(): array
	{
		return ResumeProjectDisplay::all()->toArray();
	}

	/**
	 * @param ResumeProjectDisplay $model
	 * @param string $name
	 * @param string $tag
	 * @return ResumeProjectDisplay
	 */
	public static function populate_project_display(ResumeProjectDisplay $model, string $name, string $tag): ResumeProjectDisplay
	{
		$model->name = $name;
		$model->tag = $tag;
		return $model;
	}



	/** Project Skill */

	/**
	 * @param any $id
	 * @return ResumeProjectSkills
	 */
	public static function get_project_skill(int $id = null)
	{
		if ($id) {
			$model = ResumeProjectSkills::find($id);
		} else {
			$model = new ResumeProjectSkills();
		}
		return $model;
	}

	/**
	 * @return ResumeProjectSkills[]
	 */
	public static function get_project_skills(): array
	{
		$project_skills = ResumeProjectSkills::all();
		$container = [];
		foreach ($project_skills as $skill) {
			$arr = [];
			$arr['resume_skill'] = ($skill->get_skill())->toArray();
			$arr = array_merge($skill->toArray(), $arr);
			$container[] = $arr;
		}
		return $container;
	}

	/**
	 * @param ResumeProjectSkills $model
	 * @param ResumeProject $project
	 * @param ResumeSkill $skill
	 * @return ResumeProjectSkills
	 */
	public static function populate_project_skill(ResumeProjectSkills $model, ResumeProject $project, ResumeSkill $skill): ResumeProjectSkills
	{
		$model->resume_project_id = $project->getAttribute('id');
		$model->resume_skill_id = $skill->getAttribute('id');
		return $model;
	}





	


	


	






















	


	
}
