<?php

namespace App\Objects\Resume\Project;


use App\Objects\Models\Resume;

use App\Objects\Abstracted\AbstractComponent;
use App\Objects\Models\ResumeProject;
use App\Objects\Models\ResumeProjectDisplay;
use App\Objects\Models\ResumeProjectSkills;
use Illuminate\Database\Eloquent\Collection;


class ResumeProjectComponent extends AbstractComponent
{
	/** @var ResumeProject */
	protected $database_model;

	protected $skills = [];

	/**
	 * @param int|null $pk
	 */

	public function __construct($pk = null)
	{
		parent::__construct('ResumeProject', $pk);

		/** @var array $skills */
		$this->skills = $this->database_model->get_skills();
	}

	/**
	 * @return bool
	 */
	public function save(): bool
	{
		return parent::default_save();
	}

	/**
	 * @return array
	 */
	public function output(): array
	{

		$output = parent::default_output();
		$output['display'] = ($this->database_model->get_display())->toArray();
		unset($output['resume_project_display_id']);

		$output['skills'] = [];

		/** @var ResumeProjectSkills $skill */
		foreach ($this->skills as $project_skill) {
			$arr = [];
			$arr['resume_skill'] = ($project_skill->get_skill())->toArray();
			$arr = array_merge($project_skill->toArray(), $arr);
			$output['skills'][] = $arr;
		}
		return $output;
	}

	/**
	 * @param Resume $resume
	 * @param string $title
	 * @param string $objective
	 * @param string $about_me
	 * @param $resume_display_id
	 * @param string $code
	 *
	 * @return $this
	 */
	public function populate(Resume $resume, ResumeProjectDisplay $resume_project_display, string $title, string $description, string $image_url, string $content)
	{
		$this->database_model->resume_id 					= $resume->getAttribute('id');
		$this->database_model->resume_project_display_id 	= $resume_project_display->getAttribute('id');
		$this->database_model->title             			= $title;
		$this->database_model->description         			= $description;
		$this->database_model->image_url          			= $image_url;
		$this->database_model->content              		= $content;

		return $this;
	}
}
