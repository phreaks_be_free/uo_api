<?php
/**
 * Created by PhpStorm.
 * User: jfoy
 * Date: 2/12/19
 * Time: 8:49 PM
 */

namespace App\Objects\InstanceInterpreter;


use App\Objects\Abstracted\AbstractComponent;
use App\Objects\Abstracted\AbstractInterpreter;
use App\Objects\Application\ApplicationComponent;
use App\Objects\Application\ApplicationFactory;
use App\Objects\InstanceInterpreter\Instance\InstanceComponent;
use App\Objects\InstanceInterpreter\Interpreter\InterpreterComponent;
use App\Objects\Models\InstanceInterpreter;
use App\Objects\Resume\ResumeComponent;
use App\Objects\Resume\ResumeFactory;

class InstanceInterpreterComponent {
	/** @var \App\Objects\Application\ApplicationComponent */
	protected $application;
	/** @var AbstractInterpreter|bool */
	protected $interpreter;
	/** @var bool|string */
	protected $instance_class_name;

	public function __construct( string $application_url, string $interpreter_name, array $injection_data = [] ) {
		/** @var ApplicationComponent application */
		$this->application = ApplicationFactory::get_application_by_url( $application_url );
		/** @var AbstractInterpreter interpreter */
		$this->interpreter = $this->application->get_interpreter( $interpreter_name, $injection_data );

		$this->instance_class_name = $this->application->get_instance_class();
	}

	/**
	 * @return bool|\Exception| mixed
	 */
	public function execute() {
		if ( $this->is_actionable() ) {
			try {
				$results = $this->interpreter->load_class( $this->instance_class_name );
			} catch ( \Exception $exception ) {
				return $exception;
			}

			return $results;
		}

		return new \Exception( 'InstanceInterpreter is not actionable' );
	}

	/**
	 * @return bool
	 */
	function is_actionable() {
		if ( is_a( $this->interpreter, '\App\Objects\Abstracted\AbstractInterpreter' ) &&
		     class_exists( (string) $this->instance_class_name ) ) {
			return true;
		}

		return false;
	}
}
