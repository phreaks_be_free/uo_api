<?php
/**
 * Created by PhpStorm.
 * User: jfoy
 * Date: 2/12/19
 * Time: 8:49 PM
 */

namespace App\Objects\InstanceInterpreter\Interpreter;


use App\Objects\Abstracted\AbstractComponent;
use App\Objects\Models\InstanceInterpreter;

class InterpreterComponent extends AbstractComponent {

	public function __construct( $pk_model = null ) {
		parent::__construct( 'InstanceInterpreter', $pk_model );
	}

	public function save(): bool {
		return $this->default_save();
	}

	public function output(): array {
		return $this->default_output();
	}

	public function load_class(array $data){

	}
}
