<?php
/**
 * Created by PhpStorm.
 * User: jfoy
 * Date: 2/12/19
 * Time: 8:16 PM
 */
namespace App\Objects\InstanceInterpreter\Interpreter;

use League\Flysystem\Exception;

class AbsCompAction extends \App\Objects\Abstracted\AbstractInterpreter {
	protected $id;
	protected $action;

	public function load_class( string $class_name ) {
		$function_name = $this->action;
		try{
			$class =  new $class_name($this->id);
			if(!method_exists($class,$function_name)){
				return new Exception('function does not exist');
			}

			$results = $class->$function_name();

		}catch (\Exception $exception){
			return $exception;
		}
		return $results;
	}
}
