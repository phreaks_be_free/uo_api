<?php
/**
 * Created by PhpStorm.
 * User: jfoy
 * Date: 2/12/19
 * Time: 8:49 PM
 */

namespace App\Objects\InstanceInterpreter\Instance;


use App\Objects\Abstracted\AbstractComponent;

class InstanceComponent extends AbstractComponent {

	public function __construct( $pk_model = null ) {
		parent::__construct( 'Instance', $pk_model );
	}

	public function save(): bool {
		return $this->default_save();
	}

	public function output(): array {
		return $this->default_output();
	}
}
