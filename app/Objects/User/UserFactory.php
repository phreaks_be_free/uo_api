<?php
/**
 * Created by PhpStorm.
 * User: Jason Foy
 * Date: 12/20/2016
 * Time: 8:45 PM
 */

namespace App\Objects\User;

use App\Objects\Models\User;
use App\Objects\Application\ApplicationFactory;

class UserFactory {

	/**
	 * @param int|null $int
	 *
	 * @return UserComponent
	 */
	public static function get( int $int = null ): UserComponent {
		return new UserComponent( $int );
	}

	/**
	 * @param User $user
	 *
	 * @return \App\Objects\User\UserComponent
	 */
	public static function get_by_model( User $user ): UserComponent {
		return new UserComponent( $user );
	}

	// /**
	//  * @param UserComponent|null $user
	//  *
	//  * @return array|bool
	//  * @throws \Exception
	//  */
	// public static function get_all_applications_by_user( UserComponent $user = null ) {
	// 	if ( $user === null ) {
	// 		$user = new UserComponent();
	// 	}
	// 	if ( $user->isEmpty() ) {
	// 		return false;
	// 	}
	// 	return( new ApplicationFactory )->get_applications_by_user( $user )->get_applications();
	// }
}
