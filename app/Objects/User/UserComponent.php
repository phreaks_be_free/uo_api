<?php

/**
 * Created by PhpStorm.
 * User: Jason Foy
 * Date: 12/20/2016
 * Time: 8:58 PM
 */

namespace App\Objects\User;

use App\Objects\Abstracted\AbstractComponent;
use App\Objects\Models\User;
use App\Objects\User\Application\UserApplicationFactory;


class UserComponent extends AbstractComponent {
	/** @var User */
	protected $database_model;

	/**
	 * UserComponent constructor.
	 *
	 * @param int|null $pk
	 */
	public function __construct( $pk = null ) {
		if ( empty( $pk ) ) {
			if ( ! empty( auth()->user() ) ) {
				$pk = ( auth()->user() )->getAuthIdentifier();
			}
		}
		parent::__construct( 'User', $pk );

	}

	/**
	 * @return bool
	 */
	public function save(): bool {
		return parent::default_save();
	}

	/**
	 * @return array
	 */
	public function output(): array {
		return parent::default_output();
	}

	/**
	 * @return array
	 * @return false
	 */
	public function get_applications() {
		if ( $this->isEmpty() ) {
			return false;
		}

		return $this->database_model->get_applications();
	}

}
