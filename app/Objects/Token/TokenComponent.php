<?php
/**
 * Created by PhpStorm.
 * User: Jason Foy
 * Date: 1/10/2017
 * Time: 9:04 AM
 */

namespace App\Objects\Token;


use App\Objects\Abstracted\AbstractComponent;
use App\Objects\Models\User;
use App\Objects\Models\UserToken;

class TokenComponent extends AbstractComponent {
	/**
	 * TokenComponent constructor.
	 *
	 * @param int|null $pk
	 */
	public function __construct( int $pk = null ) {
		parent::__construct( 'UserToken', $pk );
	}

	/**
	 * @return bool
	 */
	public function save(): bool {
		return parent::default_save();
	}

	/**
	 * @return array
	 */
	public function output(): array {
		return parent::default_output();
	}

	/**
	 * @return bool
	 */
	public function expire(): bool {
		$this->database_model->update( [
			'expire' => ( new \Datetime( 'now', new \DateTimeZone( 'UTC' ) ) ),
			'active' => 0
		] );
		$this->save();

		return true;
	}
}
