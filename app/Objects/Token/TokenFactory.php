<?php
/**
 * Created by PhpStorm.
 * User: Jason Foy
 * Date: 12/20/2016
 * Time: 8:45 PM
 */

namespace App\Objects\Factories;


use App\Objects\Token\TokenComponent;
use App\Objects\User\UserComponent;
use App\Objects\Models\User;
use App\Objects\Models\UserToken;
use Illuminate\Database\Eloquent\Collection;

class TokenFactory {

	protected $token_component;

	/**
	 * @param UserComponent $user
	 * @param ApplicationComponent $application_component
	 *
	 * @return TokenComponent|bool
	 */
	public static function get_token_by_user_and_application( UserComponent $user, ApplicationComponent $application_component ) {
		$user_id        = $user->get_id();
		$application_id = $application_component->get_id();
		if ( empty( $user_id ) || empty( $application_id ) ) {
			return false;
		}
		/** @var Collection $user_token_collection */
		$user_token_collection = UserToken::where( 'user_id', $user_id )
		                                  ->where( 'application_id', $application_id )
		                                  ->where( 'active', 1 )
		                                  ->take( 1 )
		                                  ->get();
		/** @var UserToken $user_token */
		if ( $user_token_collection->count() > 0 ) {
			$user_token    = $user_token_collection->first();
			$user_token_id = $user_token->getAttribute( 'id' );
		}

		return new TokenComponent( isset( $user_token_id ) ? $user_token_id : null );
	}

	/**
	 * @param UserComponent $user_component
	 * @param ApplicationComponent $application_component
	 *
	 * @return TokenComponent
	 * @throws \Exception
	 */
	public static function generate_token_by_user_and_application( UserComponent $user_component, ApplicationComponent $application_component ) {
		$token = ( new UserToken( [
			'user_id'        => $user_component->get_id(),
			'application_id' => $application_component->get_id(),
			'active'         => 1,
			'expires'        => ( ( new \DateTime( 'now', new \DateTimeZone( 'UTC' ) ) )->modify( '+ 60 days' ) ),
		] ) )->generateToken();

		return new TokenComponent( $token->getAttribute( 'id' ) );
	}

	/**
	 * @param $token
	 *
	 * @return bool
	 */
	public function getUserByToken( $token ) {
		$token = UserToken::where( 'active', 1 )
		                  ->where( 'expires', '>', ( new \DateTime( 'now' ) )->format( 'Y-m-d H:i:s' ) )
		                  ->where( 'token', $token )
		                  ->take( 1 )
		                  ->get();
		if ( empty( $token->first() ) ) {
			return false;
		}

		return $token->first()->user;
	}

}
