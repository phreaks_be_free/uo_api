<?php
/**
 * Created by PhpStorm.
 * User: jfoy
 * Date: 3/9/19
 * Time: 3:54 PM
 */

namespace App\Objects\Shopify;


use App\Objects\Abstracted\AbstractComponent;
use App\Objects\Application\ApplicationComponent;
use App\Objects\Models\ShopifyAccounts;
use GuzzleHttp\Client;
use Illuminate\Support\Collection;

class ShopifyAccountComponent extends AbstractComponent {
	/** @var ShopifyAccounts */
	protected $database_model;
	private $api_key = '1f30a5a817748503f44188cb3af5b69f';
	public static $url = 'https://%s:%s@%s/admin/%s.json';


	public function __construct( $spotify_account_id = null ) {
		parent::__construct( 'ShopifyAccounts', $spotify_account_id );
	}

	/**
	 * @param string $shop
	 *
	 * @return ShopifyAccountComponent
	 */
	public function load_by_shop_url( string $shop, $nonce = null ): self {
		$shop = str_replace( 'http://', '', str_replace( 'https://', '', $shop ) );

		if ( strpos( $shop, '.myshopify.com' ) === false ) {
			return $this;
		}

		$model = ShopifyAccounts::Where( 'shop', $shop )->take( 1 )->get()->first();

		$this->load( 'ShopifyAccounts', $model );

		return $this;
	}


	protected function call( string $resource ) {
		$client = new Client();

		$response = $client->request(
			'GET',
			$this->get_resource($resource),
			[
				'query' => [
					//'fields' => 'id,images,title,variants',
					'access_token' => $this->database_model->getAttribute('token')
				]
			]
		);

		$result = json_decode($response->getBody()->getContents(), true);
		$test = 'derp';
		return $result;
	}

	/**
	 * @param string $resource Shopify resource ('getProducts','getUsers")
	 *
	 * @return string
	 */
	protected function get_resource( string $resource ) {
		return sprintf(
			self::$url,
			$this->api_key,
			$this->database_model->getAttribute( 'token' ),
			$this->database_model->getAttribute( 'shop' ),
			$resource
		);
	}

	public function save(): bool {
		return parent::default_save();
	}

	public function output(): array {
		return parent::default_output();
	}

	/**
	 * @return string|null
	 */
	public function get_store() {
		return $this->database_model->getAttribute( 'store' );
	}


	public function get_products() {
		return $this->call( 'products' );
	}

	/**
	 * @param string $shop
	 * @param ApplicationComponent $application_component
	 *
	 * @return string
	 */
	public function register_store( string $shop, ApplicationComponent $application_component ): string {
		$this->database_model->setAttribute( 'shop', $shop );
		$this->database_model->setAttribute( 'application_id', $application_component->get_id() );

		if ( ! $this->save() ) {
			return false;
		}

		return true;
	}

	/**
	 * @param string $access_code
	 * @param string $shop
	 *
	 * @return bool
	 */
	public function complete_registration( string $access_code, string $shop ) {
		$this->database_model->setRawAttributes( [
			'token' => $access_code,
			'shop'  => $shop
		] );

		if(!$this->save()){
			return false;
		}
		return true;
	}

	/**
	 * @return string
	 */
	public function get_install_url( string $shop = null ) {
		$shop         = ( empty( $shop ) ? $this->database_model->getAttribute( 'shop' ) : $shop );
		$url          = "https://{$_SERVER['HTTP_HOST']}/application/shopify/complete";
		$redirect_uri = urlencode( $url );
		$scopes       = "read_orders,read_content,write_content,read_themes,write_themes,read_products,read_product_listings,read_script_tags,write_script_tags";

		return "https://{$shop}/admin/oauth/authorize?" .
		       "client_id={$this->api_key}&" .
		       "scope={$scopes}&" .
		       "redirect_uri={$redirect_uri}";
	}
}
