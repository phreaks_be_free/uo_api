<?php


namespace App\Objects\Shopify;


use App\Objects\Models\ShopifyAccounts;

class ShopifyComponent {


	protected $shopify_account;

	protected $call;

	public function __construct( $pk ) {
		$this->shopify_account = new ShopifyAccountComponent( $pk );
	}

	public function get_id() {
		$this->shopify_account->get_id();
	}

	public function get_account() {
		return $this->shopify_account;
	}


}
