<?php

namespace App\Objects\FeaturedItems;

use App\Objects\User\UserComponent;
use App\Objects\Models\FeaturedItems;
use App\Objects\Abstracted\AbstractComponent;
use App\Objects\Application\ApplicationComponent;

class FeaturedItemComponent extends AbstractComponent {
	/** @var FeaturedItems */
	protected $database_model;

	/**
	 * @param int|null $pk
	 */

	public function __construct( $pk = null ) {
		parent::__construct( 'FeaturedItems', $pk );
	}

	/**
	 * @return bool
	 */
	public function save(): bool {
		return parent::default_save();
	}

	/**
	 * @return array
	 */
	public function output(): array {
		return parent::default_output();
	}

	// /**
	//  * @return bool|null
	//  * @throws \Exception
	//  */
	// public function destroy() {
	// 	return $this->database_model->delete();
	// }

	/**
	 * @param ApplicationComponent $app
	 * @param UserComponent $user
	 * @param array $items
	 *
	 * @return $this|bool
	 * @throws \Exception
	 */
	public function populate( ApplicationComponent $app, UserComponent $user, array $items ) {
		if ( $user->isEmpty() || $app->isEmpty() || empty( $items ) ) {
			return false;
		}
		$this->database_model->application_id = $app->get_id();
		$this->database_model->user_id        = $user->get_id();
		$this->database_model->items          = json_encode( $items );
		$this->database_model->updated_at     = ( new \DateTime( 'now', new \DateTimeZone( 'UTC' ) ) );

		if ( empty( $this->database_model->getAttribute( 'id' ) ) ) {
			$this->database_model->created_at = new \DateTime( 'now', new \DateTimeZone( 'utc' ) );
		}

		return $this;
	}

	/**
	 * @return ApplicationComponent
	 */
	public function get_application() {
		return $this->database_model->get_application();
	}

}
