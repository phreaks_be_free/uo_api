<?php

/**
 * Created by PhpStorm.
 * User: jfoy
 * Date: 10/27/18
 * Time: 6:31 PM
 */

namespace App\Objects\Abstracted;


abstract class AbstractComponent
{

	/** @var \Illuminate\Database\Eloquent\Model */
	protected $database_model;

	/**
	 * @return bool
	 */
	abstract function save(): bool;

	/**
	 * @return array
	 */
	abstract function output(): array;

	/**
	 * @return bool
	 */
	protected function default_save(): bool
	{
		return $this->database_model->save();
	}

	/**
	 * @return array
	 */
	protected function default_output(): array
	{
		return $this->database_model->getAttributes();
	}

	/**
	 * @return int
	 */
	public function get_id()
	{
		return $this->database_model->getAttribute('id');
	}

	/**
	 * @return bool|null
	 * @throws \Exception
	 */
	public function destroy(): bool
	{
		return $this->database_model->delete();
	}

	/**
	 * @return \Illuminate\Database\Eloquent\Model
	 */
	public function get_model()
	{
		return $this->database_model;
	}

	/**
	 * AbstractComponent constructor.
	 *
	 * @param string $model_name
	 * @param int|object $pk_model
	 */
	public function __construct(string $model_name, $pk_model = null)
	{
		$this->load($model_name, $pk_model);
	}

	/**
	 * @param string $model_name
	 * @param null $pk_model
	 *
	 * @return bool
	 */
	protected function load(string $model_name, $pk_model = null)
	{
		$class = 'App\Objects\Models\\' . $model_name;
		if (class_exists($class)) {

			if (is_object($pk_model) && !empty($pk_model) && get_class($pk_model) === $class) {
				/** @var $pk_model classmodel */
				$model = $pk_model;
			} elseif (is_object($pk_model) && !empty($pk_model) && get_class($pk_model) === 'stdClass') {

				/** @var $pk_model \stdClass */
				$collection = $class::where('id', @$pk_model->id)
					->take(1)
					->get();
				$model = $collection->find(@$pk_model->id);

				if ($model === null) {
					$model = new $class();
				}
			} elseif (is_integer($pk_model)) {
				/** @var $pk_model integer */
				$collection = $class::where('id', $pk_model)
					->take(1)
					->get();

				$model = $collection->find($pk_model);

				if ($model === null) {
					$model = new $class();
				}
			} else {
				/** @var $pk_model null */
				$model = new $class();
			}

			if (!empty($model) && is_a($model, $class)) {
				$this->database_model = $model;

				return true;
			}

			return false;
		}
	}

	/**
	 * @return bool
	 */
	public function isEmpty(): bool
	{
		return empty($this->database_model->getAttribute('id'));
	}
}
