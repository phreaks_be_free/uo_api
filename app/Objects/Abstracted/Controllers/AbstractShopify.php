<?php


namespace App\Objects\Abstracted\Controllers;

use App\Http\Controllers\Controller;
use App\Objects\Models\ShopifyAccounts;
use App\Objects\Shopify\ShopifyAccountComponent;
use App\Objects\Shopify\ShopifyComponent;
use http\Client;

abstract class AbstractShopify extends Controller {
	protected $api_key = '1f30a5a817748503f44188cb3af5b69f';

	protected $api_secret = '65c6550b660205b486ef399312093042';

	public function complete() {
		if ( ! isset( $_GET['code'], $_GET['hmac'], $_GET['shop'], $_GET['timestamp'] ) ) {
			exit; //or redirect to an error page
		}
		$hmac   = $_GET['hmac'];
		$shop   = $_GET['shop'];
		$params = [
			"code"      => $_GET['code'],
			"shop"      => $shop,
			"timestamp" => $_GET['timestamp'],
		];

		$calculated_hmac = hash_hmac( 'sha256', http_build_query( $params ), $this->api_secret );
		if ( $hmac == $calculated_hmac ) {

			$client   = new \GuzzleHttp\Client();
			$response = $client->request(
				'POST',
				"https://{$shop}/admin/oauth/access_token",
				[
					'form_params' => [
						'client_id'     => $this->api_key,
						'client_secret' => $this->api_secret,
						'code'          => $_GET['code']
					]
				]
			);

			$data         = json_decode( $response->getBody()->getContents(), true );
			$access_token = $data['access_token'];

			$shopify = ( new ShopifyAccountComponent() )->load_by_shop_url( $shop );

			$shopify->complete_registration( $access_token, $shop);
		}


		$test = 'hello';
		exit;
	}
}
