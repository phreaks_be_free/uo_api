<?php
/**
 * Created by PhpStorm.
 * User: jfoy
 * Date: 2/12/19
 * Time: 8:21 PM
 */

namespace App\Objects\Abstracted;


abstract class AbstractInterpreter {

	public function __construct( array $array = null ) {
		if ( ! is_array( $array ) ) {
			$array = (array) $array;
		}
		foreach ( $array as $field => $value ) {
			if ( property_exists( get_class( $this ), $field ) ) {
				$this->$field = $value;
			}
		}
	}

	abstract function load_class( string $class_name );
}
