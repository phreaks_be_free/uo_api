<?php

/**
 * Created by PhpStorm.
 * User: jfoy
 * Date: 2/12/19
 * Time: 5:30 PM
 */

namespace App\Objects\Abstracted;

use Illuminate\Database\Eloquent\Collection;


class AbstractFactory
{

	protected $child_component_class;
	protected $child_model_class;
	/** TODO Build out constructor passing collection and build class models in abstract factory for default uses 
	 * Done ;)
	 */
	protected $components = [];

	function __construct($component = null)
	{
		if (!$component) {
			return false;
		};
		if ($this->load($component)) {
			$this->components = $this->generate_components($this->child_model_class::all());
		};
	}

	public function get($pk = null)
	{
		return (new $this->child_component_class($pk));
	}

	protected function load($component)
	{
		if (!empty($component)) {
			$abstract_component_class = 'App\Objects\Abstracted\AbstractComponent';
			if (is_string($component) && class_exists($component)) {
				$component_instance = new $component();
			} elseif (is_object($component)) {
				$component_instance = new $component();
			} else {
				$component_instance = false;
			}

			if ($component_instance && is_a($component_instance, $abstract_component_class)) {
				$this->child_component_class = get_class($component_instance);
				$this->child_model_class = get_class($component_instance->get_model());
				return true;
			}
			return false;
		}
		return false;
	}

	protected function generate_components(Collection $collection): array
	{
		$container = [];
		foreach ($collection as $key) {
			$component = new $this->child_component_class($key);
			if (!$component->isEmpty()) {
				$container[] = $component;
			}
		}
		return $container;
	}

	public function get_components()
	{
		return $this->components;
	}

	public function get_components_output(): array
	{
		$container = [];
		$container = array_map(function ($component) {
			return $component->output();
		}, $this->components);
		return $container;
	}
}
