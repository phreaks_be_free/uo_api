<?php
/**
 * Created by PhpStorm.
 * User: Jason Foy
 * Date: 12/20/2016
 * Time: 9:06 PM
 */

namespace App\Objects\Models;


use Illuminate\Database\Eloquent\Model;

class UserMetaTypes extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name','description'
    ];

}