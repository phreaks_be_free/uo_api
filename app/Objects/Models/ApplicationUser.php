<?php

namespace App\Objects\Models;

use Illuminate\Database\Eloquent\Model;

class ApplicationUser extends Model
{
    protected $table = 'application_users';
    protected $fillable = [
        'role',
        'first_name',
        'last_name',
        'email',
        'application_id',
    ];

}
