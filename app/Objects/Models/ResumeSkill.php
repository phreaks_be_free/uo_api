<?php

namespace App\Objects\Models;

use Illuminate\Database\Eloquent\Model;

class ResumeSkill extends Model
{
    public $timestamps = false;
}
