<?php

namespace App\Objects\Models;

use Illuminate\Database\Eloquent\Model;

class ResumeProfileSkills extends Model
{
	public function get_skill()
	{
		return $this->hasone('App\Objects\Models\ResumeSkill', 'id', 'resume_skill_id')->getResults();
	}
	public $timestamps = false;
}
