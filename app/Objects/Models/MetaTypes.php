<?php

namespace App\Objects\Models;

use Illuminate\Database\Eloquent\Model;

class MetaTypes extends Model
{
	protected $table = 'meta_types';

	protected $fillable = [
		'type',
		'object_manager'
	];
}
