<?php

namespace App\Objects\Models;

use Illuminate\Database\Eloquent\Model;

class ResumeProfileEmployment extends Model
{
    public $timestamps = false;
}
