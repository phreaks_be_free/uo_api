<?php

namespace App\Objects\Models;

use App\Objects\InstanceInterpreter\Instance\InstanceComponent;
use App\Objects\InstanceInterpreter\Interpreter\InterpreterComponent;
use Illuminate\Database\Eloquent\Model;

class InstanceInterpreter extends Model {
	/**
	 * @return InterpreterComponent
	 */
	public function get_interpreter(): InterpreterComponent {
		return ( new InterpreterComponent(
			$this->hasOne( 'App\Objects\Models\Interpreter', 'id', 'interpreter_id' )->getResults() ) );
	}

	/**
	 * @return InstanceComponent
	 */
	public function get_instance(): InstanceComponent {
		return ( new InstanceComponent(
			$this->hasOne( 'App\Objects\Models\Instance', 'id', 'instance_id' )->getResults()
		) );
	}
}
