<?php

namespace App\Objects\Models;

use App\Objects\Resume\ResumeComponent;
use App\Objects\Resume\ResumeFactory;
use App\Objects\Shopify\ShopifyComponent;
use Illuminate\Support\Facades\DB;
use App\Objects\User\UserComponent;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Collection;
use App\Objects\Abstracted\AbstractInterpreter;
use App\Objects\ApplicationUser\ApplicationUserFactory;
use App\Objects\Application\Roles\RoleComponent;

class Application extends Model {
	protected $table = 'applications';

	protected $fillable = [
		'name',
		'active',
		'user_id',
		'url',
	];

	/**
	 * @param string $interpreter_name
	 * @param array|null $injection_data
	 *
	 * @return AbstractInterpreter|bool
	 */
	private function check_for_approved_interpreter( string $interpreter_name, array $injection_data = null ) {
		/** @var Collection $approved */
		$approved   = DB::table( 'application_instances' )
		                ->join( 'instance_interpreters', 'instance_interpreters.id', '=', 'application_instances.instance_interpreter_id' )
		                ->join( 'interpreters', 'interpreters.id', 'instance_interpreters.interpreter_id' )
		                ->select( '*' )
		                ->where( 'application_id', $this->getAttribute( 'id' ) )
		                ->where( 'interpreters.name', $interpreter_name )
		                ->get();
		$class_info = $approved->first();
		if ( empty( $class_info ) ) {
			return false;
		}
		if ( ! class_exists( $class_info->path ) ) {
			return false;
		}
		/** @var AbstractInterpreter $interpreter */
		$interpreter = new $class_info->path( $injection_data );

		return $interpreter;
	}

	/**
	 * @param string $interpreter_name
	 * @param array|null $injection_data
	 *
	 * @return AbstractInterpreter|bool
	 */
	public function get_interpreter( string $interpreter_name, array $injection_data = null ) {
		return $this->check_for_approved_interpreter( $interpreter_name, $injection_data );
		//$this->hasMany( 'App\Objects\Models\ApplicationInstance', 'application_id', 'id' )->getResults();
	}

	/**
	 * @return UserComponent
	 */
	public function get_user(): UserComponent {
		return ( new UserComponent(
			$this->hasOne( 'App\Objects\Models\User', 'id', 'user_id' )->getResults()
		) );
	}


	/**
	 * @return array
	 *
	 */
	public function get_application_users(): array {
		return ( new ApplicationUserFactory( $this->hasMany( 'App\Objects\Models\ApplicationUser', 'application_id' )->getResults() ) )->get_applications();
	}

	/**
	 * @return array
	 */
	public function get_roles(): array {
		$role_models = DB::table( 'applications' )
		                 ->join( 'application_roles', 'applications.id', '=', 'application_roles.application_id' )
		                 ->select( '*' )
		                 ->where( 'application_id', $this->getAttribute( 'id' ) )
		                 ->get();
		$roles       = [];
		foreach ( $role_models as $role ) {
			$roles[] = new RoleComponent( $role );
		}

		return $roles;
	}

	/**
	 * @return array
	 */
	public function get_shopify(): array {
		$collection = $this->hasMany( 'App\Objects\Models\ShopifyAccounts', 'application_id' )
		                   ->getResults();
		$sa         = [];
		foreach ( $collection as $shopify_model ) {
			$sa[] = new ShopifyComponent( $shopify_model );
		}

		return $sa;
	}

	/**
	 * @return bool|string
	 */
	public function get_instance_class() {
		$approved   = DB::table( 'application_instances' )
		                ->join( 'instance_interpreters', 'instance_interpreters.id', '=', 'application_instances.instance_interpreter_id' )
		                ->join( 'instances', 'instances.id', 'instance_interpreters.instance_id' )
		                ->select( '*' )
		                ->where( 'application_id', $this->getAttribute( 'id' ) )
		                ->get();
		$class_info = $approved->first();
		if ( empty( $class_info ) ) {
			return false;
		}
		/** @var string $class_name */
		$class_name = $class_info->path;

		return $class_name;
	}
}
