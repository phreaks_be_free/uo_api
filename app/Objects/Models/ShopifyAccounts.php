<?php

namespace App\Objects\Models;

use Illuminate\Database\Eloquent\Model;

class ShopifyAccounts extends Model
{
	protected $table = 'shopify_accounts';

	protected $fillable = [
		'application_id',
		'username',
		'token',
		'name',
		'shop',
		'created_at',
		'updated_at'
	];



}
