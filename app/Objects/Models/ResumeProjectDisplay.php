<?php

namespace App\Objects\Models;

use Illuminate\Database\Eloquent\Model;

class ResumeProjectDisplay extends Model
{
    public $timestamps = false;

    function get_projects()
    {
        return $this->hasMany('App\Objects\Models\ResumeProject', 'resume_project_display_id', 'id')->getResults();
    }
}
