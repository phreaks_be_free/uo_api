<?php

namespace App\Objects\Models;

use App\Objects\Resume\Lineup\Attributes\Attributes;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;

class ResumeLineup extends Model {
	protected $table = 'resume_lineup';

	protected $fillable = [
		'resume_id',
		'resume_attribute_id',
		'resume_attribute_value'
	];

	public function attributes(): Attributes {
		return Attributes::get_by_has_one( $this->hasOne( 'App\Objects\Models\ResumeAttributes', 'id', 'resume_attribute_id' ) );
	}
}
