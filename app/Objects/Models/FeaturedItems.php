<?php

namespace App\Objects\Models;

use Illuminate\Database\Eloquent\Model;
use App\Objects\Application\ApplicationComponent;

class FeaturedItems extends Model
{
    protected $table = 'featured_items';

    protected $fillable = [
        'application_id',
        'items',
        'user_id',
    ];

    /**
     * @return ApplicationComponent
     */
    public function get_application(): ApplicationComponent
    {
        return (new ApplicationComponent(
            $this->hasOne('App\Objects\Models\Application', 'id', 'application_id')->getResults()
        ));
    }
}
