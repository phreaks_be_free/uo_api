<?php

namespace App\Objects\Models;

use App\Objects\Resume\Lineup\LineupFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class ResumeProfile extends Model {
	protected $table = 'resume_profiles';

	protected $fillable = [
		'full_name',
		'title',
		'email',
		'phone',
		'resume_id'
	];
	public $timestamps = false;
	
	public function get_resume()
	{
		return $this->hasOne('App\Objects\Models\Resume', 'id', 'resume_id')->getResults();
	}

}
