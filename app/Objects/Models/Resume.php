<?php

namespace App\Objects\Models;

use App\Objects\Resume\Lineup\LineupFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;

class Resume extends Model
{
	protected $table = 'resume';

	// protected $fillable = [
	// 	'user_id',
	// 	'name',
	// ];

	// public function lineup(): LineupFactory {
	// 	return new LineupFactory($this->hasMany( 'App\Objects\Models\ResumeLineup' )->getResults());
	// }
	public function get_profile()
	{
		return $this->hasOne('App\Objects\Models\ResumeProfile', 'resume_id', 'id')->getResults();
	}
}
