<?php

namespace App\Objects\Models;

use Illuminate\Database\Eloquent\Model;

class ResumeProfileEducation extends Model
{
    public $timestamps = false;
}
