<?php

namespace App\Objects\Models;

use Illuminate\Database\Eloquent\Model;

class Roles extends Model
{
	protected $fillable = [
		'role_id',
		'application_id',
	];
}
