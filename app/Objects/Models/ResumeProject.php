<?php

namespace App\Objects\Models;

use Illuminate\Database\Eloquent\Model;

class ResumeProject extends Model
{
	public $timestamps = false;

	public function get_display()
	{
		return $this->hasone('App\Objects\Models\ResumeProjectDisplay', 'id', 'resume_project_display_id')->getResults();
	}

	public function get_skills()
	{
		return $this->hasMany('App\Objects\Models\ResumeProjectSkills', 'resume_project_id', 'id')->getResults();
	}
}
