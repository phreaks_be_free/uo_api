<?php

namespace App\Objects\Models;

use Illuminate\Notifications\Notifiable;
use App\Objects\Application\ApplicationFactory;
use App\Objects\Application\ApplicationComponent;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name', 'last_name', 'email', 'password', 'active',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

	/**
	 * @return ApplicationComponent
	 */
    public function get_applications(): array
    {
        return (new ApplicationFactory(
            $this->hasMany('App\Objects\Models\Application', 'user_id', 'id')->getResults()
            ))->get_applications();
    }
}
