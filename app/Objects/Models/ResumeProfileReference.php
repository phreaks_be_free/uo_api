<?php

namespace App\Objects\Models;

use Illuminate\Database\Eloquent\Model;

class ResumeProfileReference extends Model
{
    public $timestamps = false;
}
