<?php

namespace App\Objects\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class ResumeProjectSkills extends Model
{
	public $timestamps = false;
	public function get_skill()
	{
		return $this->hasOne('App\Objects\Models\ResumeSkill', 'id', 'resume_skill_id')->getResults();
	}
	public function get_project()
	{
		return $this->hasOne('App\Objects\Models\ResumeProject', 'id', 'resume_project_id')->getResults();
	}
}
