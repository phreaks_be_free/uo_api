<?php

/**
 * Created by PhpStorm.
 * User: jfoy
 * Date: 2/11/19
 * Time: 8:16 PM
 */

namespace App\Objects\Application;

use App\Objects\Abstracted\AbstractFactory;
use App\Objects\Application\ApplicationComponent;
use App\Objects\Models\Application;
use App\Objects\User\UserComponent;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\DB;
use App\Objects\User\Application\UserApplicationFactory;

class ApplicationFactory extends AbstractFactory
{

	/**
	 * @var array $models
	 */
	protected $models = [];


	public function __construct(Collection $collection = null)
	{
		parent::__construct('App\Objects\Application\ApplicationComponent');
		if($collection && count($collection)) {
			$this->models = $collection;
		}
	}


	/**
	 * @param Application $application
	 * @return ApplicationComponent
	 */
	public static function get_by_model(Application $application): ApplicationComponent
	{
		return new ApplicationComponent($application);
	}


	/**
	 * @param string $url
	 * @return ApplicationComponent
	 */
	public static function get_application_by_url(string $url): ApplicationComponent
	{
		/** @var Collection $app_data */
		$app_data = Application::where('url', $url)
			->take(1)
			->get();

		return new ApplicationComponent($app_data->first());
	}


	/**
	 * @return array
	 */
	public function get_applications(): array
	{
		return $this->generate_components($this->models);
	}
}
