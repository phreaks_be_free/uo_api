<?php

namespace App\Objects\Application;

use App\Objects\Models\Application;
use App\Objects\Models\ApplicationRoles;
use App\Objects\Models\Roles;
use App\Objects\Resume\ResumeFactory;
use App\Objects\User\UserComponent;
use App\Objects\Abstracted\AbstractComponent;
use App\Objects\ApplicationUser\ApplicationUserFactory;

class ApplicationComponent extends AbstractComponent {
	/** @var Application */
	protected $database_model;

	public function get_roles() {
		return $this->database_model->get_roles();
	}

	/**
	 * @return Application
	 */
	public function get_model() {
		return $this->database_model;
	}

	/**
	 * @param int $role_id
	 *
	 * @return bool
	 */
	public function add_role( int $role_id ) {
		$application_role = new ApplicationRoles();
		/** @var Roles $role */
		$role = Roles::where( [ 'id' => $role_id ] )->take( 1 )->get();

		if ( empty( $role->first() ) ) {
			return false;
		}
		$application_role->role_id        = $role->first()->getAttribute( 'id' );
		$application_role->application_id = $this->database_model->getAttribute( 'id' );
		$application_role->timestamps     = false;
		$application_role->save();

		return true;
	}

	/**
	 * @param int|null $pk
	 */

	public function __construct( $pk = null ) {
		parent::__construct( 'Application', $pk );
	}

	/**
	 * @return bool
	 */
	public function save(): bool {
		return parent::default_save();
	}

	/**
	 * @return array
	 */
	public function output(): array {
		return parent::default_output();
	}

	/**
	 * @param string $interpreter_name
	 * @param array|null $injection_data
	 *
	 * @return \App\Objects\Abstracted\AbstractInterpreter|bool
	 */
	public function get_interpreter( string $interpreter_name, array $injection_data = null ) {
		return $this->database_model->get_interpreter( $interpreter_name, $injection_data );
	}

	/**
	 * @return bool|string
	 */
	public function get_instance_class() {
		return $this->database_model->get_instance_class();
	}

	/**
	 * @return UserComponent
	 */
	public function get_user() {
		return $this->database_model->get_user();
	}

	/**
	 * @param UserComponent $user
	 * @param string $name
	 * @param string $url
	 * @param int $active
	 *
	 * @return $this
	 * @throws \Exception
	 */
	public function populate( UserComponent $user, string $name, string $url, int $active ) {
		$this->database_model->user_id = $user->get_id();
		$this->database_model->name    = $name;
		$this->database_model->url     = $url;
		$this->database_model->active  = $active;
		if ( $this->isEmpty() ) {
			$this->database_model->created_at = new \DateTime( 'now', new \DateTimeZone( 'utc' ) );
		}
		$this->database_model->updated_at = ( new \DateTime( 'now', new \DateTimeZone( 'UTC' ) ) );


		return $this;
	}

	/**
	 * @return array|bool
	 */
	public function get_application_users() {
		if ( $this->isEmpty() ) {
			return false;
		}

		return $this->database_model->get_application_users();
	}

	/**
	 * @param array $values
	 *
	 * @return bool
	 */
	public function store_application_user( array $values ) {
		$component = ApplicationUserFactory::get();
		if ( ! $component->isEmpty() || empty( $values ) || ! is_array( $values ) ) {
			return false;
		}
		$values['application_id'] = $this->get_id();
		$component->populate( $values );
		if ( $component->save() ) {
			return true;
		}

		return false;
	}


}
