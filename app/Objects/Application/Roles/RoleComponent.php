<?php

namespace App\Objects\Application\Roles;


use App\Objects\Abstracted\AbstractComponent;
use App\Objects\Models\Roles;

class RoleComponent extends AbstractComponent {
	/** @var Roles */
	protected $database_model;

	public function get_roles() {
		$this->database_model->get_roles();
	}

	/**
	 * @param int $role_id
	 *
	 * @return bool
	 */
	public function add_role( int $role_id ) {
		$application_role = new ApplicationRoles();
		/** @var Roles $role */
		$role = Roles::where( [ 'id' => $role_id ] )->take(1)->get();

		if ( empty( $role->first() ) ) {
			return false;
		}
		$application_role->role_id = $role->first()->getAttribute( 'id' );
		$application_role->application_id = $this->database_model->getAttribute( 'id' );
		$application_role->timestamps = false;
		$application_role->save();

		return true;
	}

	/**
	 * RoleComponent constructor.
	 *
	 * @param null|\stdClass |model $pk
	 */
	public function __construct( $pk = null ) {
		parent::__construct( 'ApplicationRoles', $pk );
	}

	/**
	 * @return bool
	 */
	public function save(): bool {
		return parent::default_save();
	}

	/**
	 * @return array
	 */
	public function output(): array {
		return parent::default_output();
	}



	/**
	 * @param UserComponent $user
	 * @param string $name
	 * @param string $url
	 * @param int $active
	 *
	 * @return $this
	 * @throws \Exception
	 */
	public function populate( UserComponent $user, string $name, string $url, int $active ) {
//		$this->database_model->user_id = $user->get_id();
//		$this->database_model->name    = $name;
//		$this->database_model->url     = $url;
//		$this->database_model->active  = $active;
//		if ( $this->isEmpty() ) {
//			$this->database_model->created_at = new \DateTime( 'now', new \DateTimeZone( 'utc' ) );
//		}
//		$this->database_model->updated_at = ( new \DateTime( 'now', new \DateTimeZone( 'UTC' ) ) );


		return $this;
	}

}
