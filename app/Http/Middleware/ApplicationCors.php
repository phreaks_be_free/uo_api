<?php

namespace App\Http\Middleware;

use App\Objects\Application\ApplicationComponent;
use App\Objects\Application\ApplicationFactory;
use Closure;
use http\Env\Response;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\View;

class ApplicationCors {

	public function handle( $request, Closure $next ) {
		/** CORS SUPPORT */
		if ( $_SERVER['REQUEST_METHOD'] === 'OPTIONS' ) {
			return \Illuminate\Support\Facades\Response::make( View::make( 'welcome' ), 200 )
			                                           ->header( 'Access-Control-Allow-Origin', '*' )
			                                           ->header( 'Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS' )
			                                           ->header( 'Access-Control-Allow-Headers', ' Origin, Content-Type, Accept, Authorization, X-Request-With, cache-control,postman-token, token, access-control-allow-origin' )
			                                           ->header( 'Access-Control-Allow-Credentials', ' true' );
		}

		$url = Input::get( 'url', null );
		if ( empty( $url ) ) {
			$url = $request->input( 'url' );
		}

		if ( empty( $url ) ) {
			return redirect( '/' );
		}
		$application = ApplicationFactory::get_application_by_url( rtrim( $url, '/' ) );

		if ( ! is_a( $application, 'App\Objects\Application\ApplicationComponent' ) || $application->isEmpty() ) {
			return redirect( '/' );
		}
		/** @var Get Application URL from DB */
		$url = $application->get_model()->getAttribute( 'url' );

		if ( ! $url ) {
			return redirect( '/' );
		}

		return $next( $request )
			->header( 'Access-Control-Allow-Origin', rtrim( $url, '/' ) )
			->header( 'Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS' )
			->header( 'Access-Control-Allow-Headers', ' Origin, Content-Type, Accept, Authorization, X-Request-With, cache-control,postman-token, token, access-control-allow-origin' )
			->header( 'Access-Control-Allow-Credentials', ' true' );
	}

}
