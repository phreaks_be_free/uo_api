<?php
namespace App\Http\Middleware;
use Closure;
class JsonHeaders {

	public function handle($request, Closure $next) {
			return $next($request)
				->header('Content-Type', 'application/json');

	}
}
