<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\View\View;
use Symfony\Component\HttpFoundation\Response;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    //	/**
    //	 * @param $response
    //	 *
    //	 * @return View
    //	 */
    //	private function high_jack_call_to_insert_javascript_if_available( $response ) {
    //		/** @var View $response */
    //		if ( is_a( $response, 'Illuminate\View\View' ) ) {
    //			$file_info = new \SplFileInfo( $response->getPath() );
    //			$path      = $file_info->getPath();
    //			$file_name = str_replace( '.php', '', str_replace( '.blade.php', '', $file_info->getFilename() ) );
    //			if ( file_exists( $path . '/' . $file_name . '.js' ) ) {
    //				$content = '<script type="text/javascript">';
    //				ob_start();
    //				include( $path . '/' . $file_name . '.js' );
    //				$content .= ob_get_contents();
    //				ob_end_clean();
    //				$content .= '</script>';
    //				echo $content;
    //			}
    //		}
    //
    //		return $response;
    //	}
    //
    //	/**
    //	 * @param string $method
    //	 * @param array $parameters
    //	 *
    //	 * @return View|Response
    //	 */
    //	public function callAction( $method, $parameters ) {
    //		return $this->high_jack_call_to_insert_javascript_if_available(
    //			parent::callAction( $method, $parameters )
    //		);
    //	}
}
