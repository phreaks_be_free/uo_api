<?php

namespace App\Http\Controllers;

use PDF;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Hash;
use App\Objects\Resume\ResumeFactory;
use App\Objects\Application\ApplicationFactory;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }

    public function downloadResume(Request $request)
    {
        $url = $request->url;
        if (empty($url)) {
            return redirect()->route('landing');
        }
        $url = rtrim($url, '/');
        /** Get Application by url */
        $application    = ApplicationFactory::get_application_by_url($url);
        $default_resume = ResumeFactory::by_application($application);
        $data = $default_resume->output()["profile"];
        // return view('resume', ['data' => $data["profile"]]);
        return PDF::loadView('resume', ['data' => $data])
            ->setOption('page-size', 'A4')
            ->setOption('margin-top', 0)
            ->download('resume.pdf');
    }
}
