<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Objects\Models\ResumeSkill;
use App\Objects\Models\ResumeProject;
use App\Objects\Resume\ResumeFactory;
use Illuminate\Support\Facades\Input;
use App\Objects\Resume\ResumeComponent;
use App\Objects\Models\ResumeProjectSkills;
use App\Objects\Models\ResumeProjectDisplay;
use App\Objects\Application\ApplicationFactory;
use App\Objects\Resume\Project\ResumeProjectComponent;

class ProjectController extends Controller
{


    private $request;
    private $resumeFactory;
    private $applicationFactory;


    /**
     * @param Request $request
     * @param ResumeFactory $resumeFactory
     * @param ApplicationFactory $applicationFactory
     */
    public function __construct(Request $request, ResumeFactory $resumeFactory, ApplicationFactory $applicationFactory)
    {
        $this->request = $request;
        $this->resumeFactory = $resumeFactory;
        $this->applicationFactory = $applicationFactory;
    }


    /**
     * @param Request $request
     * @return View|RedirectResponse
     */
    function create()
    {
        $resumes = $this->resumeFactory->get_components_output();
        if (empty($resumes)) {
            return flash_and_redirect('resume', 'info', 'No Resume found, please create one first!');
        }

        $project_displays = $this->resumeFactory::get_project_displays();
        if (empty($project_displays)) {
            return flash_and_redirect('resume', 'info', 'No Project Display found, please create one first!');
        }

        return view('resume.project.create')->with(compact('resumes', 'project_displays'));
    }


    /**
     * @return View|RedirectResponse
     */
    function show()
    {
        $projectComponent = $this->resumeFactory::get_project((int) $this->request->project_id);
        if ($projectComponent->isEmpty()) {
            return flash_and_redirect('resume', 'danger', 'Resume  was not found!');
        }

        $project = $projectComponent->output();
        return view('resume.project.show')->with(compact('project'));
    }


    /**
     * @return RedirectResponse
     */
    function store()
    {
        $this->validate($this->request, [
            'resume_id' => 'required|integer|max:255',
            'title' => 'required|string|max:255',
            'description' => 'required|string',
            'project_display_id' => 'required|integer|max:255',
            'image_url' => 'required|url|max:255',
            'content' => 'required|string',
        ]);

        $resumeComponent = $this->resumeFactory->get((int) Input::get('resume_id'));
        if ($resumeComponent->isEmpty()) {
            return flash_and_redirect('resume', 'danger', 'Resume was not found!');
        }

        $project_display = $this->resumeFactory::get_project_display((int) Input::get('project_display_id'));
        if (empty($project_display)) {
            return flash_and_redirect('resume', 'danger', 'Resume Project Display was not found!');
        }

        $projectComponent = $this->resumeFactory::get_project();
        $projectComponent->populate($resumeComponent->get_model(), $project_display, Input::get('title'), Input::get('description'), Input::get('image_url'), Input::get('content'));

        if ($projectComponent->save()) {
            return flash_and_redirect('resume', 'success', 'Resume Project was successfully created!');
        }

        return flash_and_redirect('resume', 'danger', 'Unknown error occured while creating Resume Project!');
    }


    /**
     * @return View|RedirectResponse
     */
    function edit()
    {
        $projectComponent = $this->resumeFactory::get_project((int) $this->request->project_id);
        if ($projectComponent->isEmpty()) {
            return flash_and_redirect('resume', 'danger', 'Resume Project was not found!');
        }

        $resumes = $this->resumeFactory->get_components_output();
        if (empty($resumes)) {
            return flash_and_redirect('resume', 'info', 'No Resume was found, please create one first!');
        }

        $project_displays = $this->resumeFactory::get_project_displays();
        if (empty($project_displays)) {
            return flash_and_redirect('resume', 'info', 'No Project Display was found, please create one first!');
        }

        return view('resume.project.edit')->with([
            'project' => $projectComponent->output(),
            'resumes' => $resumes,
            'project_displays' => $project_displays
        ]);
    }


    /**
     * @return RedirectResponse
     */
    function update()
    {
        $projectComponent = $this->resumeFactory::get_project((int) Input::get('id'));
        if ($projectComponent->isEmpty()) {
            return flash_and_redirect('resume', 'danger', 'Resume Project was not found!');
        }

        $this->validate($this->request, [
            'resume_id' => 'required|integer|max:255',
            'title' => 'required|string|max:255',
            'description' => 'required|string',
            'project_display_id' => 'required|integer|max:255',
            'image_url' => 'required|url|max:255',
            'content' => 'required|string',
        ]);

        $resumeComponent = $this->resumeFactory->get((int) Input::get('resume_id'));
        if ($resumeComponent->isEmpty()) {
            return flash_and_redirect('resume', 'danger', 'Resume was not found!');
        }

        $project_display = $this->resumeFactory::get_project_display((int) Input::get('project_display_id'));
        if (empty($project_display)) {
            return flash_and_redirect('resume', 'danger', 'Resume Project Display was not found!');
        }

        $projectComponent->populate($resumeComponent->get_model(), $project_display, Input::get('title'), Input::get('description'), Input::get('image_url'), Input::get('content'));

        if ($projectComponent->save()) {
            return flash_and_redirect('resume', 'success', 'Resume Project was successfully updated!');
        }

        return flash_and_redirect('resume', 'danger', 'Unknown error occured while updating Resume Project!');
    }


    /**
     * @return RedirectResponse
     */
    function destroy()
    {
        $projectComponent = $this->resumeFactory::get_project((int) Input::get('id'));
        if ($projectComponent->isEmpty()) {
            return flash_and_redirect('resume', 'danger', 'Resume Project was not found!');
        }

        if ($projectComponent->destroy()) {
            return flash_and_redirect('resume', 'success', 'Resume Project was successfully deleted!');
        }

        return flash_and_redirect('resume', 'danger', 'Unknown error occured while deleting Resume Project!');
    }



    /* Resume Project Display */


    /**
     * @return View
     */
    function create_project_display()
    {
        return view('resume.project_displays.create');
    }


    /**
     * @return RedirectResponse
     */
    function store_project_display()
    {
        $model = $this->resumeFactory::get_project_display();

        $this->validate($this->request, [
            'name' => 'required|string|min:3|max:50|unique:' . $model->getTable(),
            'tag' => 'required|string|min:3|max:50|unique:' . $model->getTable()
        ]);


        $result = $this->resumeFactory::populate_project_display($model, Input::get('name'), Input::get('tag'));
        if ($result->save()) {
            return flash_and_redirect('resume', 'success', 'Resume Project Display was successfully created!');
        }

        return flash_and_redirect('resume', 'danger', 'Unknown error occured while creating Resume Project Display!');
    }


    /**
     * @return View|RedirectResponse
     */
    function edit_project_display()
    {
        $project_display = $this->resumeFactory::get_project_display((int) $this->request->project_display_id);
        if (empty($project_display)) {
            return flash_and_redirect('resume', 'danger', 'Resume Project Display was not found!');
        }

        $project_display =  $project_display->toArray();
        return view('resume.project_displays.edit')->with(compact('project_display'));
    }


    /**
     * @return RedirectResponse
     */
    function update_project_display()
    {
        $model = $this->resumeFactory::get_project_display((int) Input::get('project_display_id'));
        if (empty($model)) {
            return flash_and_redirect('resume', 'danger', 'Resume Project Display was not found!');
        }
        $this->validate($this->request, [
            'name' => [
                'required',
                'string',
                'min:3',
                'max:50',
                Rule::unique($model->getTable())->ignore($model->getAttribute('id'), 'id')
            ],
            'tag' => [
                'required',
                'string',
                'min:3',
                'max:50',
                Rule::unique($model->getTable())->ignore($model->getAttribute('id'), 'id')
            ]
        ]);



        $result = $this->resumeFactory::populate_project_display($model, Input::get('name'), Input::get('tag'));

        if ($result->save()) {
            return flash_and_redirect('resume', 'success', 'Resume Project Display was successfully updated!');
        }

        return flash_and_redirect('resume', 'danger', 'Unknown error occured while updating Resume Project Display');
    }


    /**
     * @return RedirectResponse
     */
    function destroy_project_display()
    {
        $project_display = $this->resumeFactory::get_project_display((int) Input::get('id'));
        if (empty($project_display)) {
            return flash_and_redirect('resume', 'danger', 'Resume Project Display was not found!');
        }

        $resume_projects = $project_display->get_projects();
        if (count($resume_projects)) {
            return flash_and_redirect('resume', 'danger', 'Resume Project Display has some Resume Projects associated to it, please delete them first!');
        }

        if ($project_display->delete()) {
            return flash_and_redirect('resume', 'success', 'Resume Project Display was successfully deleted!');
        }
        return flash_and_redirect('resume', 'danger', 'Unknown error occured while deleting Resume Project Display!');
    }



    /* Resume Project Skill */


    /**
     * @return View|RedirectResponse
     */
    function create_project_skill()
    {
        $resume_projects = $this->resumeFactory::get_projects();
        if (!count($resume_projects)) {
            return flash_and_redirect('resume', 'info', 'No Resume Project was found, Please create one first!');
        }

        $resume_skills = $this->resumeFactory::get_skills();
        if (!count($resume_skills)) {
            return flash_and_redirect('resume', 'info', 'No Resume Skill was found, Please create one first!');
        }
        $resume_projects = get_components_output($resume_projects);

        return view('resume.project_skills.create')->with(compact('resume_skills', 'resume_projects'));
    }


    /**
     * @return RedirectResponse
     */
    function store_project_skill()
    {
        $this->validate($this->request, [
            'resume_project' => 'required|max:255',
            'resume_skill' => 'required|max:255',
        ]);

        $projectComponent = $this->resumeFactory::get_project((int) Input::get('resume_project'));
        if ($projectComponent->isEmpty()) {
            return flash_and_redirect('resume', 'danger', 'Resume Project was not found!');
        }

        $resume_skill = $this->resumeFactory::get_skill((int) Input::get('resume_skill'));
        if (empty($resume_skill)) {
            return flash_and_redirect('resume', 'danger', 'Resume Skill was not found!');
        }

        foreach ($projectComponent->output()['skills'] as $skill) {
            if ($skill['resume_skill']['tag'] == $resume_skill->getAttribute('tag')) {
                return flash_and_redirect_back('danger', 'Skill is already associated with Resume Project!');
            }
        };

        $model = $this->resumeFactory::get_project_skill();
        $result = $this->resumeFactory::populate_project_skill($model, $projectComponent->get_model(), $resume_skill);

        if ($result->save()) {
            return flash_and_redirect('resume', 'success', 'Resume Skill was successfully assigned to Resume Project');
        }
        return flash_and_redirect('resume', 'danger', 'Unknown error occured while assigning Resume Skill to Resume Project!');
    }


    /**
     * @return View|RedirectResponse
     */
    function edit_project_skill()
    {
        $project_skill = $this->resumeFactory::get_project_skill((int) $this->request->project_skill_id);
        if (empty($project_skill)) {
            return flash_and_redirect('resume', 'danger', 'Resume Project Skill was not found!');
        }

        $resume_projects = $this->resumeFactory::get_projects();
        if (!count($resume_projects)) {
            return flash_and_redirect('resume', 'info', 'No Resume Project was found, Please create one first!');
        }

        $resume_projects = get_components_output($resume_projects);
        $resume_skills = $this->resumeFactory::get_skills();
        if (!count($resume_skills)) {
            return flash_and_redirect('resume', 'info', 'No Resume Skill was found, Please create one first!');
        }

        return view('resume.project_skills.edit')->with(compact('resume_skills', 'resume_projects', 'project_skill'));
    }


    /**
     * @return RedirectResponse
     */
    function update_project_skill()
    {
        $project_skill = $this->resumeFactory::get_project_skill((int) Input::get('project_skill_id'));
        if (empty($project_skill)) {
            return flash_and_redirect('resume', 'danger', 'Resume Project Skill was not found!');
        }

        $this->validate($this->request, [
            'resume_project' => 'required|max:255',
            'resume_skill' => 'required|max:255',
        ]);

        $projectComponent = $this->resumeFactory::get_project((int) Input::get('resume_project'));
        if ($projectComponent->isEmpty()) {
            return flash_and_redirect('resume', 'danger', 'Resume Project was not found!');
        }

        $resume_skill = $this->resumeFactory::get_skill((int) Input::get('resume_skill'));
        if (empty($resume_skill)) {
            return flash_and_redirect('resume', 'danger', 'Resume Skill was not found!');
        }

        
            foreach ($projectComponent->output()['skills'] as $key) {
                if($key['id'] !== $project_skill['id']) {
                    if ($key['resume_skill']['tag'] == $resume_skill->getAttribute('tag')) {
                        return flash_and_redirect_back('danger', 'Skill is already associated with Resume Project!');
                    }
                }
            }

        $result = $this->resumeFactory::populate_project_skill($project_skill, $projectComponent->get_model(), $resume_skill);

        if ($result->save()) {
            return flash_and_redirect('resume', 'success', 'Resume Project Skill was succesffully updated!');
        }
        return flash_and_redirect('resume', 'danger', 'Unknown error occured while updating Resume Project Skill!');
    }


    /**
     * @return RedirectResponse
     */
    function destroy_project_skill()
    {
        $model = $this->resumeFactory::get_project_skill((int) Input::get('id'));
        if (empty($model)) {
            return flash_and_redirect('resume', 'danger', 'Resume Project Skill was not found!');
        }
        if ($model->delete()) {
            return flash_and_redirect('resume', 'success', 'Resume Project Skill was successfully deleted!');
        }
        return flash_and_redirect('resume', 'danger', 'Unknown error occured while deleting Resume Project Skill!');
    }
}
