<?php

namespace App\Http\Controllers;

use App\Objects\Models\Roles;
use Illuminate\Http\Request;
use App\Objects\User\UserFactory;
use App\Objects\User\UserComponent;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use App\Objects\Application\ApplicationFactory;
use App\Objects\Application\ApplicationComponent;
use App\Objects\ApplicationUser\ApplicationUserFactory;
use Illuminate\View\View;
use Symfony\Component\HttpFoundation\Response;

class ApplicationController extends Controller
{

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Exception
     */
    public function index(UserComponent $user)
    {
        $messages = [];
        if ($user->isEmpty()) {
            return;
        }
        /** @var ApplicationComponent $apps */
        $apps = $user->get_applications();

        if (empty($apps)) {
            $messages[] = 'No applications found!';
        }

        return view('application.index')->with(['apps' => $apps, 'messages' => $messages]);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        return view('application.create', ['roles' => Roles::all()]);
    }


    /**
     * @param Request $request
     *
     * @return bool|\Illuminate\Http\RedirectResponse
     */
    public function store(Request $request, UserComponent $user)
    {
        $messages = [];
        $this->validate($request, [
            'application_name' => 'required|max:255|string',
            'application_url' => 'required|url',
            'application_role' => 'required',
        ]);

        if ($user->isEmpty()) {
            return false;
        }
        $component = new ApplicationComponent();
        $name = $request->application_name;
        $url = $request->application_url;
        $active = 1;
        $populate = $component->populate($user, $name, $url, $active);
        if ($populate && $populate->save()) {

            $component->add_role($request->application_role);
            $request->session()->flash('success', ['Application has been created successfully!']);

            return redirect()->route('home');
        } else {
            $request->session()->flash('danger', ['Error while creating application!']);

            return redirect()->route('home');
        }
    }

    public function index_users(Request $request)
    {
        $messages = [];
        $pk = (int)$request->application_id;
        $component = ApplicationFactory::get($pk);
        if ($component->isEmpty()) {
            $request->session()->flash('danger', ['Invalid application details!']);

            return redirect()->route('application');
        }
        $users = $component->get_application_users();
        if (!$users) {
            $messages[] = 'No application user found!';
        }

        return view('application_user.index')->with([
            'application' => $component,
            'users' => $users,
            'messages' => $messages
        ]);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create_user(Request $request)
    {
        $messages = [];
        $application_id = (int)$request->application_id;
        $application = ApplicationFactory::get($application_id);
        if ($application->isEmpty()) {
            $request->session()->flash('danger', ['Invalid application details!']);

            return redirect()->route('application');
        }

        return view('application_user.create')->with([
            'application' => $application,
            'messages' => $messages,
        ]);
    }

    public function store_user(Request $request)
    {
        $this->validate($request, [
            'role' => 'required|string',
            'first_name' => 'required|string',
            'last_name' => 'required|string',
            'email' => 'required|email|unique:application_users',
        ]);

        $application = $applicationFactory->get((int)$request->application_id);
        if ($application->isEmpty()) {
            $request->session()->flash('danger', ['Invalid application details!']);

            return redirect()->route('application');
        }
        $values = [];
        $values['role'] = $request->role;
        $values['first_name'] = $request->first_name;
        $values['last_name'] = $request->last_name;
        $values['email'] = $request->email;
        $result = $application->store_application_user($values);
        if ($result) {
            $request->session()->flash('success', ['Application User has been created successfully!']);

            return redirect('application/' . $application->get_id() . '/users');
        } else {
            $request->session()->flash('danger', ['Error ocurred while creating application user']);

            return redirect()->back()->withInput();
        }

    }

    public function destroy_user(Request $request)
    {
        $pk = (int)$request->user_id;
        $user = ApplicationUserFactory::get($pk);
        if ($user->isEmpty()) {
            $request->session()->flash('danger', ['Invalid application user details!']);

            return redirect()->route('application');
        }
        if ($user->destroy()) {
            $request->session()->flash('success', ['Application user has been deleted successfully']);

            return redirect()->back();
        }

    }
}
