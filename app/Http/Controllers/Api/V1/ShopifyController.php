<?php
/**
 * Created by PhpStorm.
 * User: Jason Foy
 * Date: 12/20/2016
 * Time: 8:04 PM
 */

namespace App\Http\Controllers\Api\V1;


use App\Objects\Abstracted\Controllers\AbstractShopify;
use App\Objects\Application\ApplicationComponent;
use App\Objects\Models\ShopifyAccounts;
use App\Objects\Shopify\ShopifyAccountComponent;

class ShopifyController extends AbstractShopify {

	public function index() {
		return $this->get_store_product();
	}

	public function get_store_product() {
		$shopify = new ShopifyAccountComponent( 1 );

		echo $shopify->get_products();
		echo "here";
		exit;
	}

	/**
	 * @throws \Exception
	 */
	public function install() {
		if ( ! isset( $_REQUEST['shop'] ) ) {
			throw new \Exception( 'Invalid Parameters', 403 );
		}
		$redirect = ( new ShopifyAccountComponent() )
			->get_install_url( $_REQUEST['shop'] );

		if ( isset( $redirect ) ) {
			header( "Location: {$redirect}" );
			exit;
		}
		response()->json( [ 'message' => 'invalid store or application' ] );
	}
}
