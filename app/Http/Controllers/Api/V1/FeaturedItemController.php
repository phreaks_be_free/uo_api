<?php
/**
 * Created by PhpStorm.
 * User: Jason Foy
 * Date: 12/20/2016
 * Time: 8:04 PM
 */

namespace App\Http\Controllers\Api\V1\Constitution;

use App\Http\Controllers\Controller;
use App\Objects\Application\ApplicationComponent;
use App\Objects\FeaturedItems\FeaturedItemComponent;
use Illuminate\Support\Facades\Input;

class FeaturedItemController extends Controller {

	/**
	 * @param int $featured_item
	 *
	 * @return array
	 */
	public function show( int $featured_item ) {
		$featured_item = new FeaturedItemComponent( $featured_item );
		if ( $featured_item->isEmpty() ) {
			return response()->json( [ 'result' => false, 'message' => 'item not found', 'status' => 403 ] );
		}

		return response()->json( [ 'item' => $featured_item->output(), 'status' => 200 ] );
	}

	/**
	 * @return \Illuminate\Http\JsonResponse
	 * @throws \Exception
	 */
	public function update() {
		$items            = Input::get( 'items', false );
		$featured_item_id = (int) Input::get( 'featured_item_id', false );
		$application_id   = (int) Input::get( 'application_id', false );

		$invalid = [ 'result' => false, 'message' => 'invalid parameters', 'status' => 403 ];
		/** requires items to be added */
		if ( ! is_array( $items ) || ( ! $featured_item_id && ! $application_id ) ) {
			return response()->json( $invalid );
		}

		/** @var FeaturedItemComponent $component */
		$component = new FeaturedItemComponent( $featured_item_id );
		/** Check if we need application */
		if ( $component->isEmpty() ) {
			$application = new ApplicationComponent( $application_id );
		} else {
			if ( $application_id ) {
				$application = new ApplicationComponent( $application_id );
			} else {
				$application = $component->get_application();
			}
		}
		/** Check for a valid Application */
		if ( $application->isEmpty() ) {
			return response()->json( $invalid );
		}
		$component->populate( $application, $application->get_user(), $items );
		if ( $component->save() ) {
			return response()->json( [ 'result' => true, 'status' => 200 ] );
		}

		return response()->json( [ 'result' => false, 'status' => 500, 'message' => 'failed to save' ] );

	}

	/**
	 * @return \Illuminate\Http\JsonResponse
	 * @throws \Exception
	 */
	public function destroy() {
		$featured_item_id = Input::get( 'featured_item_id' );
		$component        = new FeaturedItemComponent( $featured_item_id );
		if ( $component->isEmpty() ) {
			return response()->json( [ 'result' => 'invalid details provided', 'status' => 403 ] );
		}

		return response()->json( [ 'result' => $component->destroy(), 'status' => 200 ] );
	}

}
