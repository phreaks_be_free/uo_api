<?php
/**
 * Created by PhpStorm.
 * User: Jason Foy
 * Date: 12/20/2016
 * Time: 8:04 PM
 */

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use App\Objects\InstanceInterpreter\InstanceInterpreterComponent;

class ApplicationController extends Controller {

	/**
	 * @return mixed
	 * @throws \Exception
	 */
	public function invoke_application_instance() {
		$url              = Input::get( 'url', null );
		$interpreter_name = Input::get( 'interpreter', null );
		$injection_data   = Input::get( 'injection_data', null );

		if ( empty( $url ) || empty( $interpreter_name ) ) {
			throw new \Exception( 'Invalid Parameters' );
		}

		$execute = ( new InstanceInterpreterComponent( $url, $interpreter_name, $injection_data ) )->execute();

		if ( is_a( $execute, 'Exception' ) ) {
			$execute = '{' . $execute->getMessage() . "}";
		}

		return response()->json( $execute );
	}
}
