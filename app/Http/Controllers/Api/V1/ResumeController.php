<?php

/**
 * Created by PhpStorm.
 * User: Jason Foy
 * Date: 12/20/2016
 * Time: 8:04 PM
 */

namespace App\Http\Controllers\Api\V1;

use App\Objects\Application\ApplicationFactory;
use App\Objects\Resume\ResumeComponent;
use \App\Objects\Resume\ResumeFactory;
use \App\Objects\User\UserComponent;
use App\Http\Controllers\Controller;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

class ResumeController extends Controller
{

    public function index()
    {
        $resumes = ResumeFactory::index();$resumes = array_map(function($resume) {
			return $resume->output();
		}, $resumes);
        return response()->json(['resumes' => $resumes]);
    }


    public function get_default_by_url()
    {
        $raw_url = Input::get('url', null);
        if (empty($raw_url)) {
            return response()->json(['result' => false, 'message' => 'URL is required']);
        }

        $url = rtrim($raw_url, '/');
        /** Get Application by url */
        $application = ApplicationFactory::get_application_by_url($url);
        $default_resume = ResumeFactory::by_application($application);


        return response()->json(['resume' => $default_resume->output()]);
    }

    public function projects()
    {
        return response()->json([false]);
    }

    /**
     * @param Request $request
     * @return mixed
     * @throws Exception
     */
    public function download(Request $request)
    {
        $url = $request->url;
        if (empty($url)) {
            throw new Exception("Invalid Parameters", 400);
        }

        $url = rtrim($url, '/');
        // Get Application by url
        $application = ApplicationFactory::get_application_by_url($url);
        $default_resume = ResumeFactory::by_application($application);
        $data = $default_resume->output()["profile"];
        // return view('resume', ['data' => $data["profile"]]);
        return PDF::loadView('resume', ['data' => $data])
            ->setOption('page-size', 'A4')
            ->setOption('margin-top', 0)
            ->download('resume.pdf');
    }
}
