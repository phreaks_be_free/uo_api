<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Objects\Resume\ResumeFactory;
use Illuminate\Support\Facades\Input;
use App\Objects\Application\ApplicationFactory;
use App\Objects\Models\ResumeProfileEducation;
use App\Objects\Models\ResumeProfileEmployment;
use App\Objects\Models\ResumeProfileReference;

class ResumeController extends Controller
{

    private $request;
    private $resumeFactory;
    private $applicationFactory;

    public function __construct(Request $request, ResumeFactory $resumeFactory, ApplicationFactory $applicationFactory)
    {
        $this->request = $request;
        $this->resumeFactory = $resumeFactory;
        $this->applicationFactory = $applicationFactory;
    }

    /**
     * @return \Illuminate\View\View
     */
    function index()
    {
        $resumes = $this->resumeFactory->get_components_output();
        $resume_skills = $this->resumeFactory::get_skills();
        $resume_project_skills = $this->resumeFactory::get_project_skills();
        $resume_project_displays = $this->resumeFactory::get_project_displays();
        return view('resume.index')->with(compact('resumes', 'resume_skills', 'resume_project_skills', 'resume_project_displays'));
    }


    /**
     * @return \Illuminate\View\View
     */
    function create()
    {
        $applications = $this->applicationFactory->get_components_output();
        if (empty($applications)) return flash_and_redirect('resume', 'danger', 'No Application found!');
        return view('resume.create')->with(compact('applications'));
    }


    /**
     * @return \Illuminate\Http\RedirectResponse
     */
    function store()
    {
        $this->validate($this->request, [
            'name'       => 'required|string|max:255|unique:resume',
            'application_id' => 'required|integer|max:255',
            'default'  => 'max:255',
        ]);

        $applicationComponent = $this->applicationFactory->get((int) $this->request->application_id);
        if ($applicationComponent->isEmpty()) {
            return flash_and_redirect_back('danger', 'Application not found!');
        }

        $resumeComponent = $this->resumeFactory->get();
        $resumeComponent->populate($applicationComponent, $this->request->name, $this->request->default);

        if ($resumeComponent->save()) {
            return flash_and_redirect('resume', 'success', 'Resume was successfully added!');
        }
        return flash_and_redirect_back('danger', 'Error ocurred while creating new Resume!');
    }


    /**
     * @return View
     */
    function edit()
    {
        $component = $this->resumeFactory->get((int) $this->request->resume_id);
        $applications = $this->applicationFactory->get_components_output();
        if ($component->isEmpty()) {
            return flash_and_redirect('resume', 'danger', 'Resume not found!');
        }
        if (empty($applications)) {
            return flash_and_redirect('resume', 'danger', 'No Application found, please create one first!');
        }
        return view('resume.edit')->with(['resume' => $component->output(), 'applications' => $applications]);
    }


    /**
     * @return RedirectResponse
     */
    function update()
    {
        $this->validate($this->request, [
            'name'       => 'required|string|max:255',
            'application_id' => 'required|integer|max:255',
            'default'  => 'max:255',
        ]);

        $resumeComponent = $this->resumeFactory->get((int) $this->request->input('resume_id'));
        if ($resumeComponent->isEmpty()) return flash_and_redirect('resume', 'danger', 'Resume not found!');

        $applicationComponent = $this->applicationFactory->get((int) $this->request->application_id);
        if ($applicationComponent->isEmpty()) return flash_and_redirect_back('danger', 'Application not found!');

        $resumeComponent->populate($applicationComponent, $this->request->name, $this->request->default);

        if ($resumeComponent->save()) {
            return flash_and_redirect('resume', 'success', 'Resume was successfully updated!');
        }
        return  flash_and_redirect_back('danger', 'Error ocurred while updating Resume!');
    }


    function destroy()
    {
        $component = $this->resumeFactory->get((int) $this->request->input('id'));
        if ($component->isEmpty()) return flash_and_redirect('resume', 'danger', 'Resume not found!');

        if (!$component->get_profile()->isEmpty()) {
            return flash_and_redirect('resume', 'danger', 'Resume can\'nt be deleted. Please delete associated profile with id ' . $component->get_profile()->get_id() . ' first...');
        }
        if (!empty($component->get_projects())) {
            return flash_and_redirect('resume', 'danger', 'Resume can\'nt be deleted. Please delete associated projects first...');
        }

        if ($component->destroy()) {
            return flash_and_redirect('resume', 'success', 'Resume was successfully deleted!');
        }
        
        return flash_and_redirect('resume', 'danger', 'Unknown error occured while deleting Resume!');
    }



    /** Resume Skills */

    /**
     * @return View
     */
    function create_skill()
    {
        return view('resume.skill.create');
    }


    /**
     
     * @return RedirectResponse
     */
    function store_skill()
    {
        $this->validate($this->request, [
            'name' => 'required|string|unique:resume_skills|max:255',
            'tag' => 'required|string|unique:resume_skills|max:255'
        ]);

        $model = $this->resumeFactory->get_skill();
        $skill = $this->resumeFactory->populate_skill($model, Input::get('name'), Input::get('tag'));

        if ($skill->save()) {
            return flash_and_redirect('resume', 'success', 'Resume Skill was successfully created!');
        }
        return flash_and_redirect('resume', 'danger', 'Unknown error occured while creating Resume Skill!');
    }


    /**
     * @return View
     */
    function edit_skill()
    {
        $model = $this->resumeFactory::get_skill((int) $this->request->skill_id);
        if (empty($model)) {
            return flash_and_redirect('resume', 'danger', 'Resume Skill was not found!');
        }
        return view('resume.skill.edit')->with(['skill' => $model->toArray()]);
    }


    /**
     * @return RedirectResponse
     */
    function update_skill()
    {
        $model = $this->resumeFactory::get_skill((int) $this->request->id);
        if (empty($model)) {
            return flash_and_redirect('resume', 'danger', 'Resume Skill was not found!');
        }
        $this->validate($this->request, [
            'name' => ['required', 'string', 'max:255', Rule::unique('resume_skills')->ignore($model->getAttribute('id'), 'id')],
            'tag' => ['required', 'string', 'max:255', Rule::unique('resume_skills')->ignore($model->getAttribute('id'), 'id')],
        ]);
        $result = $this->resumeFactory::populate_skill($model, Input::get('name'), Input::get('tag'));
        if ($result->save()) {
            return flash_and_redirect('resume', 'success', 'Resume Skill was successfully updated!');
        }
        return flash_and_redirect('resume', 'danger', 'Unknown error occured while updating Resume Skill!');
    }


    /**
     * @return RedirectResponse
     */
    function destroy_skill()
    {
        $model = $this->resumeFactory::get_skill((int) $this->request->id);

        if (empty($model)) {
            return flash_and_redirect('resume', 'danger', 'Resume Skill was not found!');
        }

        if ($model->delete()) {
            return flash_and_redirect('resume', 'success', 'Resume Skill was successfully deleted!');
        }

        return flash_and_redirect('resume', 'danger', 'Unknown error occured while deleting Resume Skill!');
    }


    /* Resume Profile */


    /**
     * @return View|RedirectResponse
     */
    function show_profile()
    {
        $profileComponent = $this->resumeFactory::get_profile((int) $this->request->profile_id);
        if ($profileComponent->isEmpty()) {
            flash_and_redirect('resume', 'danger', 'Resume Profile was not found!');
        }

        return view('resume.profile.show')->with(['profile' => $profileComponent->output()]);
    }


    /**
     * @return View|RedirectResponse
     */
    function create_profile()
    {
        $resumes_with_no_profile = get_components_output($this->resumeFactory->get_resumes_with_no_profile());
        if (empty($resumes_with_no_profile)) {
            return flash_and_redirect('resume', 'danger', 'No Resume with missing Profile found!');
        }

        return view('resume.profile.create')->with(['resumes' => $resumes_with_no_profile]);
    }

    /**
     * @return RedirectResponse
     */
    function store_profile()
    {
        $this->validate($this->request, [
            'resume_id' => 'required|max:255',
            'full_name' => 'required|string|max:255',
            'title' => 'required|string|max:255',
            'email' => 'email|max:255',
            'phone' => 'string|max:255',
            'objective' => 'required|string|max:255',
            'display_picture_url' => 'required|string|max:255',
            'biography' => 'required|string|max:255',
            'slider_text' => 'required|string|max:255',
        ]);

        $resumeComponent = $this->resumeFactory->get((int) Input::get('resume_id'));

        if ($resumeComponent->isEmpty()) {
            return flash_and_redirect_back('danger', 'Resume was not found!');
        }
        if (!$resumeComponent->get_profile()->isEmpty()) {
            return flash_and_redirect('resume', 'danger', 'Resume already has a profile associated to it!');
        }

        $profileComponent = $resumeComponent->get_profile();
        $profileComponent->populate(
            $resumeComponent->get_model(),
            Input::get('full_name'),
            Input::get('email'),
            Input::get('title'),
            Input::get('phone'),
            Input::get('objective'),
            Input::get('display_picture_url'),
            Input::get('biography'),
            Input::get('slider_text')
        );

        if ($profileComponent->save()) {
            return flash_and_redirect('resume', 'success', 'Profile was successfully created!');
        }
        return flash_and_redirect('resume', 'danger', 'Unknown error occured while storing Resume Profile into Database!');
    }

    /**
     * @return View|RedirectResponse
     */
    function edit_profile()
    {
        $profileComponent = $this->resumeFactory::get_profile((int) $this->request->profile_id);
        $resumeComponent = $this->resumeFactory::by_resume_profile($profileComponent);
        if ($profileComponent->isEmpty() || $resumeComponent->isEmpty()) {
            return flash_and_redirect('resume', 'danger', 'Profile or parent Resume not found!');
        }

        $resumes = get_components_output($this->resumeFactory->get_resumes_with_no_profile());
        $resumes[] = $resumeComponent->output();
        return view('resume.profile.edit')->with(['profile' => $profileComponent->output(), 'resumes' => $resumes]);
    }


    /**
     * @return RedirectResponse
     */
    function update_profile()
    {
        $this->validate($this->request, [
            'resume_id' => 'required|max:255',
            'full_name' => 'required|string|max:255',
            'title' => 'required|string|max:255',
            'email' => 'email|max:255',
            'phone' => 'string|max:255',
            'objective' => 'required|string|max:255',
            'display_picture_url' => 'required|string|max:255',
            'biography' => 'required|string|max:255',
            'slider_text' => 'required|string|max:255',
        ]);

        $resumeComponent = $this->resumeFactory->get((int) Input::get('resume_id'));
        $profileComponent = $this->resumeFactory::get_profile((int) Input::get('id'));
        $associatedProfile = $resumeComponent->get_profile();

        if ($profileComponent->isEmpty() || $resumeComponent->isEmpty()) {
            return flash_and_redirect('resume', 'danger', 'Profile or Resume was not found!');
        }
        if (!$associatedProfile->isEmpty() && ($associatedProfile->get_resume_id() != $profileComponent->get_resume_id())) {
            return flash_and_redirect('resume', 'danger', 'Resume already has a Profile associated to it!');
        }

        $profileComponent->populate(
            $resumeComponent->get_model(),
            Input::get('full_name'),
            Input::get('email'),
            Input::get('title'),
            Input::get('phone'),
            Input::get('objective'),
            Input::get('display_picture_url'),
            Input::get('biography'),
            Input::get('slider_text')
        );

        if ($profileComponent->save()) {
            return flash_and_redirect('resume', 'success', 'Profile was successfully updated!');
        }

        return flash_and_redirect('resume', 'danger', 'Unknown error occured while updating Resume Profile!');
    }


    /**
     * @return RedirectResponse
     */
    function destroy_profile()
    {
        $profileComponent = $this->resumeFactory::get_profile((int) Input::get('id'));
        if ($profileComponent->isEmpty()) {
            return flash_and_redirect('resume', 'danger', 'Resume Profile was not found!');
        }
        if ($profileComponent->destroy()) {
            return flash_and_redirect('resume', 'success', 'Resume Profile was successfully deleted!');
        }
        return flash_and_redirect('resume', 'danger', 'Unknown error occured while deleting Resume Profile!');
    }



    /* Resume Profile Skills */

    /**
     * @return RedirectResponse|View
     */
    function create_profile_skill()
    {
        $resume_profiles = get_components_output($this->resumeFactory->get_profiles());
        $resume_skills = $this->resumeFactory::get_skills();

        if (empty($resume_skills)) {
            return flash_and_redirect('resume', 'info', 'No Resume Skill found, please create one first!');
        }

        return view('resume.profile_skills.create')->with(compact('resume_skills', 'resume_profiles'));
    }


    /**
     * @return redirectResponse
     */
    function store_profile_skill()
    {
        $this->validate($this->request, [
            'resume_profile' => 'required|max:255',
            'resume_skill' => 'required|max:255',
            'profile_skill_progress' => 'required|integer|min:20|max:100'
        ]);

        $profile_component = $this->resumeFactory::get_profile((int) Input::get('resume_profile'));
        $resume_skill = $this->resumeFactory::get_skill((int) Input::get('resume_skill'));

        if ($profile_component->isEmpty() || empty($resume_skill)) {
            return flash_and_redirect_back('danger', 'Resume Profile or Resume Skill not found!');
        }

        foreach ($profile_component->output()['skills'] as $s) {
            if ($s['resume_skill']['tag'] == $resume_skill->getAttribute('tag')) {
                return flash_and_redirect_back('danger', 'Skill is already associated with the Profile!');
            }
        };

        $model = $this->resumeFactory::get_profile_skill();
        $result = $this->resumeFactory::populate_profile_skill($model, $profile_component->get_model(), $resume_skill, (int) Input::get('profile_skill_progress'));

        if ($result->save()) {
            return flash_and_redirect('resume', 'success', 'Skill was successfully assigned to Resume Profile');
        }

        return flash_and_redirect('resume', 'danger', 'Unknown error occured while assigning skill to Resume Profile');
    }


    /**
     * @return View|RedirectResponse
     */
    function edit_profile_skill()
    {
        $profileSkill = $this->resumeFactory::get_profile_skill((int) $this->request->profile_skill_id);
        if (empty($profileSkill)) {
            return flash_and_redirect('resume', 'danger', 'Resume Profile Skill was not found!');
        }

        $resume_profiles = get_components_output($this->resumeFactory->get_profiles());
        $resume_skills = $this->resumeFactory::get_skills();
        if (empty($resume_skills)) {
            return flash_and_redirect('resume', 'info', 'No Resume Skill was found, please create one first!');
        }


        return view('resume.profile_skills.edit')->with([
            'resume_profile_skill' => $profileSkill->toArray(),
            'resume_profiles' => $resume_profiles,
            'resume_skills' => $resume_skills
        ]);
    }


    /**
     * @return RedirectResponse
     */
    function update_profile_skill()
    {
        $this->validate($this->request, [
            'resume_profile' => 'required|max:255',
            'resume_skill' => 'required|max:255',
            'profile_skill_progress' => 'required|integer|min:20|max:100'
        ]);

        $profile_component = $this->resumeFactory::get_profile((int) Input::get('resume_profile'));
        $resume_skill = $this->resumeFactory::get_skill((int) Input::get('resume_skill'));
        $profile_skill = $this->resumeFactory::get_profile_skill((int) Input::get('profile_skill_id'));
        if ($profile_component->isEmpty() || empty($resume_skill)  || empty($profile_skill)) {
            return flash_and_redirect_back('danger', 'Resume Profile or Resume Skill or being edited Profile Skill not found!');
        }

            foreach ($profile_component->output()['skills'] as $key) {
                if($key['id'] !== $profile_skill['id']) {
                    if ($key['resume_skill']['tag'] == $resume_skill->getAttribute('tag')) {
                        return flash_and_redirect_back('danger', 'Skill is already associated with Profile!');
                    }
                }
            };

            $result = $this->resumeFactory::populate_profile_skill($profile_skill, $profile_component->get_model(), $resume_skill, (int) Input::get('profile_skill_progress'));


        if ($result->save()) {
            return flash_and_redirect('resume', 'success', 'Resume Profile Skill was successfully updated!');
        }

        return flash_and_redirect('resume', 'danger', 'Unknown error occured while updating Resume Profile Skill!');
    }


    /**
     * @return RedirectResponse
     */
    function destroy_profile_skill()
    {
        $profile_skill = $this->resumeFactory::get_profile_skill((int) Input::get('id'));
        if (empty($profile_skill)) {
            return flash_and_redirect('resume', 'danger', 'Resume Profile Skill was not found!');
        }
        if ($profile_skill->delete()) {
            return flash_and_redirect('resume', 'success', 'Resume Profile Skill was successfully deleted!');
        }
        return flash_and_redirect('resume', 'danger', 'Unknown error occured while deleting Profile Skill!');
    }



    /* Resume Profile Education */


    /**
     * @return View|RedirectResponse
     */
    function create_profile_education()
    {
        $resume_profiles = get_components_output($this->resumeFactory->get_profiles());
        if (empty($resume_profiles)) {
            return flash_and_redirect('resume', 'info', 'No resume profile found, please create one first.');
        }
        return view('resume.profile_education.create')->with(compact('resume_profiles'));
    }


    /**
     * @return RedirectResponse
     */
    function store_profile_education()
    {
        $this->validate($this->request, [
            'resume_profile_id' => 'required|integer|max:255',
            'profile_education_institute' => 'required|string|max:255',
            'profile_education_description' => 'required|string|max:255',
            'profile_education_started_at' => 'required|date',
            'profile_education_ended_at' => 'required|date',
        ]);
        $profileComponent = $this->resumeFactory::get_profile((int) Input::get('resume_profile_id'));
        if ($profileComponent->isEmpty()) {
            return flash_and_redirect_back('danger', 'Resume Profile was not found!');
        }

        $model = $this->resumeFactory::get_profile_education();
        $result = $this->resumeFactory::populate_profile_education($model, $profileComponent->get_model(), Input::get('profile_education_institute'), Input::get('profile_education_description'), Input::get('profile_education_started_at'), Input::get('profile_education_ended_at'), Input::get('profile_education_current'));

        if ($result->save()) {
            return flash_and_redirect('resume', 'success', 'Profile Education was successfully created!');
        }

        return flash_and_redirect('resume', 'danger', 'Unknown error occured while creating Resume Profile Education!');
    }


    /**
     * @return View|RedirectResponse
     */
    function edit_profile_education()
    {
        $resume_profiles = $this->resumeFactory->get_profiles();
        if (empty($resume_profiles)) {
            return flash_and_redirect('resume', 'info', 'No Resume Profile found, please create one first!');
        }

        $profile_education = $this->resumeFactory::get_profile_education((int) $this->request->profile_education_id);
        if (empty($profile_education)) {
            return flash_and_redirect('resume', 'danger', 'Resume Profile Education was not found!');
        }

        return view('resume.profile_education.edit')->with(['profile_education' => $profile_education->toArray(), 'resume_profiles' => get_components_output($resume_profiles)]);
    }


    /**
     * @return RedirectResponse
     */
    function update_profile_education()
    {
        $this->validate($this->request, [
            'resume_profile_id' => 'required|integer|max:255',
            'profile_education_institute' => 'required|string|max:255',
            'profile_education_description' => 'required|string|max:255',
            'profile_education_started_at' => 'required|date',
            'profile_education_ended_at' => 'required|date',
        ]);

        $profileComponent = $this->resumeFactory::get_profile((int) Input::get('resume_profile_id'));
        if ($profileComponent->isEmpty()) {
            return flash_and_redirect_back('danger', 'Resume Profile was not found!');
        }

        $model = $this->resumeFactory::get_profile_education((int) Input::get('profile_education_id'));
        if (empty($model)) {
            return flash_and_redirect('resume', 'danger', 'Resume Profile Education was not found!');
        }

        $result = $this->resumeFactory::populate_profile_education(
            $model,
            $profileComponent->get_model(),
            Input::get('profile_education_institute'),
            Input::get('profile_education_description'),
            Input::get('profile_education_started_at'),
            Input::get('profile_education_ended_at'),
            Input::get('profile_education_current')
        );

        if ($result->save()) {
            return flash_and_redirect('resume', 'success', 'Profile Education was successfully updated!');
        }

        return flash_and_redirect('resume', 'danger', 'Unknown error occured while updating Resume Profile Education!');
    }


    /**
     * @return RedirectResponse
     */
    function destroy_profile_education()
    {
        $model = $this->resumeFactory::get_profile_education((int) Input::get('id'));
        if (empty($model)) {
            return flash_and_redirect('resume', 'danger', 'Resume Profile Education was not found!');
        }
        if ($model->delete()) {
            return flash_and_redirect('resume', 'success', 'Resume Profile Education was successfully deleted!');
        }
        return flash_and_redirect('resume', 'danger', 'Unknown error occured while deleting Resume Profile Education!');
    }



    /* Resume Profile Employment */


    /**
     * @return View|RedirectResponse
     */
    function create_profile_employment()
    {
        $resume_profiles = $this->resumeFactory->get_profiles();
        if (empty($resume_profiles)) {
            return flash_and_redirect('resume', 'info', 'No Resume Profile was found. Please create one first!');
        }
        $resume_profiles = get_components_output($resume_profiles);
        return view('resume.profile_employment.create')->with(compact('resume_profiles'));
    }


    /**
     * @return RedirectResponse
     */
    function store_profile_employment()
    {
        $this->validate($this->request, [
            'resume_profile_id' => 'required|integer|max:255',
            'profile_employment_company_name' => 'required|string|max:255',
            'profile_employment_position_description' => 'required|string|max:255',
            'profile_employment_started_at' => 'required|date',
            'profile_employment_ended_at' => 'required|date',
        ]);
        $profileComponent = $this->resumeFactory::get_profile((int) Input::get('resume_profile_id'));
        if ($profileComponent->isEmpty()) {
            return flash_and_redirect_back('danger', 'Resume Profile was not found!');
        }

        $model = $this->resumeFactory::get_profile_employment();
        $result = $this->resumeFactory::populate_profile_employment($model, $profileComponent->get_model(), Input::get('profile_employment_company_name'), Input::get('profile_employment_position_description'), Input::get('profile_employment_started_at'), Input::get('profile_employment_ended_at'), Input::get('profile_employment_current'));

        if ($result->save()) {
            return flash_and_redirect('resume', 'success', 'Profile Employment was successfully updated!');
        }

        return flash_and_redirect('resume', 'danger', 'Unknown error occured while updating Resume Profile Employment!');
    }


    /**
     * @return View|RedirectResponse
     */
    function edit_profile_employment()
    {
        $resume_profiles = $this->resumeFactory->get_profiles();
        if (empty($resume_profiles)) {
            return flash_and_redirect('resume', 'info', 'No Resume Profile found, please create one first!');
        }

        $profile_employment = $this->resumeFactory::get_profile_employment((int) $this->request->profile_employment_id);
        if (empty($profile_employment)) {
            return flash_and_redirect('resume', 'danger', 'Resume Profile Employment was not found!');
        }

        return view('resume.profile_employment.edit')->with(['profile_employment' => $profile_employment->toArray(), 'resume_profiles' => get_components_output($resume_profiles)]);
    }


    /**
     * @return RedirectResponse
     */
    function update_profile_employment()
    {
        $this->validate($this->request, [
            'resume_profile_id' => 'required|integer|max:255',
            'profile_employment_company_name' => 'required|string|max:255',
            'profile_employment_position_description' => 'required|string|max:255',
            'profile_employment_started_at' => 'required|date',
            'profile_employment_ended_at' => 'required|date',
        ]);
        $profileComponent = $this->resumeFactory::get_profile((int) Input::get('resume_profile_id'));
        if ($profileComponent->isEmpty()) {
            return flash_and_redirect_back('danger', 'Resume Profile was not found!');
        }
        $model = $this->resumeFactory::get_profile_employment((int) Input::get('profile_employment_id'));
        if (empty($model)) {
            return flash_and_redirect('resume', 'danger', 'Resume Profile Employment was not found!');
        }
        $result = $this->resumeFactory::populate_profile_employment($model, $profileComponent->get_model(), Input::get('profile_employment_company_name'), Input::get('profile_employment_position_description'), Input::get('profile_employment_started_at'), Input::get('profile_employment_ended_at'), Input::get('profile_employment_current'));

        if ($result->save()) {
            return flash_and_redirect('resume', 'success', 'Profile Employment was successfully updated!');
        }

        return flash_and_redirect('resume', 'danger', 'Unknown error occured while updating Resume Profile Employment!');
    }


    /**
     * @return RedirectResponse
     */
    function destroy_profile_employment()
    {
        $model = $this->resumeFactory::get_profile_employment((int) Input::get('id'));
        if (empty($model)) {
            return flash_and_redirect('resume', 'danger', 'Resume Profile Employment was not found!');
        }
        if ($model->delete()) {
            return flash_and_redirect('resume', 'success', 'Resume Profile Employment was successfully deleted!');
        }
        return flash_and_redirect('resume', 'danger', 'Unknown error occured while deleting Resume Profile Employment!');
    }



    /* Resume Profile Reference */


    /**
     * @return View|RedirectResponse
     */
    function create_profile_reference()
    {
        $resume_profiles = $this->resumeFactory->get_profiles();
        if (empty($resume_profiles)) {
            return flash_and_redirect('resume', 'info', 'No Resume Profile was found. Please create one first!');
        }
        $resume_profiles = get_components_output($resume_profiles);
        return view('resume.profile_reference.create')->with(compact('resume_profiles'));
    }

    /**
     * @return RedirectResponse
     */
    function store_profile_reference()
    {
        $this->validate($this->request, [
            'resume_profile_id' => 'required|integer|max:255',
            'profile_reference_name' => 'required|string|max:255',
            'profile_reference_email' => 'required|email|max:255',
            'profile_reference_phone' => 'string|max:255',
            'profile_reference_relationship' => 'required|string|max:255',
        ]);
        $profileComponent = $this->resumeFactory::get_profile((int) Input::get('resume_profile_id'));
        if ($profileComponent->isEmpty()) {
            return flash_and_redirect_back('danger', 'Resume Profile was not found!');
        }

        $model = $this->resumeFactory::get_profile_reference();
        $result = $this->resumeFactory::populate_profile_reference($model, $profileComponent->get_model(), Input::get('profile_reference_name'), Input::get('profile_reference_email'), Input::get('profile_reference_phone'), Input::get('profile_reference_phone'), Input::get('profile_reference_relationship'));

        if ($result->save()) {
            return flash_and_redirect('resume', 'success', 'Profile Reference was successfully updated!');
        }

        return flash_and_redirect('resume', 'danger', 'Unknown error occured while updating Resume Profile Reference!');
    }


    /**
     * @return View|RedirectResponse
     */
    function edit_profile_reference()
    {
        $resume_profiles = $this->resumeFactory->get_profiles();
        if (empty($resume_profiles)) {
            return flash_and_redirect('resume', 'info', 'No Resume Profile found, please create one first!');
        }

        $profile_reference = $this->resumeFactory::get_profile_reference((int) $this->request->profile_reference_id);
        if (empty($profile_reference)) {
            return flash_and_redirect('resume', 'danger', 'Resume Profile Reference was not found!');
        }

        return view('resume.profile_reference.edit')->with(['profile_reference' => $profile_reference->toArray(), 'resume_profiles' => get_components_output($resume_profiles)]);
    }


    /**
     * @return RedirectResponse
     */
    function update_profile_reference()
    {
        $this->validate($this->request, [
            'resume_profile_id' => 'required|integer|max:255',
            'profile_reference_name' => 'required|string|max:255',
            'profile_reference_email' => 'required|email|max:255',
            'profile_reference_phone' => 'string|max:255',
            'profile_reference_relationship' => 'required|string|max:255',
        ]);
        $profileComponent = $this->resumeFactory::get_profile((int) Input::get('resume_profile_id'));
        if ($profileComponent->isEmpty()) {
            return flash_and_redirect_back('danger', 'Resume Profile was not found!');
        }
        $model = $this->resumeFactory::get_profile_reference((int) Input::get('profile_reference_id'));
        if (empty($model)) {
            return flash_and_redirect('resume', 'danger', 'Resume Profile Reference was not found!');
        }
        $result = $this->resumeFactory::populate_profile_reference($model, $profileComponent->get_model(), Input::get('profile_reference_name'), Input::get('profile_reference_email'), Input::get('profile_reference_phone'), Input::get('profile_reference_relationship'));

        if ($result->save()) {
            return flash_and_redirect('resume', 'success', 'Profile Reference was successfully updated!');
        }

        return flash_and_redirect('resume', 'danger', 'Unknown error occured while updating Resume Profile Reference!');
    }


    /**
     * @return RedirectResponse
     */
    function destroy_profile_reference()
    {
        $model = $this->resumeFactory::get_profile_reference((int) Input::get('id'));
        if (empty($model)) {
            return flash_and_redirect('resume', 'danger', 'Resume Profile Reference was not found!');
        }
        if ($model->delete()) {
            return flash_and_redirect('resume', 'success', 'Resume Profile Reference was successfully deleted!');
        }

        return flash_and_redirect('resume', 'danger', 'Unknown error occured while deleting Resume Profile Reference!');
    }
}
