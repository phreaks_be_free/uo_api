<?php
/**
 * Created by PhpStorm.
 * User: Jason Foy
 * Date: 12/20/2016
 * Time: 8:04 PM
 */

namespace App\Http\Controllers;


use App\Objects\Abstracted\Controllers\AbstractShopify;
use App\Objects\Application\ApplicationComponent;
use App\Objects\Shopify\ShopifyAccountComponent;
use Illuminate\Http\Request;

class ShopifyController extends AbstractShopify {
	/**
	 * @param Request $request
	 *
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
	public function install( Request $request ) {
		return View( 'application.install_shopify', [ 'application_id' => $request->application_id ] );
	}

	/**
	 * @throws \Exception
	 */
	public function register( $application_id ) {
		if ( empty( $application_id ) ) {
			throw new \Exception( 'Invalid Parameters 123', 403 );
		}

		$shop = ( new ShopifyAccountComponent() )->load_by_shop_url( $_POST['shop'] );
		if ( $shop->isEmpty() ) {
			if ( $shop->register_store( $_POST['shop'], new ApplicationComponent( (int) $application_id ) ) ) {
				$redirect = $shop->get_install_url();
			}
		} else {
			$redirect = $shop->get_install_url();
		}

		if ( isset( $redirect ) ) {
			header( "Location: {$redirect}" );
			exit;
		}
		response()->json( [ 'message' => 'invalid store or application' ] );
	}

	public function products() {
		$_POST['shop'] = 'www-unicornoverlord-com-store.myshopify.com';
		
		$shop = ( new ShopifyAccountComponent() )->load_by_shop_url( $_POST['shop'] );

		response()->json( [ 'products' => $shop->get_products() ] );
	}
}
