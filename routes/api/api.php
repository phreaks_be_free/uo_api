<?php

use Illuminate\Support\Facades\Route;

Route::group( [
    'prefix'     => 'application',
    'middleware' => [ 'cors', 'json' ]
], function () {

    require base_path( 'routes/api/v1/application.php' );

} );

Route::group( [
    'prefix'     => 'cart',
    'middleware' => [ 'cors', 'json' ]
], function () {
    require base_path( 'routes/api/v1/cart.php' );
} );

Route::group( [
    'prefix'     => 'featured_items',
    'middleware' => [ 'cors', 'json' ]

], function () {
    require base_path( 'routes/api/v1/featured_items.php' );
} );

Route::group( [
    //'middleware' => 'api',
    'prefix'     => 'resume',

], function () {
    require base_path( 'routes/api/v1/resume.php' );
} );