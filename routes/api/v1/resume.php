<?php

use App\Objects\Application\ApplicationFactory;
use App\Objects\Resume\ResumeFactory;
use Illuminate\Http\Request;


//Route::group( [
//	'prefix' => 'by_url',
//], function () {
//	Route::options( '/default', 'Api\V1\ResumeController@get_default_by_url' );
//
//} );

Route::group([
    'prefix' => 'by_url',
    'middleware' => ['appCors', 'json']
], function () {
    Route::any('/default', 'Api\V1\ResumeController@get_default_by_url');
    Route::options('/default', 'Api\V1\ResumeController@get_default_by_url');
    Route::post('/default/page', 'Api\V1\ResumeController@get_default_by_url_and_page');
    Route::options('/default/page', 'Api\V1\ResumeController@get_default_by_url_and_page');


});

Route::group(['prefix' => 'by_url'], function () {

    Route::any('/download', 'Api\V1\ResumeController@download');


});

Route::group([
    'prefix' => 'all',
    'middleware' => ['cors', 'json']
], function () {
    Route::any('/', 'Api\V1\ResumeController@index');
});



