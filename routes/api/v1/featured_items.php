<?php
Route::options( '/get/{featured_item}', 'Api\V1\FeaturedItemController@show' );
Route::post( '/update', 'Api\V1\FeaturedItemController@update' );
Route::post( '/delete', 'Api\V1\FeaturedItemController@destroy' );
