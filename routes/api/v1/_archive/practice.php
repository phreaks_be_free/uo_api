<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

Route::group([
    'middleware' => ['api', 'practiceTokenAuth'],
    'prefix' => 'practice',
], function () {

    /**
     * list providers by practice
     */
    Route::any('providersByPractice', 'Api\V1\PracticeController@providersByPractice');

    Route::post('deactivate', 'Api\V1\UserController@disable');
    Route::get('list', 'Api\V1\UserController@getPatients');
});