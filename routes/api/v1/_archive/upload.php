<?php
use Illuminate\Http\Request;

/*****************************************************
 * Upload 3d File and e-mail
 *****************************************************/
Route::group([
    'middleware' => 'tokenAuth',
    'prefix' => 'email',
], function () {

    Route::post('/upload', function (Request $request) {

        $files = $request->file('3dfile');

        /**
         *  Check how many files we have
         */
        if(count($files)>1 || empty($files))
            return['result' => false, 'reason' => 'Expecting one file uploaded'];
        else
            $file = current($files);

        /**
         * Check all files upload
         */
        foreach($files as $file){
            if(!$file->isFile())
                return['result' => false, 'reason' => '3d file is not present in the upload.'];
            if (!$file->isValid())
                return['result' => false, 'reason' => 'An error occurred while uploaded, file received was corrupted'];
        }

        /**
         * Create Unique filename and save file to local storage
         */
        $providerName = $request->input('providerName');
        $fileId = str_replace('/tmp/','',tempnam(null,$providerName));
        $file->storeAs('3dfiles', $fileId);

        return ['result'=>true,'reason'=>'file has been saved locally'];
    });
});