<?php

use App\Http\Controllers\HomeController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;


//Build Default Routes for Authentication "Logout", "Login"
Auth::routes();


Route::get('/', function () {
    return view('welcome');
})->name('landing');

Route::get('/logout', function () {
    Auth::logout();

    return redirect()->route('landing');
});


//Require Auth
Route::group(['middleware' => 'auth'], function () {
    //Home
    Route::get('/home', function (Request $request) {
        //    // Store a piece of data in the session...
        //    session(['key' => 'value']);
        //    // Retrieve a piece of data from the session...
        //    $value = session('key');
        $controller = new HomeController();
        return $controller->index();
    })->name('home');

    Route::group([
        'prefix' => 'application',
    ], function () {
        require base_path('routes/web/application.php');
    });
});