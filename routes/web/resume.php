<?php

use App\Objects\Abstracted\AbstractComponent;
use App\Objects\Abstracted\AbstractFactory;
use App\Objects\Models\Resume;
use App\Objects\Models\ResumeProject;
use App\Objects\Resume\ResumeComponent;
use App\Objects\Resume\ResumeFactory;
use Illuminate\Support\Facades\Route;

/* Resume */


Route::get('/', 'ResumeController@index')->name('resume');

// Route::get('/', function () {
//     dd(ResumeProject::find(2323));
// })->name('resume');

Route::get('create', 'ResumeController@create');
Route::post('store', 'ResumeController@store');
Route::get('{resume_id}/edit', 'ResumeController@edit');
Route::put('update', 'ResumeController@update');
Route::delete('delete', 'ResumeController@destroy');

/* Resume Skills */
Route::get('skill/create', 'ResumeController@create_skill');
Route::get('skill/{skill_id}/edit', 'ResumeController@edit_skill');
Route::post('skill/store', 'ResumeController@store_skill');
Route::put('skill/update', 'ResumeController@update_skill');
Route::delete('skill/delete', 'ResumeController@destroy_skill');

/* Resume Profile */
Route::get('profile/create', 'ResumeController@create_profile');
Route::get('profile/{profile_id}', 'ResumeController@show_profile');
Route::post('profile/store', 'ResumeController@store_profile');
Route::get('profile/{profile_id}/edit', 'ResumeController@edit_profile');
Route::put('profile/update', 'ResumeController@update_profile');
Route::delete('profile/delete', 'ResumeController@destroy_profile');

/* Resume Profile Skills */
Route::get('profile/skill/create', 'ResumeController@create_profile_skill');
Route::post('profile/skill/store', 'ResumeController@store_profile_skill');
Route::get('profile/skill/{profile_skill_id}/edit', 'ResumeController@edit_profile_skill');
Route::put('profile/skill/update', 'ResumeController@update_profile_skill');
Route::delete('profile/skill/delete', 'ResumeController@destroy_profile_skill');

/* Resume Profile Education */
Route::get('profile/education/create', 'ResumeController@create_profile_education');
Route::post('profile/education/store', 'ResumeController@store_profile_education');
Route::get('profile/education/{profile_education_id}/edit', 'ResumeController@edit_profile_education');
Route::put('profile/education/update', 'ResumeController@update_profile_education');
Route::delete('profile/education/delete', 'ResumeController@destroy_profile_education');

/* Resume Profile Employment */
Route::get('profile/employment/create', 'ResumeController@create_profile_employment');
Route::post('profile/employment/store', 'ResumeController@store_profile_employment');
Route::get('profile/employment/{profile_employment_id}/edit', 'ResumeController@edit_profile_employment');
Route::put('profile/employment/update', 'ResumeController@update_profile_employment');
Route::delete('profile/employment/delete', 'ResumeController@destroy_profile_employment');

/* Resume Profile References */
Route::get('profile/reference/create', 'ResumeController@create_profile_reference');
Route::post('profile/reference/store', 'ResumeController@store_profile_reference');
Route::get('profile/reference/{profile_reference_id}/edit', 'ResumeController@edit_profile_reference');
Route::put('profile/reference/update', 'ResumeController@update_profile_reference');
Route::delete('profile/reference/delete', 'ResumeController@destroy_profile_reference');












/* Resume Projects */
Route::get('project/create', 'ProjectController@create');
Route::get('project/{project_id}', 'ProjectController@show');
Route::post('project/store', 'ProjectController@store');
Route::get('project/{project_id}/edit', 'ProjectController@edit');
Route::put('project/update', 'ProjectController@update');
Route::delete('project/delete', 'ProjectController@destroy');

/* Resume Project Displays */
Route::get('project/display/create', 'ProjectController@create_project_display');
Route::post('project/display/store', 'ProjectController@store_project_display');
Route::get('project/display/{project_display_id}/edit', 'ProjectController@edit_project_display');
Route::put('project/display/update', 'ProjectController@update_project_display');
Route::delete('project/display/delete', 'ProjectController@destroy_project_display');

/* Resume Project Skill */
Route::get('project/skill/create', 'ProjectController@create_project_skill');
Route::post('project/skill/store', 'ProjectController@store_project_skill');
Route::get('project/skill/{project_skill_id}/edit', 'ProjectController@edit_project_skill');
Route::put('project/skill/update', 'ProjectController@update_project_skill');
Route::delete('project/skill/delete', 'ProjectController@destroy_project_skill');
