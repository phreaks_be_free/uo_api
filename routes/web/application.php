<?php

use Illuminate\Support\Facades\Route;

Route::get('/create', 'ApplicationController@create');
Route::post('/submit', 'ApplicationController@store');
Route::get('/', 'ApplicationController@index')->name('application');
Route::get('/{application_id}/users', 'ApplicationController@index_users');
Route::get('/{application_id}/create_user', 'ApplicationController@create_user');
Route::post('/submit_user', 'ApplicationController@store_user');
Route::post('/delete_user', 'ApplicationController@destroy_user');

/** Shopify */
Route::group([
	'prefix' => 'shopify',
], function () {
	Route::get('/install', 'Api\V1\ShopifyController@install');
	Route::get('/complete', 'ShopifyController@complete');
	Route::post('/{application_id}/register', 'ShopifyController@register');
	Route::get('/{application_id}/install', 'ShopifyController@install');
	Route::get('/products', 'ShopifyController@products');
});
