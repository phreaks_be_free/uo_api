<?php

/**
 * @param string $route
 * @param string $flash_class
 * @param string $flash_message
 * @return \Illuminate\Http\RedirectResponse
 */
function flash_and_redirect(string $route, string $flash_class, string $flash_message)
{
    flash($flash_class, $flash_message);
    return redirect()->route($route);
}

/**
 * @param string $flash_class
 * @param string $flash_message
 * @return \Illuminate\Http\RedirectResponse
 */
function flash_and_redirect_back(string $flash_class, string $flash_message)
{
    flash($flash_class, $flash_message);
    return redirect()->back()->withInput();
}


/**
 * @param string $flash_class
 * @param string $flash_message
 * @return bool
 */
function flash(string $flash_class, string $flash_message)
{
    session()->flash($flash_class, [$flash_message]);
    return true;
}


function get_components_output(array $components): array
{
    $container = [];
    foreach ($components as $key) {
        $container[] = $key->output();
    }
    return $container;
}
