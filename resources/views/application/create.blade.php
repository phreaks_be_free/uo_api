@extends('layouts.internal')
@section('content')
    <div class="container">
        <div class="row justify">
            <div class="col-md-6 col-md-offset-3">
                <div class="page-header">
                    <h1>Create New Application</h1>
                </div>
                <form action="/application/submit" method="post">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <label for="name">Application Name</label>
                        <input required value="{{ old('application_name') }}" type="text" name="application_name"
                               id="name "
                               class="form-control">
                    </div>
                    <div class="form-group ">
                        <label for="url ">Application URL</label>
                        <input required value="{{ old('application_url') }}" type="text" name="application_url" id="url"
                               class="form-control">
                    </div>

                    <div class="form-group ">
                        <label for="url ">Application Role</label>
                        <select class="form-control" required value="{{ old('application_role') }}" type="text"
                                name="application_role" id="application_role">
                            @foreach ($roles as $role)
                                <option value="{{$role->getAttribute('id')}}">{{$role->getAttribute('name')}}</option>
                            @endforeach
                        </select>
                    </div>

                    <input type="submit" value="Create Application" class="btn btn-primary pull-right">
                </form>
            </div>
        </div>
    </div>
@endsection