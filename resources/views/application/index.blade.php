@extends('layouts.internal-center')
@section('script')

@endsection
@section('page')
@if(is_array($apps) && !empty($apps))
<div class="page-header">
    <h1>Your Applications</h1>
</div>

<a href="{{url()->current()}}/create" class="btn btn-primary m-1">Create New Application</a>
<hr>
@foreach ($apps as $app)
<div class="card m-1">
    <div class="card-header">
        {{$app->output()["name"]}}
    </div>
    <ul class="list-group list-group-flush">
        <li class="list-group-item">
            <a href="{{$app->output()['url']}}" class="card-link"><i class="fas fa-link"></i>
                {{$app->output()['url']}}</a>
        </li>
        <li class="list-group-item">
            <a href="{{url()->current()}}/{{$app->output()['id']}}/users" class="card-link"><i class="fa fa-users"></i>
                Users</a>

        </li>
        <li class="list-group-item">
            Roles
            @foreach ($app->get_roles() as $role)
            {{$role->get_id()}}

            @endforeach
        </li>

    </ul>
</div>
@endforeach
@else
<div class="card m-1">
    <div class="card-header">
        <a href="{{url()->current()}}/create" class="btn btn-primary m-1">Create New Application</a>
    </div>
</div>
@endif
@endsection