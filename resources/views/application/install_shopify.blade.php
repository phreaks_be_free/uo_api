@extends('layouts.internal-center')
@section('scripts')
    <script src="/js/shopify/install_shopify.js" rel="script" type="text/javascript"></script>
    <link href="/css/shopify/install_shopify.css" type="text/css" rel="stylesheet"/>
@endsection
@section('page')
    <div class="card">
        <div class="card-body">
            <form class="needs-validation" novalidate method="POST"
                  action="{{url()->action('ShopifyController@register',['application_id'=>$application_id])}}">
                {{ csrf_field() }}

                <div class="form-row">
                    <div class="col-md-3">
                        Shopify Shop URL
                    </div>
                    <div class="col-md-7 ">
                        <input minlength="8" type="text" class="form-control" id="shop" name="shop"
                               placeholder="Shopify Store URL" required>
                        <div class="valid-feedback">
                        </div>
                    </div>
                    <div class="col-md-2">
                        <button type="submit" class="btn btn-primary">Install</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
