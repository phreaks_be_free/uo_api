@extends('layouts.internal-center')
@section('page')

<a href="/application/{{$application->get_id()}}/create_user" class="btn btn-primary">Add User</a>
@if(is_array($users) &&
!empty($users))
<div class="page-header">
    <hr>
    <h4>Application: {{$application->output()['name']}}</h4>
    <hr>
    <h3>Users:</h3>
</div>
@foreach ($users as $user)
<div class="card m-1">
    <div class="card-header">
        {{$user->output()["first_name"] . ' ' . $user->output()['last_name']}}

        <form action="/application/delete_user" method="post" class="form form-inline">
            {{ csrf_field() }}
            <input type="hidden" name="user_id" value="{{$user->get_id()}}">
            <input type="submit" value="X" title="Delete user" class="btn btn-danger btn-sm">
        </form>




    </div>
    <ul class="list-group list-group-flush">
        <li class="list-group-item">
            Role: {{$user->output()['role']}}
        </li>
        <li class="list-group-item">
            Email: {{$user->output()['email']}}
        </li>
    </ul>
</div>
@endforeach @endif
@endsection