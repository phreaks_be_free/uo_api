@extends('layouts.internal-center')
@section('page')

<div class="page-header">
    <h1>Create New User</h1>
</div>
<form action="/application/submit_user" method="post">
    {{ csrf_field() }}
    <input type="hidden" name="application_id" value="{{$application->get_id()}}">
    <div class="form-group ">
        <label for="role ">Role</label>
        <input required value="{{ old('role') }}" type="text" name="role" id="role" class="form-control">
    </div>
    <div class="form-group">
        <label for="first_name">First Name</label>
        <input required value="{{ old('first_name') }}" type="text" name="first_name" id="first_name "
            class="form-control">
    </div>
    <div class="form-group">
        <label for="last_name">Last Name</label>
        <input required value="{{ old('last_name') }}" type="text" name="last_name" id="last_name "
            class="form-control">
    </div>
    <div class="form-group ">
        <label for="email ">Email</label>
        <input required value="{{ old('email') }}" type="text" name="email" id="email" class="form-control">
    </div>
    <input type="submit" value="Submit" class="btn btn-primary pull-right">
</form>
@endsection