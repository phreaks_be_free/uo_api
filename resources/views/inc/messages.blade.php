@foreach (['danger', 'success', 'info'] as $key)
    @if(Session::has($key))
        <div class="alert alert-{{$key}}">
            @if(!empty(Session::get($key)))
                @foreach (Session::get($key) as $sub_key)
                    <p>{{ $sub_key }}</p>
                @endforeach
            @endif
        </div>
    @endif
@endforeach

@if(!empty($errors->all()))
    <div class="alert alert-danger">
        @foreach ($errors->all() as $error)
            <p class="m-0">{{ $error }}</p>
        @endforeach
    </div>
@endif

@if(!empty($messages) && is_array($messages))
    <div class="alert alert-info">
        @foreach ($messages as $msg)
            <p class="m-0">{{ $msg }}</p>
        @endforeach
    </div>
@endif
