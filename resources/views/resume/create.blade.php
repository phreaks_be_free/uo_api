@extends('layouts.internal-center')
@section('page')
{{-- <div class="page-header text-center">
    <h4>Create New Resume</h4>
</div> --}}
@include('resume.inc.breadcrumb', [ 'active' => 'Create'])
<hr>
<form action="/resume/store" method="post">
    {{ csrf_field() }}
    <div class="form-group">
        <label for="name">Name</label>
        <input required value="{{ old('name') }}" type="text" name="name" id="name " class="form-control">
    </div>
    <div class="form-group">
        <label for="applcation_id">Application</label>
        @if (is_array($applications) && !empty($applications))

        <select name="application_id" class="form-control" id="application">
            @foreach ($applications as $app)
            <option @if(old('application_id')==$app['id']) selected @endif
                value="{{ $app['id'] }}">{{ $app['name'] }}</option>
            @endforeach
        </select>
        @endif
    </div>
    <div class="form-check">
        <input @if(old('default')) checked @endif name="default" type="checkbox" class="form-check-input" id="default">
        <label class="form-check-label" for="default">Default</label>
    </div>
    <input type="submit" value="Submit" class="mt-3 btn btn-primary">
</form>
@endsection