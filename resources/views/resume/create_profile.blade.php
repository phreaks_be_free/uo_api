@extends('layouts.internal-center')
@section('scripts')
<script src="{{ asset('js/resume/create_profile.js') }}"></script>
@endsection
@section('page')
@if (empty($resume))

<p class="alert alert-danger">Resume not found!</p> <a href="/resume" class="btn btn-outline-primary">Resume Page</a>
@else
<div class="page-header text-center">
    <h4>Create profile</h4>
</div>
<hr>
<form action="/resume/{{ $resume['id'] }}/profile/store" method="post" class="mb-5 pb-5">
    {{ csrf_field() }}
    <div class="form-group">
        <label for="full_name">Full Name</label>
        <input type="text" name="full_name" id="full_name" class="form-control">
    </div>
    <div class="form-group">
        <label for="title">Title</label>
        <input type="text" name="title" id="title" class="form-control">
    </div>
    <div class="form-group">
        <label for="email">Email</label>
        <input type="text" name="email" id="email" class="form-control">
    </div>
    <div class="form-group">
        <label for="phone">Phone</label>
        <input type="text" name="phone" id="phone" class="form-control">
    </div>
    <div class="form-group">
        <label for="objective">Objective</label>
        <input type="text" name="objective" id="objective" class="form-control">
    </div>
    <div class="form-group">
        <label for="display_picture_url">Display Picture URL</label>
        <input type="text" name="display_picture_url" id="display_picture_url" class="form-control">
    </div>
    <div class="form-group">
        <label for="biography">Biography</label>
        <textarea name="biography" id="biography" rows="2" class="form-control"></textarea>
    </div>
    <div class="form-group">
        <label for="slider_text">Slider Text</label>
        <textarea name="slider_text" id="slider_text" rows="2" class="form-control"></textarea>
    </div>
    <div class="row mb-4">
        <div class="col-sm-12 text-center">
            <a class="btn btn-sm btn-primary m-2" data-toggle="collapse" href="#skill" role="button"
                aria-expanded="false" aria-controls="skill">Skill</a>
        </div>
        <div id="skill" class="collapse show">
            <div id="skill-container" class="col-sm-12">
                <div class="skill-row row mb-3">
                    <div class="col-sm-4 form-group"><input type="text" placeholder="Tag" name="skill[0][tag]"
                            class="form-control"></div>
                    <div class="col-sm-4 form-group"><input type="text" placeholder="Name" name="skill[0][name]"
                            class="form-control">
                    </div>
                    <div class="col-sm-4 form-group"><input type="number" placeholder="Progress %" min="20" max="100"
                            name="skill[0][progress]" class="form-control"></div>
                </div>
            </div>
            <div class="col-sm-12 text-right">
                <button id="add-skill-row" type="button" class="btn btn-sm btn-primary">
                    <i class="fas fa-plus-circle"></i> row</button>
            </div>
        </div>

    </div>
    <div class="row mb-4">
        <div class="col-sm-12 text-center">
            <a class="btn btn-sm btn-primary m-2" data-toggle="collapse" href="#education" role="button"
                aria-expanded="false" aria-controls="education">Education</a>
        </div>
        <div id="education" class="collapse show">
            <div id="education-container" class="col-sm-12">
                <div class="education-row row mb-3 ">
                    <div class="col-sm-6 form-group">
                        <label>Started at</label>
                        <input class="form-control" type="date" name="education[0][started_at]">
                    </div>
                    <div class="col-sm-6 form-group">
                        <label>Ended at</label>
                        <input class="form-control" type="date" name="education[0][ended_at]">
                    </div>
                    <div class="col-sm-5 form-group"><input type="text" placeholder="Description"
                            name="education[0][description]" class="form-control">
                    </div>
                    <div class="col-sm-5 form-group"><input type="text" placeholder="Institute"
                            name="education[0][institute]" class="form-control"></div>
                    <div class="col-sm-2 form-group text-center align-self-center">
                        <input type="checkbox" name="education[0][current]" class="form-check-input">
                        <label class="form-check-label">Current</label>
                    </div>
                </div>
            </div>
            <div class="col-sm-12 text-right">
                <button id="add-education-row" type="button" class="btn btn-sm btn-primary">
                    <i class="fas fa-plus-circle"></i> row</button>
            </div>
        </div>
    </div>
    <div class="row mb-4">
        <div class="col-sm-12 text-center">
            <a class="btn btn-sm btn-primary m-2" data-toggle="collapse" href="#employment" role="button"
                aria-expanded="false" aria-controls="employment">Employment</a>
        </div>
        <div id="employment" class="collapse show">
            <div id="employment-container" class="col-sm-12">
                <div class="row employment-row mb-3">
                    <div class="col-sm-6 form-group">
                        <label>Started at</label>
                        <input class="form-control" type="date" name="employment[0][started_at]">
                    </div>
                    <div class="col-sm-6 form-group">
                        <label>Ended at</label>
                        <input class="form-control" type="date" name="employment[0][ended_at]">
                    </div>
                    <div class="col-sm-5 form-group"><input type="text" placeholder="Company Name"
                            name="employment[0][company_name]" class="form-control">
                    </div>
                    <div class="col-sm-5 form-group"><input type="text" placeholder="Position Description"
                            name="employment[0][position_description]" class="form-control"></div>
                    <div class="col-sm-2 form-group text-center align-self-center">
                        <input type="checkbox" name="employment[0][current]" class="form-check-input">
                        <label class="form-check-label">Current</label>
                    </div>
                </div>
            </div>
            <div class="col-sm-12 text-right">
                <button id="add-employment-row" type="button" class="btn btn-sm btn-primary">
                    <i class="fas fa-plus-circle"></i> row</button>
            </div>
        </div>
    </div>

    <div class="row mb-4">
        <div class="col-sm-12 text-center">
            <a class="btn btn-sm btn-primary m-2" data-toggle="collapse" href="#reference" role="button"
                aria-expanded="false" aria-controls="reference">Reference</a>
        </div>
        <div id="reference" class="collapse show">
            <div id="reference-container" class="col-sm-12">
                <div class="row reference-row mb-3">
                    <div class="col-sm-6 form-group"><input type="text" placeholder="Full Name"
                            name="reference[0][name]" class="form-control">
                    </div>
                    <div class="col-sm-6 form-group"><input type="text" placeholder="Relationship"
                            name="reference[0][relationship]" class="form-control">
                    </div>
                    <div class="col-sm-6 form-group"><input type="email" placeholder="Email" name="reference[0][email]"
                            class="form-control">
                    </div>
                    <div class="col-sm-6 form-group"><input type="text" placeholder="Phone" name="reference[0][phone]"
                            class="form-control"></div>
                </div>
            </div>
            <div class="col-sm-12 text-right">
                <button id="add-reference-row" type="button" class="btn btn-sm btn-primary">
                    <i class="fas fa-plus-circle"></i> row</button>
            </div>
        </div>
    </div>
    <button type="submit" value="Submit" class="my-5 btn btn-primary btn-block">Submit
    </button>
    @endif
    @endsection