@extends('layouts.internal')
@section('scripts')

@endsection
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-lg-12">
            @if(is_array($resumes) && !empty($resumes))
            <div class="mb-5">


                @include('resume.inc.resume')

                <hr>

                @include('resume.inc.resume_skills')

                <hr>

                <h3 class=" text-center">Resume Profiles</h3>

                <hr>

                @include('resume.inc.resume_profile')

                <hr>

                @include('resume.inc.resume_profile_skills')

                <hr>

                @include('resume.inc.resume_profile_education')

                <hr>

                @include('resume.inc.resume_profile_employment')

                <hr>

                @include('resume.inc.resume_profile_references')



                <hr>

                <h3 class="text-center">Resume Projects</h3>

                <hr>

                @include('resume.inc.resume_projects')

                <hr>

                @include('resume.inc.resume_project_displays')

                <hr>

                @include('resume.inc.resume_project_skills')
            </div>
            @else
            <div class="card m-1">
                <div class="card-header">
                    <a href="{{url()->current()}}/create" class="btn btn-primary m-1">Create New Resume</a>
                </div>
            </div>
            @endif
        </div>
    </div>
</div>
@endsection