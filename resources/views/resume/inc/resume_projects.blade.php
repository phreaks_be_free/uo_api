<div class="btn-group" role="group" aria-label="Resume Profile Task Button Group">

    <button class="btn btn-dark" type="button" data-toggle="collapse" data-target="#projects" aria-expanded="false"
        aria-controls="projects">Resume Projects</button>
    <a href="/resume/project/create" class="btn btn-primary">Create New Project</a>
</div>
<div id="projects" class="collapse">
    <table class="table table-bordered">
        <thead class="thead-light ">
            <tr>
                <th>id</th>
                <th>title</th>
                <th>resume_id</th>
                <th>description</th>
                <th>display</th>
                <th>skills count</th>
                <th>actions</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($resumes as $resume)
            @if (!empty($resume['projects']))
            @foreach ($resume['projects'] as $project)
            <tr>
                <th>{{$project['id']}}</th>
                <td><a href="/resume/project/{{$project['id']}}" class="">{{$project['title']}}</a></td>
                <td>{{$resume['id']}}</td>
                <td>{{$project['description']}}</td>
                <td>{{$project['display']['name'] }}</td>
                <td>{{ count($project['skills']) }}</td>
                <td class="text-center">
                    <a title="Edit" href="/resume/project/{{$project['id']}}/edit" class="text-decoration-none">
                        <i class="text-body fas fa-edit"></i>
                    </a>
                    <form action="/resume/project/delete" class="form-check-inline" method="post">
                        {{ csrf_field() }}
                        {{ method_field('delete') }}
                        <input type="hidden" name="id" value="{{$project['id']}}">
                        <button type="submit" title="Delete" class="btn btn-link">
                            <i class="text-danger fas fa-trash"></i>
                        </button>
                    </form>
                </td>
            </tr>
            @endforeach
            @endif
            @endforeach
        </tbody>
    </table>
</div>