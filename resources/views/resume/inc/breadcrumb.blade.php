<nav aria-label="breadcrumb">
    <ol class="breadcrumb">
        @if(!Request::is('resume'))
        <li class="breadcrumb-item" aria-current="page">
            <a href="{{ url('resume') }}">Resume</a>
        </li>
        @endif
        @if(!empty($route))
        <li class="breadcrumb-item" aria-current="page">
            <a href="{{ $route['url'] }}">{{$route['name']}}</a>
        </li>
        @endif
        @if(!empty($active))
        <li class="breadcrumb-item active" aria-current="page">{{$active}}</li>
        @endif

    </ol>
</nav>