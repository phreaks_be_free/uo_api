<div class="btn-group" role="group" aria-label="Resume Skills Task Button Group">
    <button class="btn btn-dark" type="button" data-toggle="collapse" data-target="#resume-skills" aria-expanded="false"
        aria-controls="resume-skills">Resume Skills</button>
    <a href="{{url()->current()}}/skill/create" class="btn btn-primary">Add New Skills</a>
</div>
<div id="resume-skills" class="collapse">
    <table class="table table-sm table-hover table-bordered">
        <thead class="thead-light">
            <tr>
                <th>id</th>
                <th>name</th>
                <th>tag</th>
                <th>actions</th>
            </tr>
        </thead>
        <tbody>
            @if(!empty($resume_skills))
            @foreach ($resume_skills as $skill)
            <tr>
                <th>{{ $skill['id'] }}</th>
                <td>{{ $skill['name'] }}</td>
                <td>{{ $skill['tag'] }}</td>
                <td class="text-center">
                <a title="Edit" href="/resume/skill/{{$skill['id']}}/edit" class="text-decoration-none">
                        <i class="text-body fas fa-edit"></i>
                    </a>
                    <form action="/resume/skill/delete" class="form-check-inline" method="post">
                    {{ csrf_field() }}
                    {{ method_field('delete') }}
                    <input type="hidden" name="id" value="{{$skill['id']}}">
                    <button type="submit" title="Delete" class="btn btn-link">
                        <i class="text-danger fas fa-trash"></i>
                    </button>
                    </form>
                </td>
            </tr>
            @endforeach
            @endif
        </tbody>
    </table>
</div>