<div class="btn-group" role="group" aria-label="Resume Profile Task Button Group">
    <button class="btn btn-dark" type="button" data-toggle="collapse" data-target="#profile-employment"
        aria-expanded="false" aria-controls="profile-employment">Resume Profile Employment</button>
    <a href="/resume/profile/employment/create" class="btn btn-primary">Add New Employment</a>
</div>
<div id="profile-employment" class="collapse">
    <table class="table table-hover table-bordered">
        <thead class="thead-light">
            <tr>
                <th>id</th>
                <th>profile_id</th>
                <th>company_name</th>
                <th>position_description</th>
                <th>started_at</th>
                <th>ended_at</th>
                <th>current</th>
                <th>actions</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($resumes as $resume)

            @if(!empty($resume['profile']['employment']))
            @foreach ($resume['profile']['employment'] as $emp)
            <tr>
                <th>{{$emp['id']}}</th>
                <td><a href="/resume/profile/{{$emp['resume_profile_id']}}">
                        {{$emp['resume_profile_id']}}
                    </a></td>
                <td>{{$emp['company_name']}}</td>
                <td>{{$emp['position_description']}}</td>
                <td>{{$emp['started_at']}}</td>
                <td>{{$emp['ended_at']}}</td>
                <td>{{$emp['current']}}</td>
                <td class="text-center">
                    <a title="Edit" href="/resume/profile/employment/{{$emp['id']}}/edit" class="text-decoration-none">
                        <i class="text-body fas fa-edit"></i>
                    </a>
                    <form action="/resume/profile/employment/delete" class="form-check-inline" method="post">
                        {{ csrf_field() }}
                        {{ method_field('delete') }}
                        <input type="hidden" name="id" value="{{$emp['id']}}">
                        <button type="submit" title="Delete" class="btn btn-link">
                            <i class="text-danger fas fa-trash"></i>
                        </button>
                    </form>
                </td>
            </tr>
            @endforeach
            @endif

            @endforeach
        </tbody>
    </table>
</div>