<div class="btn-group" role="group" aria-label="Resume Profile Skills Task Button Group">
    <button class="btn btn-dark" type="button" data-toggle="collapse" data-target="#profile-skills"
        aria-expanded="false" aria-controls="profile-skills">Resume Profile Skills</button>
    <a href="/resume/profile/skill/create" class="btn btn-primary">Assign Skill to Profile</a>
</div>
<div id="profile-skills" class="collapse">
    <table class="table table-sm table-hover table-bordered">
        <thead class="thead-light">
            <tr>
                <th>id</th>
                <th>profile_id</th>
                <th>tag</th>
                <th>name</th>
                <th>progress</th>
                <th>actions</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($resumes as $resume)
            @if(!empty($resume['profile']['skills']))
            @foreach ($resume['profile']['skills'] as $skill)
            <tr>
                <th>{{$skill['id']}}</th>
                <td><a href="/resume/profile/{{$resume['profile']['id']}}">
                        {{$resume['profile']['id']}}
                    </a></td>
                <td>{{$skill['resume_skill']['tag']}}</td>
                <td>{{$skill['resume_skill']['name']}}</td>
                <td>{{$skill['progress']}}</td>
                <td class="text-center">
                    <a title="Edit" href="/resume/profile/skill/{{$skill['id']}}/edit" class="text-decoration-none">
                        <i class="text-body fas fa-edit"></i>
                    </a>
                    <form action="/resume/profile/skill/delete" class="form-check-inline" method="post">
                        {{ csrf_field() }}
                        {{ method_field('delete') }}
                        <input type="hidden" name="id" value="{{$skill['id']}}">
                        <button type="submit" title="Delete" class="btn btn-link">
                            <i class="text-danger fas fa-trash"></i>
                        </button>
                    </form>
                </td>
            </tr>
            @endforeach
            @endif

            @endforeach
        </tbody>
    </table>
</div>