<div class="btn-group" role="group" aria-label="Resume Profile Task Button Group">
    <button class="btn btn-dark" type="button" data-toggle="collapse" data-target="#profile" aria-expanded="false"
        aria-controls="profile">Resume Profiles</button>
    <a class="btn btn-primary" href="/resume/profile/create">Add New Profile</a>
</div>
<div id="profile" class="collapse">
    <table class="table table-hover table-bordered">
        <thead class="thead-light">
            <tr>
                <th>id</th>
                <th>full_name</th>
                <th>resume_id</th>
                <th>title</th>
                <th>skills count</th>
                <th>actions</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($resumes as $resume)
            @if(!empty($resume['profile']['id']))
            <tr>
                <th>{{$resume['profile']['id']}}</th>
                <td><a href="/resume/profile/{{$resume['profile']['id']}}">
                        {{$resume['profile']['full_name']}}
                    </a></td>
                <td>{{$resume['profile']['resume_id']}}</td>
                <td>{{$resume['profile']['title']}}</td>
                <td>{{count($resume['profile']['skills'])}}</td>
                <td class="text-center">
                    <a title="Edit" href="/resume/profile/{{$resume['profile']['id']}}/edit"
                        class="text-decoration-none">
                        <i class="text-body fas fa-edit"></i>
                    </a>
                    <form action="/resume/profile/delete" class="form-check-inline" method="post">
                        {{ csrf_field() }}
                        {{ method_field('delete') }}
                        <input type="hidden" name="id" value="{{$resume['profile']['id']}}">
                        <button type="submit" title="Delete" class="btn btn-link">
                            <i class="text-danger fas fa-trash"></i>
                        </button>
                    </form>
                </td>
            </tr>
            @endif
            @endforeach
        </tbody>
    </table>
</div>