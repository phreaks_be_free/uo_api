<div class="btn-group" role="group" aria-label="Resume Profile Task Button Group">
    <button class="btn btn-dark" type="button" data-toggle="collapse" data-target="#profile-references"
        aria-expanded="false" aria-controls="profile-references">Resume Profile References</button>
    <a href="/resume/profile/reference/create" class="btn btn-primary">Add New Reference</a>
</div>
<div id="profile-references" class="collapse">
    <table class="table table-hover table-bordered">
        <thead class="thead-light">
            <tr>
                <th>id</th>
                <th>profile_id</th>
                <th>name</th>
                <th>email</th>
                <th>phone</th>
                <th>relationship</th>
                <th>actions</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($resumes as $resume)

            @if(!empty($resume['profile']['references']))
            @foreach ($resume['profile']['references'] as $ref)
            <tr>
                <th>{{$ref['id']}}</th>
                <td><a href="/resume/profile/{{$ref['resume_profile_id']}}">
                        {{$ref['resume_profile_id']}}
                    </a></td>
                <td>{{$ref['name']}}</td>
                <td>{{$ref['email']}}</td>
                <td>{{$ref['phone']}}</td>
                <td>{{$ref['relationship']}}</td>
                <td class="text-center">
                    <a title="Edit" href="/resume/profile/reference/{{$ref['id']}}/edit" class="text-decoration-none">
                        <i class="text-body fas fa-edit"></i>
                    </a>
                    <form action="/resume/profile/reference/delete" class="form-check-inline" method="post">
                        {{ csrf_field() }}
                        {{ method_field('delete') }}
                        <input type="hidden" name="id" value="{{$ref['id']}}">
                        <button type="submit" title="Delete" class="btn btn-link">
                            <i class="text-danger fas fa-trash"></i>
                        </button>
                    </form>
                </td>
            </tr>
            @endforeach
            @endif

            @endforeach
        </tbody>
    </table>
</div>