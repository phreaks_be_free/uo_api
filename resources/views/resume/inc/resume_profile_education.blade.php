<div class="btn-group" role="group" aria-label="Resume Profile Education Task Button Group">
    <button class="btn btn-dark" type="button" data-toggle="collapse" data-target="#profile-education"
        aria-expanded="false" aria-controls="profile-education">Resume Profile Education</button>
    <a href="/resume/profile/education/create" class="btn btn-primary">Add New Education</a>
</div>
<div id="profile-education" class="collapse">
    <table class="table table-hover table-bordered">
        <thead class="thead-light">
            <tr>
                <th>id</th>
                <th>profile_id</th>
                <th>description</th>
                <th>institute</th>
                <th>started_at</th>
                <th>ended_at</th>
                <th>current</th>
                <th>actions</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($resumes as $resume)

            @if(!empty($resume['profile']['education']))
            @foreach ($resume['profile']['education'] as $edu)
            <tr>
                <th>{{$edu['id']}}</th>
                <td><a href="/resume/profile/{{$resume['profile']['id']}}">
                        {{$edu['resume_profile_id']}}
                    </a></td>
                <td>{{$edu['description']}}</td>
                <td>{{$edu['institute']}}</td>
                <td>{{$edu['started_at']}}</td>
                <td>{{$edu['ended_at']}}</td>
                <td>{{$edu['current']}}</td>
                <td class="text-center">
                    <a title="Edit" href="/resume/profile/education/{{$edu['id']}}/edit" class="text-decoration-none">
                        <i class="text-body fas fa-edit"></i>
                    </a>
                    <form action="/resume/profile/education/delete" class="form-check-inline" method="post">
                        {{ csrf_field() }}
                        {{ method_field('delete') }}
                        <input type="hidden" name="id" value="{{$edu['id']}}">
                        <button type="submit" title="Delete" class="btn btn-link">
                            <i class="text-danger fas fa-trash"></i>
                        </button>
                    </form>
                </td>
            </tr>
            @endforeach
            @endif

            @endforeach
        </tbody>
    </table>
</div>