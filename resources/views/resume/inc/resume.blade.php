<div class="btn-group" role="group" aria-label="Resume Task Button Group">
    <button class="btn btn-dark" type="button" data-toggle="collapse" data-target="#resume" aria-expanded="false"
        aria-controls="resume">Resume</button>
    <a href="{{url()->current()}}/create" class="btn btn-primary">Add New Resume</a>

</div>
<div class="collapse show" id="resume">

    <table class="table table-hover table-bordered">
        <thead class="thead-light">
            <tr>
                <th>id</th>
                <th>name</th>
                <th>created_at</th>
                <th>updated_at</th>
                <th>default</th>
                <th>application_id</th>
                <th>actions</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($resumes as $resume)
            <tr>
                <th>{{$resume['id']}}</th>
                <td>{{$resume['name']}}</td>
                <td>{{$resume['created_at']}}</td>
                <td>{{$resume['updated_at']}}</td>
                <td>{{$resume['default']}}</td>
                <td>{{$resume['application_id']}}</td>
                <td class="text-center">
                        <a title="Edit" href="/resume/{{$resume['id']}}/edit" class="text-decoration-none">
                                <i class="text-body fas fa-edit"></i>
                            </a>
                            <form action="/resume/delete" class="form-check-inline" method="post">
                            {{ csrf_field() }}
                            {{ method_field('delete') }}
                            <input type="hidden" name="id" value="{{$resume['id']}}">
                            <button type="submit" title="Delete" class="btn btn-link">
                                <i class="text-danger fas fa-trash"></i>
                            </button>
                            </form>
                        </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>