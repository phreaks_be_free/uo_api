<div class="btn-group" role="group" aria-label="Resume Skills Task Button Group">
    <button class="btn btn-dark" type="button" data-toggle="collapse" data-target="#resume-project-displays"
        aria-expanded="false" aria-controls="resume-project-displays">Resume Project Displays</button>
    <a href="/resume/project/display/create" class="btn btn-primary">Add New Project Display</a>
</div>
<div id="resume-project-displays" class="collapse">
    <table class="table table-sm table-hover table-bordered">
        <thead class="thead-light">
            <tr>
                <th>id</th>
                <th>name</th>
                <th>tag</th>
                <th>actions</th>
            </tr>
        </thead>
        <tbody>
            @if(!empty($resume_project_displays))
            @foreach ($resume_project_displays as $display)
            <tr>
                <th>{{ $display['id'] }}</th>
                <td>{{ $display['name'] }}</td>
                <td>{{ $display['tag'] }}</td>
                <td style="width: 17%" class="text-center">
                    <a title="Edit" href="/resume/project/display/{{$display['id']}}/edit" class="text-decoration-none">
                        <i class="text-body fas fa-edit"></i>
                    </a>
                    <form action="/resume/project/display/delete" class="form-check-inline" method="post">
                        {{ csrf_field() }}
                        {{ method_field('delete') }}
                        <input type="hidden" name="id" value="{{$display['id']}}">
                        <button type="submit" title="Delete" class="btn btn-link">
                            <i class="text-danger fas fa-trash"></i>
                        </button>
                    </form>
                </td>
            </tr>
            @endforeach
            @endif
        </tbody>
    </table>
</div>