<div class="btn-group" role="group" aria-label="Resume Project Skills Task Button Group">
    <button class="btn btn-dark" type="button" data-toggle="collapse" data-target="#project-skills"
        aria-expanded="false" aria-controls="project-skills">Resume Project Skills</button>
    <a href="/resume/project/skill/create" class="btn btn-primary">Assign Skill to Project</a>
</div>
<div id="project-skills" class="collapse">
    <table class="table table-sm table-hover table-bordered">
        <thead class="thead-light">
            <tr>
                <th>id</th>
                <th>project_id</th>
                <th>tag</th>
                <th>name</th>
                <th>actions</th>
            </tr>
        </thead>
        <tbody>
            @if(!empty($resume_project_skills))
            @foreach ($resume_project_skills as $skill)
            <tr>
                <th>{{$skill['id']}}</th>
                <td><a href="/resume/project/{{$skill['resume_project_id']}}">
                        {{$skill['resume_project_id']}}
                    </a></td>
                <td>{{$skill['resume_skill']['tag']}}</td>
                <td>{{$skill['resume_skill']['name']}}</td>
                <td class="text-center">
                    <a title="Edit" href="/resume/project/skill/{{$skill['id']}}/edit" class="text-decoration-none">
                        <i class="text-body fas fa-edit"></i>
                    </a>
                    <form action="/resume/project/skill/delete" class="form-check-inline" method="post">
                        {{ csrf_field() }}
                        {{ method_field('delete') }}
                        <input type="hidden" name="id" value="{{$skill['id']}}">
                        <button type="submit" title="Delete" class="btn btn-link">
                            <i class="text-danger fas fa-trash"></i>
                        </button>
                    </form>
                </td>
            </tr>
            @endforeach
            @endif
        </tbody>
    </table>
</div>