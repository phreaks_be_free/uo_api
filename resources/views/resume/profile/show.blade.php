@extends('layouts.internal')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            @include('resume.inc.breadcrumb', ['active' => 'Profile'])
            @if(!empty($profile['id']))
            <a href="{{ url()->current() . '/edit' }}" class="btn btn-sm btn-primary">Edit</a>
            <div id="profile" class="">
                <table class="table table-striped">
                    <tbody>
                        <tr>
                            <th style="width: 10%">Full Name</th>
                            <td>{{$profile['full_name']}}</td>
                        </tr>
                        <tr>
                            <th style="width: 10%">Title</th>
                            <td>{{$profile['title']}}</td>
                        </tr>
                        <tr>
                            <th style="width: 10%">Email</th>
                            <td>{{$profile['email']}}</td>
                        </tr>
                        <tr>
                            <th style="width: 10%">Phone</th>
                            <td>{{$profile['phone']}}</td>
                        </tr>
                        <tr>
                            <th style="width: 10%">Objective</th>
                            <td>{{$profile['objective']}}</td>
                        </tr>
                        <tr>
                            <th style="width: 10%">Biography</th>
                            <td>{{$profile['biography']}}</td>
                        </tr>
                        <tr>
                            <th style="width: 10%">Slider Text</th>
                            <td>{{$profile['slider_text']}}</td>
                        </tr>
                        <tr>
                            <th style="width: 10%">DP URL</th>
                            <td>{{$profile['display_picture_url']}}</td>
                        </tr>
                        <tr>
                            <th style="width: 10%">Skills</th>
                            <td>{{ implode(' | ', array_column(array_column($profile['skills'], 'resume_skill'), 'name')) }}
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
            @endif
        </div>
    </div>
</div>
@endsection