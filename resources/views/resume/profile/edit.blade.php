@extends('layouts.internal-center')
@section('scripts')
<script src="{{ asset('js/resume/create_profile.js') }}"></script>
@endsection
@section('page')
@include('resume.inc.breadcrumb', ['active' => 'Edit Profile'])
@if(!empty($resumes) && !empty($profile['id']))
<hr>
<form action="/resume/profile/update" method="post" class="mb-5 pb-5">
    {{ csrf_field() }}
    {{ method_field('put') }}
<input type="hidden" name="id" value="{{ $profile['id'] }}">
    <div class="form-group">
        <label for="resume">Resume</label>
        <select name="resume_id" class="form-control">
            @foreach ($resumes as $resume)
            <option @if(old('resume_id', $profile['resume_id'])==$resume['id']) selected @endif
                value="{{ $resume['id'] }}">{{ $resume['id'] . ': ' . $resume['name'] }}</option>
            @endforeach
        </select>
    </div>
    <div class="form-group">
        <label for="full_name">Full Name</label>
    <input value="{{ old('full_name', $profile['full_name']) }}" type="text" name="full_name" id="full_name" class="form-control">
    </div>
    <div class="form-group">
        <label for="title">Title</label>
        <input value="{{ old('title', $profile['title']) }}" type="text" name="title" id="title" class="form-control">
    </div>
    <div class="form-group">
        <label for="email">Email</label>
        <input value="{{ old('email', $profile['email']) }}" type="text" name="email" id="email" class="form-control">
    </div>
    <div class="form-group">
        <label for="phone">Phone</label>
        <input value="{{ old('phone', $profile['phone']) }}" type="text" name="phone" id="phone" class="form-control">
    </div>
    <div class="form-group">
        <label for="objective">Objective</label>
        <input value="{{ old('objective', $profile['objective']) }}" type="text" name="objective" id="objective" class="form-control">
    </div>
    <div class="form-group">
        <label for="display_picture_url">Display Picture URL</label>
        <input value="{{ old('display_picture_url', $profile['display_picture_url']) }}" type="text" name="display_picture_url" id="display_picture_url" class="form-control">
    </div>
    <div class="form-group">
        <label for="biography">Biography</label>
        <textarea  name="biography" id="biography" rows="2" class="form-control">{{ old('biography', $profile['biography']) }}</textarea>
    </div>
    <div class="form-group">
        <label for="slider_text">Slider Text</label>
        <textarea name="slider_text" id="slider_text" rows="2" class="form-control">{{ old('slider_text', $profile['slider_text']) }}</textarea>
    </div>

    <button type="submit" value="Submit" class="my-5 btn btn-primary btn-block">Submit
    </button>
    @endif
    @endsection