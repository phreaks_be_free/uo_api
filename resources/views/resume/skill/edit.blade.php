@extends('layouts.internal-center')
@section('scripts')
<script src="{{ asset('js/resume/index.js') }}"></script>
@endsection
@section('page')
@include('resume.inc.breadcrumb', ['active' => 'Edit Skil'])
@if(!empty($skill) && is_array($skill))
<form action="{{ url()->route('resume') }}/skill/update" method="post">
    {{ csrf_field() }}
    {{ method_field('put') }}
    <input type="hidden" name="id" value="{{ $skill['id'] }}">
    <div class="form-group">
        <label for="name">Name</label>
    <input type="text" name="name" value="{{ $skill['name'] }}" id="name" class="form-control">
    </div>
    <div class="form-group">
        <label for="tag">Tag</label>
        <input type="text" name="tag" value="{{ $skill['tag'] }}" id="tag" class="form-control">
    </div>

    <button type="submit" value="Submit" class="my-5 btn btn-primary btn-block">Submit
    </button>
</form>
@endif
@endsection