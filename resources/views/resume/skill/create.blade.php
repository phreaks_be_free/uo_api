@extends('layouts.internal-center')
@section('page')
@include('resume.inc.breadcrumb', ['active' => 'Create Skill'])
<form action="{{ url()->route('resume') }}/skill/store" method="post">
    {{ csrf_field() }}
    <div class="form-group"><label for="name">Name</label><input type="text" name="name" id="name" class="form-control">
    </div>
    <div class="form-group"><label for="tag">Tag</label><input type="text" name="tag" id="tag" class="form-control">
    </div>
    <button type="submit" value="Submit" class="my-5 btn btn-primary btn-block">Submit
    </button>
</form>
@endsection