@extends('layouts.internal-center')
@section('scripts')
<script src="{{ asset('js/resume/index.js') }}"></script>
@endsection
@section('page')
@include('resume.inc.breadcrumb', ['active' => 'Edit Project Skill'])
@if(!empty($project_skill))
<form action="{{ url()->route('resume') }}/project/skill/update" method="post">
    {{ csrf_field() }}
    {{ method_field('put') }}
<input type="hidden" name="project_skill_id" value="{{ $project_skill['id'] }}">
    <div class="form-group">
        <label>Resume Project</label>
        @if(!empty($resume_projects) && is_array($resume_projects))
        <select name="resume_project" class="form-control">
            @foreach($resume_projects as $project)
            <option @if(old('resume_project', $project_skill['resume_project_id'])==$project['id']) selected @endif value="{{$project['id']}}">
                {{$project['id'] . ': ' . $project['title']}}</option>
            @endforeach
        </select>
        @endif
    </div>
    <div class="form-group">
        <label>Resume Skill</label>
        @if(!empty($resume_skills) && is_array($resume_skills))
        <select name="resume_skill" class="form-control">
            @foreach($resume_skills as $skill)
            <option @if(old('resume_skill', $project_skill['resume_skill_id'])==$skill['id']) selected @endif value="{{$skill['id']}}">
                {{$skill['name']}}</option>
            @endforeach
        </select>
        @endif
    </div>
    <div class="col-md-12 text-right">

        <button type="submit" value="Submit" class="btn btn-primary">Submit
        </button>
    </div>
</form>
@endif
@endsection