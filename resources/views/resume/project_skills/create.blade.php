@extends('layouts.internal-center')
@section('scripts')
<script src="{{ asset('js/resume/index.js') }}"></script>
@endsection
@section('page')
@include('resume.inc.breadcrumb', ['active' => 'Create Project Skill'])
<form action="{{ url()->route('resume') }}/project/skill/store" method="post">
    {{ csrf_field() }}
    <div class="form-group">
        <label>Resume Project</label>
        @if(!empty($resume_projects) && is_array($resume_projects))
        <select name="resume_project" class="form-control">
            @foreach($resume_projects as $project)
            <option @if(old('resume_project')==$project['id']) selected @endif value="{{$project['id']}}">
                {{$project['id'] . ': ' . $project['title']}}</option>
            @endforeach
        </select>
        @endif
    </div>
    <div class="form-group">
        <label>Resume Skill</label>
        @if(!empty($resume_skills) && is_array($resume_skills))
        <select name="resume_skill" class="form-control">
            @foreach($resume_skills as $skill)
            <option @if(old('resume_skill')==$skill['id']) selected @endif value="{{$skill['id']}}">
                {{$skill['name']}}</option>
            @endforeach
        </select>
        @endif
    </div>
    <div class="col-md-12 text-right">

        <button type="submit" value="Submit" class="btn btn-primary">Submit
        </button>
    </div>
</form>
@endsection