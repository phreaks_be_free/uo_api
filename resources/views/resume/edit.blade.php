@extends('layouts.internal-center')
@section('scripts')
<script src="{{ asset('js/resume/index.js') }}"></script>
@endsection
@section('page')
@include('resume.inc.breadcrumb', ['active' => 'Edit'])
@if(!empty($resume) && is_array($resume))
<form action="/resume/update" method="post">
    {{ csrf_field() }}
    {{ method_field('put') }}
<input type="hidden" name="resume_id" value="{{ $resume['id'] }}">
    <div class="form-group">
        <label for="name">Name</label>
        <input required value="{{ old('name', $resume['name']) }}" type="text" name="name" id="name " class="form-control">
    </div>
    <div class="form-group">
        <label for="applcation_id">Application</label>
        @if (is_array($applications) && !empty($applications))

        <select name="application_id" class="form-control" id="application">
            @foreach ($applications as $app)
            <option @if(old('application_id', $resume['application_id'])==$app['id']) selected @endif
                value="{{ $app['id'] }}">{{ $app['name'] }}</option>
            @endforeach
        </select>
        @endif
    </div>
    <div class="form-check">
        <input @if(old('default', $resume['default'])) checked @endif name="default" type="checkbox" class="form-check-input" id="default">
        <label class="form-check-label" for="default">Default</label>
    </div>
    <input type="submit" value="Submit" class="mt-3 btn btn-primary">
</form>
@endif

@endsection