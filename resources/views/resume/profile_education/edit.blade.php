@extends('layouts.internal-center')
@section('scripts')
<script src="{{ asset('js/resume/index.js') }}"></script>
@endsection
@section('page')
@include('resume.inc.breadcrumb', ['active' => 'Edit Profile Education'])
@if(!empty($profile_education))
<form action="/resume/profile/education/update" method="post">
    {{ csrf_field() }}
    {{ method_field('put') }}
    <input type="hidden" name="profile_education_id" value="{{ $profile_education['id'] }}">
    <div class="form-group">
        <label>Resume Profile</label>
        @if(!empty($resume_profiles) && is_array($resume_profiles))
        <select name="resume_profile_id" class="form-control">
            @foreach($resume_profiles as $profile)
            <option @if(old('resume_profile_id', $profile_education['resume_profile_id'])==$profile['id']) selected
                @endif value="{{$profile['id']}}">
                {{$profile['id'] . ': ' . $profile['full_name']}}</option>
            @endforeach
        </select>
        @endif
    </div>
    <div class="form-group">
        <label>Institute</label>
        <input type="text" name="profile_education_institute"
            value="{{ old('profile_education_institute', $profile_education['institute']) }}" class="form-control">
    </div>
    <div class="form-group">
        <label>Description</label>
        <input type="text" name="profile_education_description"
            value="{{ old('profile_education_description', $profile_education['description']) }}" class="form-control">
    </div>
    <div class="form-group">
        <label>Started at</label>
        <input type="date" name="profile_education_started_at"
            value="{{ old('profile_education_started_at', $profile_education['started_at']) }}" class="form-control">
    </div>
    <div class="form-group">
        <label>Ended at</label>
        <input type="date" name="profile_education_ended_at"
            value="{{ old('profile_education_ended_at', $profile_education['ended_at']) }}" class="form-control">
    </div>
    <div class="checkbox">
        <label><input type="checkbox" name="profile_education_current" @if(old('profile_education_current')=='on'
                ||!empty($profile_education['current']))checked @endif>Current</label>
    </div>
    <div class="col-md-12 text-right">

        <button type="submit" value="Submit" class="btn btn-primary">Submit
        </button>
    </div>
</form>
@endif
@endsection