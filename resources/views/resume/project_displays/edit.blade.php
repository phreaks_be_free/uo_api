@extends('layouts.internal-center')
@section('scripts')
<script src="{{ asset('js/resume/index.js') }}"></script>
@endsection
@section('page')
@include('resume.inc.breadcrumb', ['active' => 'Edit Project Display'])
@if($project_display)
<form action="{{ url()->route('resume') }}/project/display/update" method="post">
    {{ csrf_field() }}
    {{ method_field('put') }}
    <input type="hidden" name="project_display_id" value="{{ $project_display['id'] }}">
    <div class="form-group">
        <label for="name">Name</label>
        <input value="{{ old('name', $project_display['name']) }}" type="text" name="name" id="name"
            class="form-control">

    </div>
    <div class="form-group">
        <label for="tag">Tag</label>
        <input value="{{ old('name', $project_display['tag']) }}" type="text" name="tag" id="tag" class="form-control">
    </div>
    <button type="submit" value="Submit" class="my-5 btn btn-primary btn-block">Submit
    </button>
</form>
@endif
@endsection