@extends('layouts.internal-center')
@section('scripts')
<script src="{{ asset('js/resume/index.js') }}"></script>
@endsection
@section('page')
@include('resume.inc.breadcrumb', ['active' => 'Create Project Display'])
<form action="{{ url()->route('resume') }}/project/display/store" method="post">
    {{ csrf_field() }}
    <div class="form-group">
        <label for="name">Name</label>
        <input value="{{ old('name') }}" type="text" name="name" id="name" class="form-control">

    </div>
    <div class="form-group">
        <label for="tag">Tag</label>
        <input value="{{ old('tag') }}" type="text" name="tag" id="tag" class="form-control">
    </div>
    <button type="submit" value="Submit" class="my-5 btn btn-primary btn-block">Submit
    </button>
</form>
@endsection