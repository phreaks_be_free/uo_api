@extends('layouts.internal-center')
@section('scripts')
<script src="{{ asset('js/resume/index.js') }}"></script>
@endsection
@section('page')
@include('resume.inc.breadcrumb', ['active' => 'Edit Profile Employment'])
@if(!empty($profile_employment))
<form action="/resume/profile/employment/update" method="post">
    {{ csrf_field() }}
    {{ method_field('put') }}
    <input type="hidden" name="profile_employment_id" value="{{ $profile_employment['id'] }}">
    <div class="form-group">
        <label>Resume Profile</label>
        @if(!empty($resume_profiles) && is_array($resume_profiles))
        <select name="resume_profile_id" class="form-control">
            @foreach($resume_profiles as $profile)
            <option @if(old('resume_profile_id', $profile_employment['resume_profile_id'])==$profile['id']) selected
                @endif value="{{$profile['id']}}">
                {{$profile['id'] . ': ' . $profile['full_name']}}</option>
            @endforeach
        </select>
        @endif
    </div>
    <div class="form-group">
        <label>Company Name</label>
        <input type="text" name="profile_employment_company_name"
            value="{{ old('profile_employment_company_name', $profile_employment['company_name']) }}"
            class="form-control">
    </div>
    <div class="form-group">
        <label>Position Description</label>
        <input type="text" name="profile_employment_position_description"
            value="{{ old('profile_employment_position_description', $profile_employment['position_description']) }}"
            class="form-control">
    </div>
    <div class="form-group">
        <label>Started at</label>
        <input type="date" name="profile_employment_started_at"
            value="{{ old('profile_employment_started_at', $profile_employment['started_at']) }}" class="form-control">
    </div>
    <div class="form-group">
        <label>Ended at</label>
        <input type="date" name="profile_employment_ended_at"
            value="{{ old('profile_employment_ended_at', $profile_employment['ended_at']) }}" class="form-control">
    </div>
    <div class="checkbox">
        <label><input type="checkbox" name="profile_employment_current" @if(old('profile_employment_current')=='on'
                ||!empty($profile_employment['current']))checked @endif>Current</label>
    </div>
    <div class="col-md-12 text-right">

        <button type="submit" value="Submit" class="btn btn-primary">Submit
        </button>
    </div>
</form>
@endif
@endsection