@extends('layouts.internal-center')
@section('scripts')
<script src="{{ asset('js/resume/index.js') }}"></script>
@endsection
@section('page')
@include('resume.inc.breadcrumb', ['active' => 'Create Profile Skill'])
<form action="{{ url()->route('resume') }}/profile/skill/store" method="post">
    {{ csrf_field() }}
    <div class="form-group">
        <label>Resume Profile</label>
        @if(!empty($resume_profiles) && is_array($resume_profiles))
        <select name="resume_profile" class="form-control">
            @foreach($resume_profiles as $profile)
            <option @if(old('resume_profile')==$profile['id']) selected @endif value="{{$profile['id']}}">
                {{$profile['id'] . ': ' . $profile['full_name']}}</option>
            @endforeach
        </select>
        @endif
    </div>
    <div class="form-group">
        <label>Resume Skill</label>
        @if(!empty($resume_skills) && is_array($resume_skills))
        <select name="resume_skill" class="form-control">
            @foreach($resume_skills as $skill)
            <option @if(old('resume_skill')==$skill['id']) selected @endif value="{{$skill['id']}}">
                {{$skill['name']}}</option>
            @endforeach
        </select>
        @endif
    </div>
    <div class="form-group">
        <label>Progress</label>
        <input type="number" min="20" max="100"
            name="profile_skill_progress" value="{{ old('profile_skill_progress') }}" class="form-control">
    </div>
    <div class="col-md-12 text-right">

        <button type="submit" value="Submit" class="btn btn-primary">Submit
        </button>
    </div>
</form>
@endsection