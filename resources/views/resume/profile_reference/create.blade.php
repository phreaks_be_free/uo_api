@extends('layouts.internal-center')
@section('scripts')
<script src="{{ asset('js/resume/index.js') }}"></script>
@endsection
@section('page')
@include('resume.inc.breadcrumb', ['active' => 'Create Profile Reference'])
<form action="/resume/profile/reference/store" method="post">
    {{ csrf_field() }}
    <div class="form-group">
        <label>Resume Profile</label>
        @if(!empty($resume_profiles) && is_array($resume_profiles))
        <select name="resume_profile_id" class="form-control">
            @foreach($resume_profiles as $profile)
            <option @if(old('resume_profile_id')==$profile['id']) selected @endif value="{{$profile['id']}}">
                {{$profile['id'] . ': ' . $profile['full_name']}}</option>
            @endforeach
        </select>
        @endif
    </div>
    <div class="form-group">
        <label>Name</label>
        <input type="text" name="profile_reference_name" value="{{ old('profile_reference_name') }}"
            class="form-control">
    </div>
    <div class="form-group">
        <label>Email</label>
        <input type="email" name="profile_reference_email" value="{{ old('profile_reference_email') }}"
            class="form-control">
    </div>
    <div class="form-group">
        <label>Phone</label>
        <input type="text" name="profile_reference_phone" value="{{ old('profile_reference_phone') }}"
            class="form-control">
    </div>
    <div class="form-group">
        <label>Relationship</label>
        <input type="text" name="profile_reference_relationship" value="{{ old('profile_reference_relationship') }}"
            class="form-control">
    </div>
    <div class="col-md-12 text-right">

        <button type="submit" value="Submit" class="btn btn-primary">Submit
        </button>
    </div>
</form>
@endsection