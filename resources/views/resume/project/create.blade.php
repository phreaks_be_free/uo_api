@extends('layouts.internal-center')
@section('scripts')
<script src="{{ asset('js/resume/create_profile.js') }}"></script>
@endsection
@section('page')
@include('resume.inc.breadcrumb', ['active' => 'Create Project'])
@if(!empty($resumes) && !empty($project_displays))

<form action="/resume/project/store" method="post" class="mb-5 pb-5">
    {{ csrf_field() }}

    <div class="form-group">
        <label for="resume">Resume</label>
        <select name="resume_id" class="form-control">
            @foreach ($resumes as $resume)
            <option @if(old('resume_id')==$resume['id']) selected @endif value="{{ $resume['id'] }}">
                {{ $resume['id'] . ': ' . $resume['name'] }}</option>
            @endforeach
        </select>
    </div>
    <div class="form-group">
        <label for="project_display_id">Project Display</label>
        <select name="project_display_id" class="form-control">
            @foreach ($project_displays as $project_display)
            <option @if(old('project_display_id')==$project_display['id']) selected @endif
                value="{{ $project_display['id'] }}">
                {{ $project_display['id'] . ': ' . $project_display['name'] }}</option>
            @endforeach
        </select>
    </div>
    <div class="form-group">
        <label for="title">Title</label>
        <input value="{{ old('title') }}" type="text" name="title" id="title" class="form-control">
    </div>

    <div class="form-group">
        <label for="image_url">Image URL</label>
        <input value="{{ old('image_url') }}" type="text" name="image_url" id="image_url" class="form-control">
    </div>

    <div class="form-group">
        <label for="description">Description</label>
        <textarea name="description" id="description" rows="2" class="form-control">{{ old('description') }}</textarea>
    </div>

    <div class="form-group">
        <label for="content">Content</label>
        <textarea name="content" id="content" rows="3" class="form-control">{{ old('content') }}</textarea>
    </div>

    <button type="submit" value="Submit" class="my-5 btn btn-primary btn-block">Submit
    </button>
    @endif
    @endsection