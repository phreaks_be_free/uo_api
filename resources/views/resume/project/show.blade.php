@extends('layouts.internal')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
                @include('resume.inc.breadcrumb', ['active' => 'Project'])
            @if(!empty($project['id']))
            <a href="{{ url()->current() . '/edit' }}" class="btn btn-sm btn-primary">Edit</a>
            <div id="profile" class="">
                <table class="table table-striped">
                    <tbody>
                        <tr>
                            <th style="width: 10%">ID</th>
                            <td>{{$project['id']}}</td>
                        </tr>
                        <tr>
                            <th style="width: 10%">Resume ID</th>
                            <td>{{$project['resume_id']}}</td>
                        </tr>
                        <tr>
                            <th style="width: 10%">Display</th>
                            <td>{{$project['display']['name']}}</td>
                        </tr>
                        <tr>
                            <th style="width: 10%">Title</th>
                            <td>{{$project['title']}}</td>
                        </tr>
                        <tr>
                            <th style="width: 10%">Description</th>
                            <td>{{$project['description']}}</td>
                        </tr>
                        <tr>
                            <th style="width: 10%">Image URL</th>
                            <td>{{$project['image_url']}}</td>
                        </tr>
                        <tr>
                            <th style="width: 10%">Skills</th>
                            <td>{{ implode(' | ', array_column($project['skills'], 'name')) }}</td>
                        </tr>
                        <tr>
                            <th style="width: 10%">content</th>
                            <td>{{$project['content']}}</td>
                        </tr>
                    </tbody>
                </table>
            </div>
            @endif
        </div>
    </div>
</div>
@endsection