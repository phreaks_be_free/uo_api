@extends('layouts.internal')
@section('scripts')

@endsection
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-lg-12">
            @if(is_array($resume) && !empty($resume))
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb mt-2">
                    <li class="breadcrumb-item"><a href="{{ url('resume') }}">Resume</a></li>
                    <li class="breadcrumb-item active" aria-current="page">{{ $resume['name']}}</li>
                </ol>
            </nav>

            @if ($resume['profile'] && $resume['profile']['education']&& $resume['profile']['references']&&
            $resume['profile']['skills'] )
            <button class="btn btn-dark" type="button" data-toggle="collapse" data-target="#profile"
                aria-expanded="false" aria-controls="profile">Resume Profile</button>
            <div id="profile" class="collapse show">
                <table class="table table-bordered">
                    <tbody>
                        <tr>
                            <th style="width: 10%">Full Name</th>
                            <td>{{$resume['profile']['full_name']}}</td>
                        </tr>
                        <tr>
                            <th style="width: 10%">Title</th>
                            <td>{{$resume['profile']['title']}}</td>
                        </tr>
                        <tr>
                            <th style="width: 10%">Email</th>
                            <td>{{$resume['profile']['email']}}</td>
                        </tr>
                        <tr>
                            <th style="width: 10%">Objective</th>
                            <td>{{$resume['profile']['objective']}}</td>
                        </tr>
                        <tr>
                            <th style="width: 10%">Biography</th>
                            <td>{{$resume['profile']['biography']}}</td>
                        </tr>
                        <tr>
                            <th style="width: 10%">Slider Text</th>
                            <td>{{$resume['profile']['slider_text']}}</td>
                        </tr>
                        <tr>
                            <th style="width: 10%">DP URL</th>
                            <td>{{$resume['profile']['display_picture_url']}}</td>
                        </tr>
                        <tr>
                            <th style="width: 10%">Skills</th>
                            <td>{{ implode(' | ', array_column($resume['profile']['skills'], 'name')) }}</td>
                        </tr>
                        <tr>
                            <th style="width: 10%">Education</th>
                            <td>
                                <table class="mt-1 table table-sm table-bordered">
                                    <thead class="thead-light">
                                        <tr>
                                            <th scope="col">id</th>
                                            <th scope="col">description</th>
                                            <th scope="col">institute</th>
                                            <th scope="col">started_at</th>
                                            <th scope="col">ended_at</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($resume['profile']['education'] as $edu)
                                        <tr>
                                            <th scope="row">{{$edu['id']}}</th>
                                            <td>{{$edu['description']}}</td>
                                            <td>{{$edu['institute']}}</td>
                                            <td>{{$edu['started_at']}}</td>
                                            <td>{{$edu['ended_at']}}</td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <th style="width: 10%">Employment</th>
                            <td>
                                <table class="mt-1 table table-sm table-bordered">
                                    <thead class="thead-light">
                                        <tr>
                                            <th scope="col">id</th>
                                            <th scope="col">company_name</th>
                                            <th scope="col">position_description</th>
                                            <th scope="col">started_at</th>
                                            <th scope="col">ended_at</th>
                                            <th scope="col">current</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($resume['profile']['employment'] as $emp)
                                        <tr>
                                            <th scope="row">{{$emp['id']}}</th>
                                            <td>{{$emp['company_name']}}</td>
                                            <td>{{$emp['position_description']}}</td>
                                            <td>{{$emp['started_at']}}</td>
                                            <td>{{$emp['ended_at']}}</td>
                                            <td>{{$emp['current']}}</td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <th style="width: 10%">References</th>
                            <td>
                                <table class="mt-1 table table-sm table-bordered">
                                    <thead class="thead-light">
                                        <tr>
                                            <th scope="col">id</th>
                                            <th scope="col">name</th>
                                            <th scope="col">email</th>
                                            <th scope="col">phone</th>
                                            <th scope="col">relationship</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($resume['profile']['references'] as $ref)
                                        <tr>
                                            <th scope="row">{{$ref['id']}}</th>
                                            <td>{{$ref['name']}}</td>
                                            <td>{{$ref['email']}}</td>
                                            <td>{{$ref['phone']}}</td>
                                            <td>{{$ref['relationship']}}</td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <hr>
            @else
            <a href="{{ url()->current() }}/profile/create" class="btn btn-primary">Create Profile</a>
            @endif
            <a href="{{ url()->current() }}/profile/create" class="btn btn-primary">Create Project</a>
            @if($resume['projects'])
            <button class="btn btn-dark" type="button" data-toggle="collapse" data-target="#projects"
                aria-expanded="false" aria-controls="profile">Resume Projects</button>
            <div id="projects" class="collapse show">
                <table class="table table-bordered">
                    <thead class="thead-light ">
                        <tr>
                            <th>id</th>
                            <th>title</th>
                            <th>description</th>
                            <th>display</th>
                            <th>skills</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($resume['projects'] as $project)
                        <tr>
                            <th>{{$project['id']}}</th>
                            <td>{{$project['title']}}</td>
                            <td>{{$project['description']}}</td>
                            <td>{{$project['display']['name'] }}</td>
                            <td>{{ implode(", ", array_column($project['skills'], 'name')) }}</td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            @endif
            @endif
        </div>
    </div>
</div>
@endsection