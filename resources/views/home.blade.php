@extends('layouts.internal-center')
@section('scripts')
<link href="/css/home.css" rel="stylesheet" type="text/css" />
@endsection
@section('page')
<div class="row">
    <div class="col-md-6 home-data-panel">
        <div class="card">
            <div class="card-header">
                Applications
            </div>
            <ul class="list-group list-group-flush">
                <li class="list-group-item">
                    <a href="/application/create"><i class="fas fa-tablet-alt"></i> Create New Application</a>
                </li>
                <li class="list-group-item">
                    <a href="/application"><i class="fas fa-list"></i> All Applications</a>
                </li>
            </ul>
        </div>
    </div>
    <div class="col-md-6 home-data-panel">
        <div class="card">
            <div class="card-header">
                Resumes
            </div>
            <ul class="list-group list-group-flush">
                <li class="list-group-item">
                    <a href="/resume/create"><i class="fas fa-file"></i> Create New Resume</a>
                </li>
                <li class="list-group-item">
                    <a href="/resume"><i class="fas fa-list"></i> All Resume Data</a>
                </li>
            </ul>
        </div>
    </div>

    <div class="col-md-6 home-data-panel">
        <div class="card">
            <div class="card-header">
                Featured Items
            </div>

        </div>
    </div>
</div>
{{-- 
    <div class="row">
        <div class="col-md-6">
            <div class="card">
                <div class="card-header">
                    Featured News
                </div>

            </div>
        </div>
    </div> --}}

@endsection