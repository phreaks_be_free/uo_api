<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>UnicornOverlord</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
        <link href="/css/doormat.css" type="text/css" rel="stylesheet" />
    </head>

    <body>
        @if (Auth::check())
        <script>
            window.location = "/home";
        </script>
        @else
        <div class="flex-center position-ref full-height">
            @if (Route::has('login'))
            <div class="top-right links">
                <a href="{{ url('/login') }}">Login</a>
            </div>
            @endif

            <div class="content">
                <div class="title m-b-md">
                    Oops, I think we might have gotten lost....
                </div>
                <div class="links">
                    return to <a href="https://www.unicornoverlord.com">UnicornOverlord</a>
                </div>
            </div>
        </div>

        @endif
    </body>


</html>