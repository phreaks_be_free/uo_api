@extends('_archive.app')

@section('content')

    <form method="post" action="/uploads" enctype="multipart/form-data">
        {{csrf_field()}}
        <input type="file" name="uploaded_file[]" multiple/>

        <button type="submit">Upload File</button>
    </form>
@endsection
