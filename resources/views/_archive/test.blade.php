@extends('layouts.internal')

@section('content')

    <script>
        $(function(){

            var token = null;

            function startup(login){
                if(login == 0){
                    $('#login').show();
                    $('#editUser').hide();
                    $('#uploadFile').hide();
                }else{
                    $('#login').hide();
                    $('#editUser').show();
                    $('#uploadFile').show();
                }
            }

            startup(0);



            $('#login').on('submit',function(e){
                e.preventDefault();
                e.stopPropagation();

                $.ajax({
                    url:'/api/v1/user/login',
                    type:'post',
                    dataType:'json',
                    data:{
                        'email':$(this).find('input[name="email"]').val(),
                        'password':$(this).find('input[name="password"]').val(),
                        'csrf_field':$(this).find('input[name="_token"]').val()
                    }
                }).done(function (response) {

                    if(response.result == true){
                        startup(1);
                        $('body').prepend('Login Successful');
                        $('#token_return').val(response.token);
                    }
                    else{
                        startup(0);
                        $('body').prepend('Login Failed');
                        token = null;
                    }

                });
            });





        });
    </script>
    <form id='login' method="post">
        <label for="email">Email</label><input type="text" name="email"/>
        <label for="password">Password</label><input type="password" name="password"/>
        <input type="submit"/>

        {{csrf_field()}}
    </form>




    <form id='editUser' method="post" action="http://footprint.app/api/v1/user/manage">
        <label>Existing User Id</label><input type="number" name="id" value="2"/>
        <label>First Name</label><input type="text" name="first_name" value="User123"/>
        <label>Last Name</label><input type="text" name="last_name" value="User321"/>
        <label>Weight</label><input type="text" name="weight" value="25"/>
        <label>DOB</label><input type="text" name="dob" value="05/29/1987"/>
        <input type="hidden" name="token"/>
        <input type="submit"/>
        {{csrf_field()}}
    </form>
    <hr/>

    <form id="uploadFile" method="post" action="/api/v1/file/upload" enctype="multipart/form-data">
        {{csrf_field()}}
        <input type="file" name="uploaded_file[]" multiple/>

        <button type="submit">Upload File</button>
    </form>

    <input type="text" id="token_return"/>

@endsection