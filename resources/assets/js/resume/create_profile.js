$(document).ready(() => {


    $('#add-skill-row').click(() => {
        let length = $('.skill-row').length;
        let html = '<div class="skill-row row mb-3"><div class="col-sm-4 form-group"><input type="text" placeholder="Tag" name="skill[' + length + '][tag]" class="form-control"></div><div class="col-sm-4 form-group"><input type="text" placeholder="Name" name="skill[' + length + '][name]" class="form-control"></div></div>'
        $('#skill-container').append(html);
    });

    // '<div class="col-sm-4 form-group"><input type="number" placeholder="Progress %" min="20" max="100" name="skill[' + length + '][progress]" class="form-control"></div>'

    $('#add-education-row').click(() => {
        let length = $('.education-row').length;
        let html = '<div class="education-row row mb-3"><div class="col-sm-6 form-group"><label>Started at</label><input class="form-control" type="date" name="education[' + length + '][started_at]"></div><div class="col-sm-6 form-group"><label>Ended at</label><input class="form-control" type="date" name="education[' + length + '][ended_at]"></div><div class="col-sm-5 form-group"><input type="text" placeholder="Description" name="education[' + length + '][description]" class="form-control"></div><div class="col-sm-5 form-group"><input type="text" placeholder="Institute" name="education[' + length + '][institute]" class="form-control"></div><div class="col-sm-2 form-group text-center align-self-center"><input type="checkbox" name="education[' + length + '][current]" class="form-check-input"><label class="form-check-label">Current</label></div></div>'
        $('#education-container').append(html);
    });
    $('#add-employment-row').click(() => {
        let length = $('.employment-row').length;
        let html = '<div class="row employment-row mb-3"> <div class="col-sm-6 form-group"> <label>Started at</label> <input class="form-control" type="date" name="employment[' + length + '][started_at]"> </div> <div class="col-sm-6 form-group"> <label>Ended at</label> <input class="form-control" type="date" name="employment[' + length + '][ended_at]"> </div> <div class="col-sm-5 form-group"><input type="text" placeholder="Company Name" name="employment[' + length + '][company_name]" class="form-control"> </div> <div class="col-sm-5 form-group"><input type="text" placeholder="Position Description" name="employment[' + length + '][position_description]" class="form-control"></div> <div class="col-sm-2 form-group text-center align-self-center"> <input type="checkbox" name="employment[' + length + '][current]" class="form-check-input"> <label class="form-check-label">Current</label> </div> </div>'
        $('#employment-container').append(html);
    });
    $('#add-reference-row').click(() => {
        let length = $('.reference-row').length;
        let html = '<div class="row reference-row mb-3"> <div class="col-sm-6 form-group"><input type="text" placeholder="Full Name" name="reference[' + length + '][name]" class="form-control"> </div> <div class="col-sm-6 form-group"><input type="text" placeholder="Relationship" name="reference[' + length + '][relationship]" class="form-control"> </div> <div class="col-sm-6 form-group"><input type="email" placeholder="Email" name="reference[' + length + '][email]" class="form-control"> </div> <div class="col-sm-6 form-group"><input type="text" placeholder="Phone" name="reference[' + length + '][phone]" class="form-control"></div> </div>';
        $('#reference-container').append(html);
    })
});