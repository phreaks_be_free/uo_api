$(document).ready(() => {
    $('#add-skill-row').click(() => {
        let length = $('.skill-row').length;
        let html = '<div class="skill-row row "><div class="col-sm-6 form-group"><input type="text" placeholder="Tag" name="skill[' + length + '][tag]" class="form-control"></div><div class="col-sm-6 form-group"><input type="text" placeholder="Name" name="skill[' + length + '][name]" class="form-control"></div></div>'
        $('#skill-container').append(html);
    });
})