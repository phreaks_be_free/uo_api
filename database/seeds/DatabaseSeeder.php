<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::table('applications')->insert([
            'name' => 'First Application',
            'url' => 'http://locahost',
            'user_id' => 2,
            'active' => 1,
        ]
        );
        DB::table('application_users')->insert([
            'role' => 'Visitor',
            'first_name' => 'foo',
            'last_name' => 'bar',
            'email' => 'foo@bar.com',
            'application_id' => 1,

        ]
        );
    }
}
