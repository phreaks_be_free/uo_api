<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ResumePages extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
	    Schema::create( 'resume_pages', function ( Blueprint $table ) {
		    $table->increments( 'id' );
		    $table->string( 'name' );
	    } );
	    DB::table( 'resume_pages' )->insert(
		    [
			    [
				    'name' => 'about',
			    ],
			    [
				    'name' => 'intro'
			    ]
		    ]
	    );
	    Schema::table('resume_lineup', function (Blueprint $table) {
		    $table->unsignedInteger('resume_page_id');
	    });






    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
