<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ApplicationsData extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::disableForeignKeyConstraints();

        DB::table('applications')->insert([
            [
                'name' => 'Dev',
                'url' => 'http://localhost:4200',
                'user_id' => 1,
                'active' => 1,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now()
            ],
            [
                'name' => 'localhost',
                'url' => 'http://localhost:4200',
                'user_id' => 2,
                'active' => 1,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now()
            ]
        ]);



        DB::table('application_users')->insert([
            'role' => 'admin',
            'first_name' => 'Ali',
            'last_name' => 'Raza',
            'email' => 'artanha7@gmail.com',
            'application_id' => 2,
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now()
        ]);

        DB::table('resume')->insert([
            'name' => 'Dev',
            'default' => 1,
            'application_id' => 1,
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now()
        ]);
        DB::table('resume_profiles')->insert([
            'resume_id' => 1,
            'full_name' => 'Jason Foy',
            'title' => 'Snr Dev',
            'email' => 'jason.foy21@gmail.com',
            'objective' => 'To turn ideas into reality!',
            'biography' => 'I was never good at school. I am just a super curious guy who loves to turn ideas into code.',
            'slider_text' => 'Full Stack Web Developer, Young, Self-Imporvement Enthuasiast',
            'display_picture_url' => 'https://images.unsplash.com/photo-1549692520-acc6669e2f0c?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=633&q=80',
        ]);
        Schema::table('resume_profile_educations', function (Blueprint $table) {
            $table->renameColumn('name', 'institute');
        });

        DB::table('resume_profile_educations')->insert([
            'resume_profile_id' => 1,
            'description' => 'Intern',
            'institute' => 'VistaStaffing',
            'current' => 0,
            'started_at' => '2019-07-02',
            'ended_at' => '2019-07-17'
        ]);
        DB::table('resume_profile_employments')->insert([
            [
                'resume_profile_id' => 1,
                'company_name' => 'Solutionstream',
                'position_description' => 'Snr. Dev',
                'started_at' => '2014-12-01',
                'ended_at' => '2019-07-02',
                'current' => 1,
            ],
            [
                'resume_profile_id' => 1,
                'company_name' => 'VistaStaffing',
                'position_description' => 'Snr. Dev',
                'started_at' => '2011-12-01',
                'ended_at' => '2019-05-01',
                'current' => 0,
            ]
        ]);
        DB::table('resume_profile_references')->insert([
            'resume_profile_id' => 1,
            'name' => 'Ali',
            'email' => 'Available Upon Request',
            'phone' => 'Available Upon Request',
            'relationship' => 'Friend',
        ]);

        DB::table('resume_profile_skills')->insert([
            [
                'resume_profile_id' => 1,
                'resume_skill_id' => 1,
                'progress' => 70
            ],
            [
                'resume_profile_id' => 1,
                'resume_skill_id' => 2,
                'progress' => 70
            ],
            [
                'resume_profile_id' => 1,
                'resume_skill_id' => 3,
                'progress' => 90
            ],
            [
                'resume_profile_id' => 1,
                'resume_skill_id' => 4,
                'progress' => 100
            ],
            [
                'resume_profile_id' => 1,
                'resume_skill_id' => 5,
                'progress' => 50
            ],
            [
                'resume_profile_id' => 1,
                'resume_skill_id' => 6,
                'progress' => 60
            ],
            [
                'resume_profile_id' => 1,
                'resume_skill_id' => 7,
                'progress' => 90
            ],
            [
                'resume_profile_id' => 1,
                'resume_skill_id' => 8,
                'progress' => 40
            ]
        ]);
        Schema::table('resume_projects', function (Blueprint $table) {
            $table->renameColumn('about_me', 'description');
        });
        DB::table('resume_projects')->insert([
            [
                'resume_id' => 1,
                'title' => 'Ninja RPG',
                'objective' => 'Combat as a Ninja against Chance!',
                'description' => 'Self Teaching Program for Javascript',
                'resume_project_display_id' => 1,
                'code' => 'Code to Display',
            ],
            [
                'resume_id' => 1,
                'title' => 'Some project',
                'objective' => 'To do something cool',
                'description' => 'This is the description area',
                'resume_project_display_id' => 2,
                'code' => 'Code to display',
            ]
        ]);
        DB::table('resume_project_skills')->insert([
            [
                'resume_project_id' => 1,
                'resume_skill_id' => 2,
            ],
            [
                'resume_project_id' => 1,
                'resume_skill_id' => 3,
            ],
            [
                'resume_project_id' => 1,
                'resume_skill_id' => 6,
            ],
            [
                'resume_project_id' => 1,
                'resume_skill_id' => 11,
            ],
            [
                'resume_project_id' => 1,
                'resume_skill_id' => 5,
            ],
            [
                'resume_project_id' => 1,
                'resume_skill_id' => 7,
            ]
        ]);
        DB::table('application_roles')->insert([
            'role_id' => 1,
            'application_id' => 2,
        ]);
        Schema::enableForeignKeyConstraints();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('applications', function (Blueprint $table) {
            //
        });
    }
}
