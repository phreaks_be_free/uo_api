<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class LineUpAttributeData extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
	    Schema::table('resume_lineup', function (Blueprint $table) {
		    $table->dropForeign(['resume_attribute_id']);
		    $table->dropColumn('resume_attribute_id');
		    $table->unsignedInteger('meta_type_id');
		    $table->foreign( 'meta_type_id' )->references( 'id' )->on( 'meta_types' );
	    });

	    Schema::drop('resume_attributes');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
