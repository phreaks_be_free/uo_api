<?php

use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AvailableApplications extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::create( 'roles', function ( Blueprint $table ) {
			$table->increments( 'id' );
			$table->string( 'name' )->unique();
		} );

		DB::table( 'roles' )->insert(
			[
				[
					'name' => 'resume',
				],
				[
					'name' => 'shopify'
				]
			]
		);

		Schema::create( 'application_roles', function ( Blueprint $table ) {
			$table->increments( 'id' );
			$table->unsignedInteger( 'role_id' );
			$table->unsignedInteger( 'application_id' );

			$table->unique('application_id' );

			$table->foreign( 'role_id' )->references( 'id' )->on( 'roles' );
			$table->foreign( 'application_id' )->references( 'id' )->on( 'applications' );
		} );

	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::dropIfExists( 'application_roles' );
		Schema::dropIfExists( 'roles' );
	}
}
