<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterApplications extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {

		/**
		 * Clean UP
		 */
		Schema::dropIfExists( 'instance_interpreters' );
		Schema::dropIfExists( 'application_instances' );

		Schema::dropIfExists( 'instances' );
		Schema::dropIfExists( 'interpreters' );

		Schema::dropIfExists( 'application_users' );


		Schema::dropIfExists( 'user_tokens' );
		Schema::dropIfExists( 'application_users' );
		Schema::dropIfExists( 'applications' );

		Schema::dropIfExists( 'role_acls' );
		Schema::dropIfExists( 'acls' );
		Schema::dropIfExists( 'user_roles' );
		Schema::dropIfExists( 'roles' );


		Schema::create( 'instances', function ( Blueprint $t ) {
			$t->increments( 'id' );
			$t->string( 'name' );
			$t->string( 'description' );
			$t->text( 'path' );
		} );

		Schema::create( 'interpreters', function ( Blueprint $t ) {
			$t->increments( 'id' );
			$t->string( 'name' );
			$t->string( 'description' );
			$t->text( 'path' );
		} );

		Schema::create( 'instance_interpreters', function ( Blueprint $t ) {
			$t->increments( 'id' );
			$t->unsignedInteger( 'instance_id' );
			$t->unsignedInteger( 'interpreter_id' );

			$t->foreign( 'instance_id' )->references( 'id' )->on( 'instances' );
			$t->foreign( 'interpreter_id' )->references( 'id' )->on( 'interpreters' );

		} );

		Schema::create('application_users', function (Blueprint $table) {
            $table->increments('id');
		    $table->string('role');
		    $table->string('first_name');
            $table->string('last_name');
            $table->string('email')->unique();
		    $table->unsignedInteger('application_id');
		    $table->timestamps();

		});
		
		
		Schema::create( 'applications', function ( Blueprint $t ) {
			$t->increments( 'id' );
			$t->string( 'name', 255 );
			$t->string( 'url', 255 );
			$t->unsignedInteger( 'user_id' );
			$t->boolean( 'active' );
			$t->timestamps();
			
			$t->foreign( 'user_id' )->references( 'id' )->on( 'users' );
		} );
		

		Schema::table('application_users', function (Blueprint $table) {
			$table->foreign( 'application_id' )->references( 'id' )->on( 'applications' );
		});

		Schema::create( 'application_instances', function ( Blueprint $t ) {
			$t->increments( 'id' );
			$t->unsignedInteger( 'application_id' );
			$t->unsignedInteger( 'instance_interpreter_id' );
			$t->boolean( 'active' );
			$t->timestamps();

			$t->foreign( 'application_id' )->references( 'id' )->on( 'applications' );
			$t->foreign( 'instance_interpreter_id' )->references( 'id' )->on( 'instance_interpreters' );
		} );

		// DB::table( 'applications' )
		//   ->where( 'id', 1 )
		//   ->delete();

		// DB::table( 'applications' )->insert( [
		// 	'active'  => 1,
		// 	'name'    => 'Development Application',
		// 	'url'     => 'http://localhost:4200',
		// 	'user_id' => 1
		// ] );
		// DB::table( 'instances' )->insert( [
		// 	'name'        => 'resume',
		// 	'description' => 'resume',
		// 	'path'        => '\\App\\Objects\\Resume\\ResumeComponent'
		// ] );

		// DB::table( 'interpreters' )->insert( [
		// 	'name'        => 'AbsCompAction',
		// 	'description' => 'Common App Call',
		// 	'path'        => '\\App\\Objects\\InstanceInterpreter\\Interpreter\\AbsCompAction'
		// ] );

		// DB::table( 'instance_interpreters' )->insert( [
		// 	'instance_id'    => 1,
		// 	'interpreter_id' => 1
		// ] );

		// DB::table( 'application_instances' )->insert( [
		// 	'application_id'          => 1,
		// 	'instance_interpreter_id' => 1,
		// 	'active'                  => 1
		// ] );
		

	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::drop( 'instance_interpreter' );
		Schema::drop( 'application_instances' );
		Schema::drop( 'instances' );
	}
}
