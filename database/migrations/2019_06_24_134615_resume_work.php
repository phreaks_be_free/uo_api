<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ResumeWork extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{

		/**
		 * Profile Skills
		 */
		Schema::create('resume_skills', function (Blueprint $table) {
			$table->increments('id');
			$table->string('tag')->unique();
			$table->string('name');
		});
		DB::table('resume_skills')->insert(
			[
				[
					'name' => 'PHP',
					'tag'  => 'php'
				],
				[
					'name' => 'JavaScript',
					'tag'  => 'js'
				],
				[
					'name' => 'GIT',
					'tag'  => 'git'
				],
				[
					'name' => 'Angular Framework',
					'tag'  => 'angular'
				],
				[
					'name' => 'Laravel Framework',
					'tag'  => 'laravel'
				],
				[
					'name' => 'CSS',
					'tag'  => 'css'
				],
				[
					'name' => 'Vue',
					'tag'  => 'vue'
				],
				[
					'name' => 'Bash',
					'tag'  => 'bash'
				],
				[
					'name' => 'Batch',
					'tag'  => 'dos'
				],
				[
					'name' => 'Python',
					'tag'  => 'python'
				],
				[
					'name' => 'jQuery',
					'tag'  => 'jquery'
				],
				[
					'name' => 'MySql',
					'tag'  => 'mysql'
				],
				[
					'name' => 'MSSql',
					'tag'  => 'mssql'
				],
				[
					'name' => 'Dynamo',
					'tag'  => 'aws-dynamo'
				],
				[
					'name' => 'NodeJs',
					'tag'  => 'node'
				],
				[
					'name' => 'Lambda',
					'tag'  => 'aws-lambda'
				],
			]
		);

		/**
		 * Resume Projects
		 */
		Schema::create('resume_project_displays', function (Blueprint $table) {
			$table->increments('id');
			$table->string('name');
			$table->string('tag');
		});

		DB::table('resume_project_displays')->insert(
			[
				[
					'name' => 'code highlighter',
					'tag'  => 'code_highlighter'
				],
				[
					'name' => 'iframe',
					'tag'  => 'iframe'
				],
			]
		);
		/**
		 * Projects
		 */
		Schema::create('resume_projects', function (Blueprint $table) {
			$table->increments('id');

			$table->unsignedInteger('resume_id');
			$table->foreign('resume_id')->references('id')->on('resume');

			$table->string('title');
			$table->string('objective');
			$table->text('about_me');


			$table->unsignedInteger('resume_project_display_id');
			$table->foreign('resume_project_display_id')->references('id')->on('resume_project_displays');
		});

		/**
		 * Profile Skills
		 */
		Schema::create('resume_project_skills', function (Blueprint $table) {
			$table->increments('id');
			$table->unsignedInteger('resume_project_id');
			$table->foreign('resume_project_id')->references('id')->on('resume_projects')->onDelete('cascade');

			$table->unsignedInteger('resume_skill_id');
			$table->foreign('resume_skill_id')->references('id')->on('resume_skills')->onDelete('cascade');;
		});

		/**
		 * Profile Information
		 */
		Schema::create('resume_profiles', function (Blueprint $table) {
			$table->increments('id');
			$table->unsignedInteger('resume_id');
			$table->foreign('resume_id')->references('id')->on('resume');


			$table->string('full_name');
			$table->string('title');
			$table->string('email')->nullable();
			$table->string('phone')->nullable();
		});

		/**
		 * Resume Profile Education
		 */
		Schema::create('resume_profile_educations', function (Blueprint $table) {
			$table->increments('id');
			$table->unsignedInteger('resume_profile_id');
			$table->foreign('resume_profile_id')->references('id')->on('resume_profiles')->onDelete('cascade');

			$table->string('name');
			$table->string('description');
			$table->date('started_at')->nullable();
			$table->date('ended_at')->nullable();
			$table->boolean('current')->default(false);
		});

		/**
		 * Resume Profile Employment
		 */
		Schema::create('resume_profile_employments', function (Blueprint $table) {
			$table->increments('id');
			$table->unsignedInteger('resume_profile_id');
			$table->foreign('resume_profile_id')->references('id')->on('resume_profiles')->onDelete('cascade');

			$table->string('company_name');
			$table->string('position_description');
			$table->date('started_at');
			$table->date('ended_at');
			$table->boolean('current')->default(false);
		});

		/**
		 * Resume Profile Skills
		 */
		Schema::create('resume_profile_skills', function (Blueprint $table) {
			$table->increments('id');
			$table->unsignedInteger('resume_profile_id');
			$table->foreign('resume_profile_id')->references('id')->on('resume_profiles')->onDelete('cascade');

			$table->unsignedInteger('resume_skill_id');
			$table->foreign('resume_skill_id')->references('id')->on('resume_skills')->onDelete('cascade');
		});


		/**
		 * Resume Profile References
		 */
		Schema::create('resume_profile_references', function (Blueprint $table) {
			$table->increments('id');
			$table->unsignedInteger('resume_profile_id');
			$table->foreign('resume_profile_id')->references('id')->on('resume_profiles')->onDelete('cascade');

			$table->string('name');
			$table->string('email')->nullable();
			$table->string('phone')->nullable();
			$table->string('relationship');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
	}
}
