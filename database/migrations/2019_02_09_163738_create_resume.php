<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateResume extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::create( 'meta_types', function ( Blueprint $table ) {
			$table->increments( 'id' );
			$table->string( 'type', 11 );
			$table->string( 'object_manager', 255 );
		} );

		DB::table('meta_types')->insert(
			[
				[
					'type' => 'string',
					'object_manager'=>''
				],
				[
					'type' => 'int',
					'object_manager'=>''
				],
				[
					'type' => 'json',
					'object_manager'=>''
				],
				[
					'type' => 'xml',
					'object_manager'=>''
				],
				[
					'type' => 'html',
					'object_manager'=>''
				],
				[
					'type' => 'base64',
					'object_manager'=>''
				],
			]);
		Schema::create( 'resume', function ( Blueprint $table ) {
			$table->increments( 'id' );
			$table->unsignedInteger( 'user_id' );
			$table->string( 'name', 90 );
			$table->timestamps();
			$table->foreign( 'user_id' )->references( 'id' )->on( 'users' );
		} );

		Schema::create( 'resume_attributes', function ( Blueprint $table ) {
			$table->increments( 'id' );
			$table->string( 'name', 90 );
			$table->unsignedInteger( 'meta_type_id' );
			$table->foreign( 'meta_type_id' )->references( 'id' )->on( 'meta_types' );
		} );

		Schema::create( 'resume_lineup', function ( Blueprint $table ) {
			$table->increments( 'id' );
			$table->unsignedInteger( 'resume_id' );
			$table->unsignedInteger( 'resume_attribute_id' );
			$table->text( 'resume_attribute_value' );
			$table->timestamps();
			$table->foreign( 'resume_id' )->references( 'id' )->on( 'resume' );
			$table->foreign( 'resume_attribute_id' )->references( 'id' )->on( 'resume_attributes' );
		} );
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::drop( 'resume_lineup' );
		Schema::drop( 'resume_attributes' );
		Schema::drop( 'meta_types' );
		Schema::drop( 'resume' );
	}
}
