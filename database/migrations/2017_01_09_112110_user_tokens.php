<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UserTokens extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {



	    Schema::create('applications',function(Blueprint $table){
		    $table->increments('id');
		    $table->string('title',90);
		    $table->longText('connection_storage');
		    $table->boolean('active');
		    $table->timestamps();
	    });

	    Schema::create( 'user_tokens', function ( Blueprint $table ) {
		    $table->increments( 'id' );
		    $table->unsignedInteger( 'user_id' );
		    $table->unsignedInteger( 'application_id' );

		    $table->string( 'token', 90 );
		    $table->boolean( 'active' );
		    $table->dateTime( 'expires' );
		    $table->timestamps();

		    $table->foreign( 'user_id' )->references( 'id' )->on( 'users' );
		    $table->foreign( 'application_id' )->references( 'id' )->on( 'applications' );

	    } );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('user_tokens');
    }
}
