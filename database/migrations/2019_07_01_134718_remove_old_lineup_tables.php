<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemoveOldLineupTables extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		\App\Objects\Models\ResumeLineup::truncate();
		Schema::dropIfExists( 'resume_lineup' );
		Schema::dropIfExists( 'resume_lineup_types' );
		Schema::dropIfExists( 'resume_pages' );

	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		//
	}
}
