<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterShopifyAccounts extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::dropIfExists( 'shopify_accounts' );
		Schema::create( 'shopify_accounts', function ( Blueprint $table ) {
			$table->increments( 'id' );
			$table->unsignedInteger( 'application_id' )->unique();
			$table->string( 'token')->nullable();
			$table->string( 'shop' )->unique();
			$table->integer( 'timestamp' )->nullable();
			$table->timestamps();

			$table->foreign( 'application_id' )->references( 'id' )->on( 'applications' );
		} );
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::dropIfExists( 'shopify_accounts' );
	}
}
