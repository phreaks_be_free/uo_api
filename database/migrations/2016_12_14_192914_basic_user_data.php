<?php

use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class BasicUserData extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        /**
         * Default SU
         */
        DB::table('users')->insert([
                'username' => 'Jason.Foy',
                'email' => 'foy.jason21@gmail.com',
                'password' => '$2y$10$S9Fm0VRr5KWX5cfS2xFCt.wODRUQeNpCHVBQiaefUNLesPfstsDr2'
        ]
        );
        DB::table('users')->insert([
                'username' => 'ali',
                'email' => 'artanha7@gmail.com',
                'password' => Hash::make('alimalik'),
        ]
        );

        /**
         * Create Default Role
         */
        DB::table('roles')->insert(
            [
                [
                    'name' => 'practice',
                    'description' => 'Practice',
                    'active' => 1
                ],
                [
                    'name' => 'provider',
                    'description' => 'Provider',
                    'active' => 1
                ],
                [
                    'name' => 'sys-admin',
                    'description' => 'System Admin',
                    'active' => 1
                ]
            ]);

        /**
         * Associate User to Role
         */
        DB::table('user_roles')->insert([
            'role_id' => 3,
            'user_id' => 1
        ]);

        DB::table('acls')->insert([
            [
                'controller' => 'test',
                'action' => 'test'
            ],
            [
                'controller' => 'test2',
                'action' => 'test2'
            ]
        ]);

        DB::table('role_acls')->insert([
            [
                'role_id' => 1,
                'acl_id' => 1
            ],
            [
                'controller' => 1,
                'action' => 2
            ]
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::table('role_acls')->delete();
        DB::table('user_roles')->delete();
        DB::table('roles')->delete();
        DB::table('acls')->delete();
    }
}
