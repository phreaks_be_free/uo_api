<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterResumeProfilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('resume_profiles', function (Blueprint $table) {

            $table->string('objective');
            $table->text('biography');
            $table->text('slider_text');
            $table->string('display_picture_url');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('resume_profiles', function (Blueprint $table) {
            //
        });
    }
}
