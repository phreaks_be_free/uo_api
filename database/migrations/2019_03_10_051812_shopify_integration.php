<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ShopifyIntegration extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
	    Schema::create('shopify_accounts', function (Blueprint $table) {
		    $table->increments('id');
		    $table->unsignedInteger('application_id');
		    $table->string('username');
		    $table->string('password');
		    $table->string('shop');
		    $table->timestamps();

		    $table->foreign('application_id')->references('id')->on('applications');
	    });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
	    Schema::dropIfExists('shopify_accounts');
    }
}
