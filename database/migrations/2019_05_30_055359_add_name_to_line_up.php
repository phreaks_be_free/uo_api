<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNameToLineUp extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
	    Schema::create('resume_lineup_types', function (Blueprint $table) {
	    	$table->increments('id');
		    $table->string('name');
	    });

	    DB::table( 'resume_lineup_types' )->insert(
		    [
			    [
				    'name' => 'objective',
			    ],
			    [
				    'name' => 'skills'
			    ],
			    [
				    'name' => 'education'
			    ],
			    [
				    'name' => 'references'
			    ],
		    ]
	    );

    	Schema::table('resume_lineup',function (Blueprint $table){
		    $table->unsignedInteger('lineup_type_id');
		    $table->foreign( 'lineup_type_id' )->references( 'id' )->on( 'resume_lineup_types' );
	    });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
