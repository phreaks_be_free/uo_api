<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterResumeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
	    Schema::table('resume', function (Blueprint $table) {
		    $table->dropForeign(['user_id']);
		    $table->dropColumn('user_id');
		    $table->unsignedInteger('application_id');
		    $table->foreign( 'application_id' )->references( 'id' )->on( 'applications' );
	    });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
