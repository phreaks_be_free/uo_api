<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Entities extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('practices',function(Blueprint $table){
            $table->increments('id');
            $table->string('name');
            $table->string('account_manager_phone')->nullable();
            $table->string('account_manager_email')->nullable();
            $table->string('account_manager_first_name');
            $table->string('account_manager_last_name');
            $table->timestamps();
        });
        Schema::create('clinics',function(Blueprint $table){
            $table->increments('id');
            $table->unsignedInteger('practice_id');
            $table->string('name');
            $table->string('address');
            $table->timestamps();

            $table->foreign('practice_id')->references('id')->on('practices');
        });
        Schema::create('providers',function(Blueprint $table){
            $table->increments('id');
            $table->unsignedInteger('clinic_id');
            $table->string('first_name');
            $table->string('last_name');
            $table->boolean('is_orthotist');
            $table->timestamps();

            $table->foreign('clinic_id')->references('id')->on('clinics');
        });
        Schema::create('patients',function(Blueprint $table){
            $table->increments('id');
            $table->unsignedInteger('provider_id');
            $table->string('first_name');
            $table->string('list_name');
            $table->float('weight');
            $table->dateTime('dob');

            $table->timestamps();

            $table->foreign('provider_id')->references('id')->on('providers');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('patients');
        Schema::drop('providers');
        Schema::drop('clinics');
        Schema::drop('practices');
    }
}
