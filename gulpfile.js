const elixir = require('laravel-elixir');

require('laravel-elixir-vue-2');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(mix => {
    mix.sass('app.scss');
    mix.sass('home.scss', 'public/css/home.css');

    mix.sass('home.scss', 'public/css/home.css');
    mix.sass([
        '../../../node_modules/bootstrap/scss/bootstrap.scss',
    ],
        'public/css/vendors.css');

    mix.sass('shopify/install_shopify.scss', 'public/css/shopify/install_shopify.css');

    mix.copy('resources/assets/js', 'public/js/');

    /** Vendor **/
    mix.copy('node_modules/jquery/dist/jquery.js', 'public/js/jquery.js')

});

elixir(mix => {
    mix.sass('doormat.scss');
});


// elixir(function(mix) {
//     mix.scriptsIn('public/js/some/directory');
// });
//
// elixir(function(mix) {
//     mix.scripts([
//         'order.js',
//         'forum.js'
//     ]);
// });
